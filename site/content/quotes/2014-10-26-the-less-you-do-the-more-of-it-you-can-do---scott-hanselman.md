---
layout: quote
author: Scott Hanselman
quote: The less you do, the more of it you can do.
title: The less you do, the more of it you can do. - Scott Hanselman
tags: productivity, concentration, attention
---

This is a very recent quote.
In fact, [Scott Hanselman](http://www.hanselman.com) mentioned very sarcasticly this in [his talk about productivity](https://www.youtube.com/watch?v=IWPgUn8tL8s).

He argues, as so many do in recent years, that we try to do too much simultaneously.
We shouldn't multi task. Even worse, there is no such thing as multitasking. There is only single tasking and context switching.

Suppose your life's work is composed of projects.
Let's look at these projects as an abstract (not-necessarily strictly defined) amount of work.
The maximum average amount of work you can put into every project is inversely related to the amount of projects you have running.
As a simple illustration, here is a plot of the maximum average number of hours you can put into your projects in function of the amount of projects.

![Graph of throughput in terms of the number of things you do per week per number of projects](/assets/scott-hanselman/illustration.png)

If you assume that the amount of hours you put into any project is at least somewhat related to the quality of the result, then it's easy to see how having less projects in your life is beneficial your life's work.

Personally, I just love how this explains why some people can do nothing, infinitely.
