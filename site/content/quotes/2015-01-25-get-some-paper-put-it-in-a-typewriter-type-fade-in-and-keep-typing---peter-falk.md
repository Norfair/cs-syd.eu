---
layout: quote
author: Peter Falk
quote: Get some paper, put it in a typewriter, type FADE IN... and keep typing.
title: Get some paper, put it in a typewriter, type FADE IN... and keep typing. - Peter Falk
tags: paper, work, focus, time, productivity
---

I consider workflow optimisation one of my hobbies.
At some point, a friend of mine pointed out how much time I spend trying to optimise work.
He pointed me to this quote, and I think it contains a message that people like me need to keep in the back of their head.

The quote comes from someone giving advice on how to write a play.
Instead of focussing on planning and organising your work, it argues that we need to focus on _doing_ the work and not giving up.
This a friendly reminder to focus on the work rather than the work system.


