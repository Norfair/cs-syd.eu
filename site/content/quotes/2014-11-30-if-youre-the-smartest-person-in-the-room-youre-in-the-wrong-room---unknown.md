---
layout: quote
author: unknown
quote: If you’re the smartest person in the room, you’re in the wrong room.
title: If you’re the smartest person in the room, you’re in the wrong room. - unknown
tags: smart, wisdom, room
---

Ever since I started studying at university, I have been surrounded with people who are a lot smarter than me.
I had never felt as stupid as I did in my first year at the university.

I absolutely loved it.
I received the opportunity to learn form great minds.
Ever since, I have made a habit out of finding the people from whom I can learn the most.

That's what I think this quote is all about.
It's about always finding new people (or things, of course) to learn from and I think that's worth sharing.

Now, as a little extra, the literal minded people among you will probably have readied a very sarcastic response to this quote.
The best reaction I've gotten to this quote has to be this one

> Following that logic to the extreme, no-one should ever be in the same room, or indeed a room by themselves.

This logic is of course infallible, but it somewhat defeats the purpose of the quote.
