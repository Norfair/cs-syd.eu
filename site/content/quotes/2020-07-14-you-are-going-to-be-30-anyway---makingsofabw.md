---
layout: quote
title: You are going to be 30 anyway, you might as well be a doctor - @Makingsofabw
quote: You are going to be 30 anyway, you might as well be a doctor
author: @Makingsofabw
tags: doctor
---

It has been a while since I posted another quote post.
This one is about a wholesome story about a woman and her daughter that I like to bring up when it comes to explaining my strict lifestyle.

<div></div> <!--more-->

The story is quite short:

> A woman once asked her daughter what she wanted to be when she grew up.
> The daughter, clearly conflicted, said that she wanted to be a doctor.
> Her mother was happy about this wish, but noticed her daughter's hesitance.
> She asked why her daughter did not sound sure about her goal.
> The daughter replied: "I would like to be a doctor, but that means that I will be studying until I am thirty."
> Her mother poetically responded: "You are going to be thirty anyway, you might as well be a doctor."

I like this story because it acknowledges that there is a future, and that you are responsible for it.
But even better than that, it recommends that you take responsibility for your future because you have nothing else to do anyway.

I bring this story up in the context of diet and exercise.
You have to eat anyway, and you will have habits anyway, so you might as well eat well and you might as well build good habits.
If you start building good habits now, you can have amazing results in the gym sooner than you think.
Conversely, if you had started years ago, you would already have these results and how much do you remember of the last two years anyway?
