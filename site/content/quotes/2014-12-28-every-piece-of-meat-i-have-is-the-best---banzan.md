---
layout: quote
author: Banzan
quote: Every piece of meat I have is the best
title: Every piece of meat I have is the best - Banzan
tags: meat, Banzan
---

This quote comes from a story in the tradition of Zen Buddhism.
It's the story of Banzan at the butcher's shop.

> Before he became a great Zen master, he spent many years in pursuit of enlightenment, but it eluded him.
> Then, one day, as he was walking in the marketplace, he overheard a conversation between a butcher and his customer.
> 'Give me the best piece of meat you have,' said the customer.
> And the butcher replied, 'Every piece of meat I have is the best.
> There is no piece of meat here that is not the best.'
> Upon hearing this, Banzan became enlightened. 

Honestly, it took me a while to get what Banzan understood, because it definitively wasn't what the butcher meant.
I'm guessing the butcher was just trying to sell his meat, but in that moment, Banzan understood something.

The story is trying to explain that no two items are as comparable as we take them to be.
Any piece of meat is unique and therefore the best of its kind.

Now, your ambition is probably not to become a great Zen master, but you can still take something away from this story.
Whenever you feel inadequate, remember that if you were 'better', you would be different and you might not want to be different from what you are now.



