---
layout: quote
quote: I have not reached the burrito point yet.
author: Xavier Goás Aguililla
title: I have not reached the burrito point yet. - Xavier Goás Aguililla
tags: burrito,point,Haskell,monads
---

I once asked my friend whether he understood Monads and could explain them to others.
He sarcastically responded "No, I have not reached the burrito point yet.", supposedly in reference to 
[a paper that explains burritos as strong monads](https://www.cs.cmu.edu/~edmo/silliness/burrito_monads.pdf).

<div></div><!--more-->

He defined 'the burrito point' as the level of understanding at which you can understand why a seemingly simple analogy is relevant with respect to a concept that is perceived to be hard to understand.
More generally it can be taken to mean "an adequate level of understanding".
