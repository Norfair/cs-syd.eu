---
layout: quote
author: The Roots
quote: You don't say "Good luck!". You say "Don't give up!".
title: You don't say "Good luck!". You say "Don't give up!". - The Roots
tags: fire, motivation, success, work, luck
---

I absolutely love this quote.
It's from a song that was my alarm clock for months, even though it's not my usual taste in music.

> The Fire - The Roots

The lyrics talk about someone who is passionate about his work and excited about his life.
The song celebrates the fact that this person knows that perseverance is more important than luck.

It's an encouragement to keep "the fire" in your heart, and "let it burn".
It's the perfect song to wake up to.
