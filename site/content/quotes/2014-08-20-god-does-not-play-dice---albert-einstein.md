---
layout: quote
author: Albert Einstein
quote: God does not play dice.
title: God does not play dice. - Albert Einstein
tags: religion, science
---

Einstein was both religious and a scientist.
He was convinced that the universe is based on deterministic laws.
Quantum mechanics, however, tells us that the universe is governed by probabilistic laws.
With whatever amount of knowledge about the present, or the past, we still can't predict the future

As a religious person, Einstein believed that God is all-knowing.
When he found out about this probabilistic nature, he said: "God does not play dice.".
He got angry. He never accepted any of the philosophical interpretations of this concept, but he still contributed to the field.
This can be looked at in a number of ways, but these are the two that I suggest:

- As Einstein lived in the twentieth century, in a time where being a scientist was frowned upon already, he can be viewed as an example of how religion and science can coexist. 
- Imagine what kind of scientists we have missed out on due to people's lives being controlled be religion. Imagine what could have been discovered or invented if people weren't punished for saying anything that isn't consistent with the bible.
