---
layout: quote
author: Neil Sutherland
quote: I stopped believing in God when I realised it's just dog backwards
title: I stopped believing in God when I realised it's just dog backwards - Neil Sutherland
tags: religion, prejudice, reflection
---

This is a lesser known quote from the first [inbetweeners movie](http://www.imdb.com/title/tt1716772/?ref_=ttqt_qt_tt).
It is a good example of the fact that not everything that sounds clever is clever.
Of course what Neil says, is true. If you reverse the word "dog", you do indeed get the word "god".
["G - O - D ... D - O - G ..."](https://www.youtube.com/watch?v=bp40SayRcws), as he goes on to explain, was a revelation that allowed him to stop worrying.

In fact, the entire scene is worth watching, but I couldn't find it anywhere online.
An entire speech follows after this quote where Neil explained how he became very happy when he stopped worrying about things.
The first question that came to my mind after I was done laughing (and I laughed way too hard at this), was the following:

    What exactly, about this person's statement, train of thought and view of the world, is different from the statements and trains of thought that we applaud?

I have pondered this question for a while now, and I've come to a worrying conclusion.
There are a few things to consider.

- To use the words of a fellow inbetweeners fan: "He just really sounds like an idiot."

  It is a logical fallacy to think that only people who sound smart could say something clever.
  In fact, as Bill Nye says it: ["Everyone you will ever meet knows something you don't.”](/quotes/2014-09-10-everyone-you-will-ever-meet-knows-something-you-dont---bill-nye.html)
  I have to admit, though, that I suffer from this prejudice myself.

- What is a revelation to Neil, is obvious to most people. "god" is indeed "dog" backwards, but nobody bothered to think about that.
  
  This consideration, in itself, is fine, but it  doesn't mean that there is nothing more to learn from the statement.
  In the case of this quote, the entire train of thought that you're now reading started after some reflection on my own prejudice.

It seems that general prejudice about the way someone sounds when stating his opinion is far worse than it could be.
This means that if you have something important to say, you already have to know how to convey your message, otherwise you might as well have nothing to say.
Lots of valuable information could be aggressively contained in the minds of people who haven't found a way to effectively communicate that information through the walls of our prejudice.
