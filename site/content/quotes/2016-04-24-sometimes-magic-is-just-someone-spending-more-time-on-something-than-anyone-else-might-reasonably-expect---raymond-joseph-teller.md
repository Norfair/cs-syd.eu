---
layout: quote
quote: Sometimes magic is just someone spending more time on something than anyone else might reasonably expect.
author: Raymond Joseph Teller
title: Sometimes magic is just someone spending more time on something than anyone else might reasonably expect. - Raymond Joseph Teller
tags: magic,time
---

To be clear, Raymond Joseph Teller is a world-class magician.

I've had the enormous privilege to live with an aspiring magician myself.
I've witnessed first-hand how magical seemingly benign manipulation of small objects can amaze the smartest people.

Of course it is amazing to see my brother's show.
Audiences get to enjoy a night of bewilderment and laughter.
I can only imagine how invigorating it must feel for him.

![My brother's rope act, 1](/assets/sometimes-magic/tijl1.png)

If you only come to the show, you will never see the real work.
For every hour of a show, he spends at least 40 hours practicing.
I'm not saying he finds it any less enjoyable but it's important to realise what kind of work goes into making magic happen.

![My brother's rope act, 2](/assets/sometimes-magic/tijl2.png)

Not all magic happens during one of my brother's shows.
Other people too do amazing work.
Oddly enough, the fact that magicians practice a lot is comparatively well-known.

We each know how much we work ourselves and we know about our achievements through it,
but somehow we never fail to underestimate how much another person may be willing to work for their goals.

This is not something to be disheartened by.
It means that you too can achieve magic.
It may, however, just require you to spend more time than anyone else might reasonably expect.
