---
layout: quote
author: Unknown
quote: Today is a gift. That's why it is called the present.
title: Today is a gift. That's why it is called the present. - Unknown
tags: beauty, business, stress
---

The full quote does not fit into the a title:

    The clock is running.
    Make the most of today.
    Time waits for no man.
    Yesterday is history.
    Tomorrow is a mystery.
    Today is a gift.
    That's why it is called the present.

The first time I heard this, I was watching [Kung Fu Panda](https://www.youtube.com/watch?v=Znnj1oFxCRM), but [apparently](http://www.phrases.org.uk/meanings/384000.html) this quote dates back from the days before the english language.

The first half of this saying may sound hurried.
In contrast, it is written to remind you to allow yourself to stop and appreciate the beauty of the world we live in.

Too many of us have fallen into [the busy trap](http://opinionator.blogs.nytimes.com/2012/06/30/the-busy-trap/?_php=true&_type=blogs&_r=0).
This quote can remind you that it is not necessarily good to feel busy, and that being alive is a privilege that not many are afforded.
