---
layout: quote
author: Linus Torvalds
quote: I like offending people, because I think people who get offended should be offended.
title: I like offending people, because I think people who get offended should be offended. - Linus Torvalds
tags: offense, arrogance, perception
---

Ah Linus, perceived arrogant as always.
Is this arrogant though? Let's have a look.

Google, [define](/posts/2014-09-21-confusing-definitions.html) 'offended' please:

> Offended: resentful or annoyed, typically as a result of a perceived insult.

Note very carefully the word 'perceived' here.
I used to think that there were two prerequisites for a statement to be offensive.

- The statement has to be _meant_ as an insult.
- The statement has to be _accepted_ as an insult.

Then again, some people even succeed in being offended by things that aren't meant as an insult at all.

Let's take complimenting a person on their clothes as an example.
Suppose I say to someone "Those are really nice jeans, you're wearing!".
(I hope you can agree that this is really supposed to be a compliment.)

Now, I think we can equally agree that any person can perceive this as an insult in at least three different ways, for example: depending on where they hear the emphasis in the sentence.

- _That_'s a really nice pair of jeans, you're wearing!
- That's a really _nice_ pair of jeans, you're wearing!
- That's a really nice pair of _jeans_, you're wearing!

As even a compliment can be perceived as an insult, I have revised my opinion.
I now think that there is only _one_ prerequisite for a statement to be insulting:

- The statement has to be _accepted_ as an insult.

Notice how this means that you alone are responsible for being insulted.

Back to Linus.
With "people who get offended", I personally think Linus means "people who have a habit of accepting insults".
Is that arrogant?

