---
layout: quote
quote: Have a day
author: Unknown
title: Have a day - Unknown
tags: day,sad
---

Even in an amazing life, sometimes you just have a bad day.

<div></div><!--more-->

This one bad day will not stop you from achieving your goals.
What will stop you is if you let this bad day bring you down and make you give up.

Tomorrow will be better.

> I know, you're sad, so I won't tell you to have a good day.
> Instead, I advise you to simply have a day.
> Stay alive, feed yourself well, wear comfortable clothes, and don't give up on yourself just yet.
> It'll get better.
> Until then, have a day.

