---
layout: quote
quote: Don't find fault, find a remedy.
author: Henry Ford
title: Don't find fault, find a remedy. - Henry Ford
tags: fault, remedy, responsibility, time, energy
---

Say you've made a mistake.
Something has happened that you didn't intend to have happen and you didn't want to happen.
Let's say someone is kind enough to point out your mistake.
What do you do?

<div></div><!--more-->

You might not care that you've made the mistake.
You can then ignore the entire situation and go on with your life.

Say, however, that you care enough to reply to the person that pointed out your mistake ever so helpfully.
You can hold on to the nearest, cheapest reason that you can think of to explain why the mistake found its way into existence.
After that, you can still move on with your life, having satisfied your remarkable need to attribute responsibility by either taking that responsibility, or giving it to someone/something else.

The mistake is still there though.
If you haven't fixed the cause by now, it might happen again.
Are you going to have it happen again?

There is no use in finding the reason why your mistake took place if not to help you fix it now or to help you prevent a similar mistake in the future.
If you are not planning to fix your mistake, the difference between finding fault and not finding fault is only time, wasted time.
Not only your time, but the time of the person who was helpful to you.
Now that's not very grateful, is it?

Enough of my guilt tripping.
You haven't even necessarily done anything!
What I'm trying to show you here is that the mistake, in this situation, is not a bad thing.
On the contrary, it allows you to improve yourself through the helpfulness of the person that pointed it out.
What I'm also trying to show is that *the reason you've made the mistake does not matter unless you use it for reflective purposes*.
