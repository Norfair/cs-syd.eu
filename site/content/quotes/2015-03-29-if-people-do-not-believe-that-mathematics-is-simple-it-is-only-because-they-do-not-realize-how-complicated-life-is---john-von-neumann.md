---
layout: quote
author: John von Neumann
quote: If people do not believe that mathematics is simple, it is only because they do not realize how complicated life is.
title: If people do not believe that mathematics is simple, it is only because they do not realize how complicated life is. - John von Neumann
tags: mathematics, simplicity, complexity
---

Have you ever wondered about how many of the things you say on a daily basis are true?

Let's only consider statements as questions don't concern themselves with the truth before they are answered.
For a moment, just forget about anything subjective or vague you've said like "I feel warm" or "The weather is good today".
Those statements can  be thought of as both true and false because they haven't been clarified: What is good weather?
For the sake of argument, you can complete any statement to the full statement you meant as statements are often abbreviated.

Remember that there is nothing between truth and falsehood.
A statement is either true or untrue.
What are you left with?

It's not a lot, is it?
Moreover, it's really hard to even speak of truth in this context.
Most statements are not meant to be true, they're meant to convey information.

Mathematics only concerns itself with what is true.
If a statement has been proven to be true, it is true.
If a statement has been proven to be false, it is false.
If something has not been proven, it is not used to prove others, until it is proven.
Note that a statement doesn't _become_ true when it is proven, it was always true, but we've only just discovered that.
This is why we speak about _discovering mathematics_.

Further more, a true statement is true in _every_ circumstance.
The truth of a mathematical statement is not even dependent on the very existence of the universe.

Isn't that just so much simpler?
