---
title: On answers to questions
---

It has come to my attention that there exist different opinions on what to do
what someone asks you a question.
I am convinced that I am not the only person who will have been unaware of this
fact, so I would like to write this down in order to help prevent further
frustration.

<div></div><!--more-->

### Situation of Frustration

In what follows I will describe a situation in which person A asks person B a
question in order to gather information.
There are other purposes for questions, but in this case we will disregard
these for the moment.


Alice and Bob are roommates.
They get along well and are well meaning individuals in their own right.
They share a small number of bottles of water in the shared fridge.
They have an understanding to keep the fridge stocked with between two and four
bottles of water, and to refill the fridge with more bottles of water from the
cellar whenever necessary.
However, the fridge needs to be used for other things than only bottles of
water, so they keep a larger stock of bottles in the cellar as well.

One day, Bob is planning a day trip for which he knows he needs two bottles of
water.
While exiting the door of the apartment to go to the cellar to get his boots,
and without first checking the fridge, Bob remembers that he needs two bottles
of water.
He knows that he could get them from the fridge in the apartment if there are
currently four or more bottles in the fridge.
Of course he does not remember how many bottles are currently in the fridge.
Indeed, the fridge might need restocking.
He figures that he can get the right number of bottles from the cellar to
restock the fridge, if necessary, and to bring to his trip.
This way he can save a trip to the cellar, how efficient!
He knows that Alice is currently in the kitchen because he just saw her there.

*He asks Alice from across the hallway: "How many bottles of water do we still
have in the fridge?"*

Alice knows about Bob's trip, but not that he would need bottles of water for
it or how many.
She notices, however, that Bob is going down the staircase.
He must be on his way to restock the fridge, how helpful!
She checks the fridge and finds only two bottles of water, which means the
fridge doesn't technically need restocking but might as well be.

*She responds: "Bring some!"*

This is where the frustration begins.


**Bob, in formulating the question, assumed that he will take the responsibility
of parsing the response into the information that he needs: A number of
bottles.
Alice, in responding to the question, assumed that she will take the
responsibility of parsing the question to produce the information that she
thinks he needs: A boolean "bring more" or "don't bring more".**

### Options

At this point bob exits the hearing range within which he could still
communicate with Alice via yelling.
In order to continue the conversation he would have to go back up the stairs,
therefore eliminating the efficiency of only taking the trip to the cellar once
that the question was meant to achieve.


When Bob hears "Bring some!", he has a few options:

* Assume that Alice has full context: She knows about the trip and knows that
  he needs two extra bottles.

  This is a valid guess because Alice is very helpful, well-prepared, and could
  definitely have guessed that he needed water for his trip.

  In this situation, Alice might have said "Not enough, bring some" if there
  were 4 or fewer bottles in the fridge.
  Indeed, if there were 4 bottles in the fridge, she'd want Bob to bring just
  the 2 bottles for his trip.
  If there were no bottles in the fridge, she'd want Bob to bring 4 bottles for
  the fridge, and 2 more for his trip.
  This means that Bob needs to bring anywhere from 2 to 6 bottles.
  This risks bringing too many bottles, which would have to be brought back to
  the cellar in another trip to the cellar.


* Assume that Alice knows about the contents of the fridge but not about the
  fact that Bob needs two bottles for his trip.

  This is a valid guess because Bob believes that Alice has no responsibility
  to figure out if he needs water for his trip.

  In this case, Alice could have said what she said if there were two or fewer
  bottles in the fridge.
  Indeed, if there were two bottles in the fridge, she'd want him to bring two
  more, and if there were no bottles in the fridge, she'd want him to bring four
  more.

  This means that Bob needs to bring anywhere from 4 to 6 bottles.
  This option also risks overstocking the fridge.


* Assume that Alice did not even check the fridge and just guessed.

  This is a valid guess because Alice did not respond with a number and can
  sometimes respond on autopilot.

  In this case the fridge could be anywhere from empty (0 bottles) to
  completely stocked (4 bottles)
  It means that bob needs to bring between 2 and 6 bottles.

* Go back up the stairs to continue the conversation and/or check on the fridge himself.


Bob comes back with anywhere between 0 and 6 bottles of water, and risks both
under-stocking the fridge (which requires another trip to the cellar) and
over-stocking the fridge (which also requires another trip to the cellar).


From Bob's perspective, Alice has doomed him to the cellar because she did not
answer his question with a number.
From Alice's perspective, she has been extra helpful by relieving bob of the
responsibility to make a decision about whether to bring more bottles.


### Who is right?


#### Alice's point of view

Here's the big sad problem:

**Alice and Bob have both made a _valid_ trade-off that sometimes helps and sometimes hinders.**


Alice performs an optimistic optimisation:

She knows that the two of them know each other well.
She likes taking extra responsibility if it means that she can help someone.
For example the responsibility of parsing a question into the information the
asker is looking for.

In any situation where she has the full context, which is actually very common,
this type of response saves the asker from some thinking.

**From her perspective, she is being very helpful by never answering the
question directly.**

She also believe she _is_ answering the question and that this is what a good
answer looks like.
She will interpret complaints about her style of response as a lack of
appreciation for her helpfulness.


#### Bob's point of view

Bob performs a pessimistic optimisation:

He knows that she does not always have the full context.
In order to prevent possible duplication of effort, he decides to always take
the responsibility to parse a response into the information he is looking for.
He meticulously crafts questions so that an answer to the question gives him
the information he is looking for.

In any situation where the asker does not have the full context, and you can
never tell if the current situation is one of those, this type of response
saves a duplication of effort.

**From his perspective, he is being very helpful by not requiring the answerer
to do any thinking for him.**

He also believes that not answering the question involves going out of one's
way to make the communication extra difficult. 


#### Frustration

Both people in this situation are trying to be helpful and also believe that
they themselves are.
They also both believe that the other is going out of their way to be extra
unhelpful.

Alice: Why can't he just appreciate my being helpful?!
Bob: Why can't she just answer the question?!


### Conclusion

#### Awareness

For the sake of avoiding frustration, it is important to be aware of both Alice
and Bob's perspective.
Knowing that the other is not going out of their way to be unhelpful, but
rather trying to be helpful in their own way, goes a long way.

Note that awareness only helps with the frustration that a situation like this
might cause.
It does not help with the actual miscommunication.


#### Next steps

As either Alice or Bob, one might have gained awareness of these different ways
of responding to questions, but what can they do with this awareness?

Note that any of the following proposed change is only to be applied to oneself.
It is impossible to robustly solve a communications issue by requiring others
to change.

As Alice, when you know that you are dealing with a Bob:
Answer questions with an answer in the unit that he is looking for even if you
think you have all the context and think you could help more.
This lets him make conclusions without any guesswork.

As Bob, when you know that you are dealing with an Alice:
Ask questions that include all the potentially relevant context.
This lets her feel helpful while avoiding any miscommunication that might occur
due to missing context.
It also speeds up communications in the common case where there is no missing
context by adding extra redundancy.


#### Alternative solutions

These are included for the sake of completeness.
Yes, using one of these would work for the case of the bottles on the trip but
do not solve this problem in general.

* Bob make sure never to start communications that he won't have the chance to
  elaborate on.
* Bob could check the fridge himself and not involve Alice whatsoever.
* Bob and Alice could maintain their own separate inventory of water.
* Bob and Alice could live separately.
* Everyone could just stop drinking water and die.

These solutions are all indeed more robust, but miss the point of this
discussion.




TODO run Chatgpt on this with the questions

- Please check for spelling mistakes
- Who is right, Alice or Bob?

