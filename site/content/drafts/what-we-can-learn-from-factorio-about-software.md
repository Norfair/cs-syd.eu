---
title: What we can learn about making software from factorio
tags: smos, programming, software, factorio
---

Factorio is my all time favourite game.
I am convinced that it has made me a better engineer.
As a hiring manager, I have started accepting save files of well functioning
factories to have applicants skip past the resume stage of the interview
process.

<div></div><!--more-->

### Automate 

### Don't tear down the old version before you finish and test the new version.

### Don't be afraid to waste resources if it lets you save time

### Temporarily speeding things up manually can only help with latency, not throughput

### Perfect is the enemy of the good

OCD will not help you, don't worry about your belts being laid out correctly

### Assume that things will go wrong

### Make sure that the desired state is a steady state
