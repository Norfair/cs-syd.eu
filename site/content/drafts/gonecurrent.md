---
title: Building a highly available failure detector
---



### What is a failure detector?

It guesses when a notifier has died

### Why a deadman's switch?

A dead notifier can't tell you that it's dead.

### Why not a health check?

Firewalls

This way we can avoid the requirement that the failure detector needs to be able to contact the notifier.

### Why a distributed algorithm?

Because the failure detector can die too.

### Why a "new" algorithm?

I couldn't find any others that did what I wanted and the ones I found were much more general and complex.

_because it's fun_.


# The algorithm

## Requirements

### Essentials

* Administrators must be alarmed if notifiers die
* Administrators must be alarmed even if peers die
* Nodes' clocks will not be synchronised
* Notifiers only say that they are alive.
  Nodes don't ask notifiers if they're still alive.
  (This means that notifiers can be behind a firewall.)

### Nice-to-haves

* All nodes run the same code
* Nodes' clocks can be arbitrarily far out of sync.
* Nodes' clocks can run at different speeds.
* Notifiers are simple
* Notifying is cheap/fast
* The network is cheap/fast

### Non-requirements

* No need to handle malicious nodes
* Nodes are in a clique topology.
  No need to handle non-clique topologies.


## High-level overview

TODO: Pick a better name for `peer`, as whether someone/something is a peer or not is a statement about their relationship with someone/something else, not about what they themselves are.

TODO: Saying "Any peer will consider .. if .." sounds as if you're defining "will consider" by the "if", i.e. people will read this as an if then else. However, the first and third statements both "define" what it means for a peer to believe that a notifier is live (i.e. as "I've had contact recently" vs. "at least one of the peers has had contact recently"). The best way to fix this is probably to take away one of the statements, and merge what is behind the two "if"s, i.e. "Any peer will consider the notifier live if ... or ...".

* Any notifier promises to notify any peer on a regular basis.
  Any peer will consider the notifier live if it has heard from the notifier recently.
* Peers promise to synchronise with their peers on a regular basis.
  Any peer will consider another peer live if it has heard from the peer recently.
* Peers synchronise information about when each of them has last heard from notifiers.
  Any peer will consider a notifier live if any of the peers that it considers
  live has recently heard from the notifier.
* Nodes are considered dead if no mechanism above causes it to be considered live.
* The appropriate administrator is alarmed when a node is considered dead.

## Building up the algorithm

### One peer, one notifier

The peer stores:

* How to alarm the administrator in the event that the notifier dies
* The notifier timeout $T$
* When the peer last heard from the notifier: notifier time stamp $t$

Then:

The notifier notifies the peer every $N$ seconds such that $N$ is (significantly) less than $T$.

When the notifier contacts the peer, the peer updates its time stamp $t$ to the time stamp that the request was received.

Every $L$ seconds, the peer checks if the notifier is still considered live:

$$
    t_{now} < t + T
$$

If it is not considered live, the peer concludes that it is dead and alarms the administrator.

#### Processing delay

We assume that the combined lag from all the processing and latency involved in the above $d$ takes less than $T-N$ seconds.

$$
d < T - N
$$

In other words:

* The notifier starts making requests more or less on time.
* It does not take the notifier much time to make a request.
* It does not take the network much time to transfer the request.
* And it does not take the peer much time to process the request.

This allows us to conclude that the notifier may be considered dead if the peer has not heard from it in over $T$ seconds.

TODO: Add note here. Your assumption isn't actually true, because almost any practical probability distribution will not have a maximum, and neither do I expect this one to. The point, rather, is that if d >= T - N, that considering the node/peer dead is actually appropriate.

#### Choosing $T$ and $N$

If $N$ is chosen too small, the notifier has to spend a lot of extra processing time on notifying the peer.
If $T$ is chosen too large, it will take a long time to alarm the administrator after the notifier dies.

If $N$ is chosen too close to $T$, such that the combined time of the
processing listed above takes longer than $T-N$, then the notifier may briefly
be considered dead by the peer, even though it is still live but slow.

Luckily, administrators are typically _much_ slower to react than any nodes in this system.
While it typically takes less than a second for a request to be processed, it
will typically take an administrator minutes to respond to an alarm.

This means that there is a sweet spot for $T$ between the administrator's latency and the request processing latency.
This is usually a large enough amount of time that $N$ can be chosen as a comfortable fraction of $T$, such as $T/2$.
That way the time spent notifying peers is a negligible fraction of processing time.


#### Choosing a clock

The choice of clock matters because certain clocks can break the entire system.
For example, if we naively choose a non-monotonic clock like the wall clock to record time stamps, both false positives and false negatives become more likely.

Wall clocks often have daylight savings time, which means the clock is turned forward or backward twice a year.
When the clock is turned forward by an hour, the notifier will instantly look dead even though it is not.
Even worse, when the clock is turned backward by an hour, the peer will become blind to the notifier's death for an hour.

Instead, nodes need to use monotonically increasing clocks.
In particular, those clocks need to be monotonically increasing across node restarts.
Otherwise you have the same problem as with daylight savings time, but on every restart.

#### Recording timestamps

To avoid that nodes would have to synchronise their clocks, we must carefully
consider which node's clock is used to record a timestamp.
In this section, all timestamps will be recorded using the peer's clock.
In the next section, each timestamp will be carefully denoted to come from a specific node's clock.

Comparing timestamps from different clocks is not allowed.
Computing durations using timestamps from different clocks is now allowed.
Comparing durations from different clocks is also not allowed.

#### Definitions

* $T$: Notifier timeout
* $t$: Notifier "last heard" timestamp, i.e. when the last (most recent)
  notification from the notifier was recorded by the peer.
* $N$: Notifier period
* $L$: Looper period
* $t_{death}$: The time that the dead notifier died, on the peer's clock
* $t_{last}$: The last (most recent) time that the notifier contacted the peer, on the peer's clock.
  Note that this value is unknowable, because a request could arrive at the peer arbitrarily later than it was sent. 

#### Proof: A live peer will eventually consider a dead notifier dead

We prove this by proving the peer will consider a dead notifier dead as of $t_{death} + T + d$.
In other words: the liveness condition will be false:

$$
\neg (t_{now} < t + T)
$$

If a node considers what time it is "now", after $t_{death} + T + d$, then it will find:

$$
\begin{align}
t_{death} + T + d < t_{now}  && \text(1)
\end{align}
$$

Consider a notifier that last sent a notify request to the peer at $t_{last}$.
The peer recorded its "last heard of" time stamp $t$ shortly after:

$$
\begin{align}
t_{last} < t < t_{last} + d  && \text(2)
\end{align}
$$


Because a dead node cannot notify anyone, its last notification must have been sent before its death.

$$
\begin{align}
t_{last} < t_{death} && \text(3)
\end{align}
$$

Combining these three, we find the following:

$$
\begin{align}
    t_{death} + T + d &< t_{now}   &  \text{(1)}  \\\\
     t_{last} + T + d &< t_{now}   &  \text{(3)} \\\\
     t_{last} + d + T &< t_{now}   &  \text{(rearrange)} \\\\
                t + T &< t_{now}   &  \text{(2)}
\end{align}
$$


But this is in contradiction of what it means for the node to consider the notifier live:

$$
t_{now} < t + T
$$

$$
\blacksquare
$$

#### Proof: The peer will consider a live notifier live

We prove this by proving that it is impossible that the peer considers a live notifier dead.

As before, it takes a short time $d$ to send a request and record its time stamp $t$.

$$
\begin{align}
t_{last} < t < t_{last} + d  &&  \text{(1)}
\end{align}
$$

In particular, $d$ is smaller than the difference between $T$ and $N$, and all three are positive:

$$
\begin{align}
d &< T - N  &  \text{(2)} \\\\
d &> 0      &  \text{(3)}
\end{align}
$$

A live node is defined as one that has recently sent a notify request.

$$
\begin{align}
t_{last} < t_{now} < t_{last} + N && \text (4)
\end{align}
$$

Combining these three, we find the following:

$$\begin{align}
t_{now} &< t_{last} + N & \text{(4)} \\\\
        &< t + N        & \text{(1)} \\\\
        &< t + T - d    & \text{(2)} \\\\
        &< t + T        & \text{(3)} \\\\
\end{align}$$

But this is in contradiction with the node considering the notifier dead:

$$
t_{now} >= t + T
$$


$$
\blacksquare
$$


#### Notifying the administrator

TODO rename this to alarming the administrator.

Notifying a human administrator is a very important aspect of a system like this.
Indeed, the service that we rely on for the notification needs to be more robust than the failure detector.
For maximum robustness, multiple options will have to be provided.

In any case, the specifics of how to contact the administrator are out of scope for this explanation.
This explanation only concerns itself with "when do we contact the administrator?" and "who contacts the administrator?"

The question of "who?" is easy to answer in this section: the peer.
This will get more complicated in the following section.

As for the "when?" question;
The peer will need to run a periodic background job.
This job is simple: alarm the administrator if the notifier is currently considered dead.

#### The administrator will eventually be alarmed about a dead notifier

In a previous section we proved that the peer will consider a dead notifier dead latest after $t_{death} + T + d$.
If the background job for alarming administrators about dead notifiers runs every $L$ seconds, then we can conclude that an administrators will be notified latest after $t_{death} + T + d + L$.

$$
\blacksquare
$$

#### Issues

The biggest issue with this approach is that the administrator will not be alarmed if the peer and the notifier are dead at the same time.
In the next section, we will use multiple peers to make sure that all but one of them may die without disrupting the service.


### Multiple peers, one notifier

Each peer stores:

* How to alarm the administrator in the event that the notifier dies
* The notifier timeout $Tn$

Each peer $p$ now also stores for each peer $q$:

* How to alarm the administrator in the event that peer `q` dies
* The peer timeout $Tp_q$
* When it last heard from the other peer, both when the request arrived (on its clock) $tp_{p, q, p}$ and when the request was sent (on their clock) $tp_{p, q, q}$.
  The notation $tp_{x, y, z}$ denotes "The last time peer $x$ heard from peer $y$, on peer $z$'s clock."
* When that peer last heard from the notifier: notifier time stamp $tn_{p}$


TODO: We can tell if the request (notifier to peer) succeeded or not (because HTTPS), so we don't need to handle the case of the notification failing without the notifier knowing.

Then:

The notifier notifies a peer every $N$ seconds, in round-robin fashion.
If that notification fails because the peer is down, it immediately notifies the peer next-in-line.

When the notifier contacts a peer, the peer updates its time stamp $tn_{p}$ to the time stamp that the request was received.

Every $S$ seconds, every peer $x$ synchronises with every other peer $y$ they
know about and shares all time stamps $tp_{x, y, x}$, $tp_{x, y, y}$, as well as $tn_{x}$.
Peers save the time stamps they learn about using the maximum function to solve
conflicts.
When any peer $x$ synchronises with another peer $y$, they each update their
"last heard" timestamps to the current time.
In other words, peers "hear from" each other when they synchronise.

Every $L_p$ seconds, every peer checks if every other peer is still considered live.
If not considered live, the peer concludes that it is dead and alarms the peer's administrator.

Every $L_n$ seconds, every peer checks if the notifier is still considered live.
If it not considered live, the peer concludes that the notifier is dead and
virtually elects a leader to alarm the administrator.

#### TODO: Definitions, version 2

* $Tn$: Notifier timeout
* $Tp_{x}$: Peer $x$'s peer timeout
* $tn_{x}$: Notifier "last heard" timestamp, i.e. when the last (most recent)
  notification from the notifier was recorded by peer $x$ (on peer $x$'s clock).
* $tp_{x, y, x}$: Peer "last heard" timestamp, i.e. when the last (most recent)
  notification from peer $x$ was sent to peer $y$, on peer $x$'s clock.
* $tp_{x, y, y}$: Peer "last heard" timestamp, i.e. when the last (most recent)
  notification from peer $x$ was recorded by the peer $y$, on peer $y$'s clock.
  (Note that $tp_{x, y, x}$ would have been recorded after $tp_{x, y, y}$ on some universal clock, but their numerical values do not have to be ordered either way.)
* $tn_{death, x}$: The time that the dead notifier died, on peer $x$'s clock
* $tp_{death, x, y}$: The time that the peer $x$ died, on peer $y$'s clock
* $t_{now, x}$: Right now, on peer $x$'s clock.
* $tn_{last, x}$: The last (most recent) time that the notifier contacted peer $x$, on peer $x$'s clock.
* $tp_{last, x, y, x}$: The last (most recent) time that the peer $x$ contacted peer $y$, on peer $x$'s clock.
* $tp_{last, x, y, y}$: The last (most recent) time that the peer $x$ contacted peer $y$, on peer $y$'s clock.

* $N$: Notifier period
* $S$: Synchronisation period
* $Ln$: Notifier Alarmer period
* $Lp$: Peer Alarmer period
* $d_n$: Notifier lag
* $d_p$: Peer lag

#### Peers as notifiers

Now that there are multiple peers at play, we can consider each peer a notifier
for each other peer by viewing synchronisation as a notification mechanism.

As such, both proofs above apply, which means that all live peers will consider
all other live peers live, and will eventually consider all dead peers dead.

In particular, that means that any peer $x$ considers another peer $y$ live if peer $x$ has heard from peer $y$ recently:

$$
    t_{now, x} < tp_{x, y, x} + Tp_{y}
$$

Note that we do have to assume that clocks run at the same speeds on different peers, otherwise it wouldn't make sense to add $Tp_{y}$ to a timestamp on $x$'s clock.
(In practice this requirement is not as strict as it sounds, but in theory it is.)

#### Synchronisation

The mechanism of synchronisation is detailed here because it involves many important timestamps.

When a peer $x$ synchronises with another peer $y$, the following things happen in order:

1. Peer $x$ records $tp_{y, x, x}$ (for peer $y$'s benefit) and puts it in a sync request for peer $y$.
2. Peer $x$ sends the sync request to peer $y$.
3. The sync request arrives at peer $y$.
4. Peer $y$ records $tp_{y, x, y}$
5. Peer $y$ processes the sync request by saving all timestamps on its end.
6. Peer $y$ records $tp_{x, y, y}$ (for peer $x$'s benefit) and puts it in the sync response for peer $x$.
7. Peer $y$ sends the sync response to peer $i$.
8. The sync response arrives at peer $x$.
9. Peer $x$ records $tp_{x, y, x}$

Note:

* Peers can only record timestamps on their own clock.
* The order in which these events occur is well known, but the numerical order of timestamps on different clocks can be any.
  If "$x$ happens before $y$" is denoted as $x \prec y$, then we know:

  $$
    tp_{y, x, x} \prec tp_{y, x, y} \prec tp_{x, y, y} \prec tp_{x, y, x}
  $$

  but numerically, we don't know anything about timestamps that come from different clocks.

* Sync requests and responses might have been sent at the same time, or in
  reverse, so we **cannot** conclude following inequalities in general:

  $$
    tp_{y, x, y} < tp_{x, y, y}
  $$

  $$
    tp_{y, x, x} < tp_{x, y, x}
  $$



#### A dead peer is one that does not look live

The way a peer decides whether the notifier is live now changes to account for:

1. The notifier may have contacted another peer to say that it is still alive
2. Peers may have died.


Any peer $x$ now considers the notifier dead if it has no indication that the notifier may be live.
The two ways that it may consider the notifier live are:

1. It itself has heard from the notifier directly:

   $$
       t_{now, x} < tn_{x} + Tn
   $$

2. Any other live peer has heard from the notifier recently and synchronised since:

   $$
       \exists y.  (t_{now, x} < tp_{x, y, x} + Tp_{y}) \wedge (tp_{x, y, y} < tn_{y} + Tn)
   $$

   In other words: $x$ thinks that $y$ is live and $y$ had heard from the
   notifier recently right before the last time that $x$ and $y$ synchronised.

   Note that both of these comparisons make sense.
   On the left they compare timestamps on $x$'s clock.
   On the right they compare timestamps on $y$'s clock.

The peer $x$ considers the notifier live if it has heard from the notifier _or_
if it thinks that any of its live peers has heard from the node recently.

Note that we can let every node synchronise with itself to get rid of the first option.
(Alternatively, we can define $tp_{x, x, x}$ to always equal $t_{now, x}$.)
So, without loss of generality, we can shorten the liveness condition to:

$$
       \exists y.  (t_{now, x} < tp_{x, y, x} + Tp_{y}) \wedge (tp_{x, y, y} < tn_{y} + Tn)
$$

#### Proof: All live peers will consider a dead notifier dead


TODO: Finish this argument, Nick already gave you what you need.

We prove this by showing that each live peer $y$ will consider a dead notifier dead after $tn_{death} + Tp_{y} + L_p + d_{p}$.
Now, since
$$
(P => Q) = (\neg Q => \neg P)
$$
instead of proving that "if the notifier is dead, all live peers know it's dead", we will prove that, if one peer believes the notifier is alive, that it actually is alive.
Imagine one peer `p` believes the notifier is alive.
This means, by definition, that
$$
       \exists y.  (t_{now} < tp_{p, y} + Tp_{y}) \wedge (tp_{p, y}' < tn_{y} + Tn)
$$

Let us call such a peer `y`. It is then true that
$$
(t_{now} < tp_{p, y} + Tp_{y}) \wedge (tp_{p, y}' < tn_{y} + Tn)
$$

Now since `tp_{p,y}'` is the time at which `y` notified `p` (according to clock `y`) that the notifier has contacted it at time `tn_y` (according to clock `y`), it is true that
$$
tn_y < tp_{p,y}'
$$

We therefore know that
$$
tn_y < tp_{p,y}' < tn_{y} + Tn
$$



#### Old version: All live peers will consider a dead notifier dead

We prove this by showing that each live peer $y$ will consider a dead notifier dead after $tn_{death} + Tp_{y} + S + d_{p} + d_{n} $.

$$
tn_{death} + Tp_{y} + S + d_{p} + d_{n} < t_{now}
$$

Consider any peer $x$, such that the following argument may be applied to all peers.

The peer $x$ wrote down the last time it was contacted by the notifier as $tn_{x}$.
The time that the notified sent the request happened before that, but no more than $d_n$ before that.

$$
\forall x. t_{last, x} < tn_{x} < t_{last, x} + d_{n}
$$


Because a notifier node cannot notify anyone, its last notification to any node must have been sent before its death.

$$
\forall x. tn_{last, x} < tn_{death, x}
$$

Here, $tn_{death, p}$ is the moment at which the notifier died, according to $p$'s clock.


<!--

We know from a previous section that any peer $x$ can correctly tell that a peer $y$ is dead after $tp_{death, y, x} + Tp + d_{p}$ using its time stamp:

$$
\neg (t_{now, x} < tp_{x, y, x} + Tp_{y})
$$

This means that at any time after that, that will still work.

For any live peer $y$, we know it will have recently synchronised with $x$, at $tp_{last, x, y, y}$.

$$
\forall y. tp_{last, x, y, y} < t_{now, y} < t_{last, x, y, y} + S
$$

And not long (maximum $d_{p}$) after, their "last heard" time stamp was updated.

$$
\forall y. ts_{last, x, y, y} < tp_{y, x, y} < ts_{last, x, y, y} + d_{p}
$$

The reverse is also true: $x$ recently synchronised with $y$ as well.

$$
\forall y. tp_{last, y, x, x} < t_{now, x} < t_{last, y, x, x} + S
$$

$$
\forall y. ts_{last, y, x, x} < tp_{x, y, x} < ts_{last, y, x, x} + d_{p}
$$

-->

For any live node $y$, we can now put these together:

$$
\begin{align}
       tn_{death} + Tp_{y} + S + d_{n} <& t_{now} \\\\
       tn_{last, y} + Tp_{y} + S + d_{n} <& t_{now} & \text{ because } \forall p. tn_{last, y} < tn_{death, y} \\\\
       tn_{last, y} + d_{n} + Tp_{y} + S <& t_{now} & \text{(rearrange)} \\\\
       tn_{y} + Tp_{y} <& t_{now} & \text{ because } tn_{y} < tn_{last, y} + d_{n}
\end{align}
$$


Combining the detection of dead peers with the above, we get:


$$
       \forall y.  (tp_{x, y} + Tp_{y} < t_{now}) \vee \neg (tp_{x, y} < tn_{y} + Tn)
$$

$$
       \forall y.  \neg (t_{now} < tp_{x, y} + Tp_{y}) \vee \neg (tp_{x, y} < tn_{y} + Tn)
$$

$$
       \forall y.  \neg ((t_{now} < tp_{x, y} + Tp_{y}) \wedge (tp_{x, y} < tn_{y} + Tn))
$$

$$
       \neg \exists y.  (t_{now} < tp_{x, y} + Tp_{y}) \wedge (tp_{x, y} < tn_{y} + Tn)
$$

But this is exactly the opposite of what it means for the node to consider the notifier live:

$$
       \exists y.  (t_{now} < tp_{x, y} + Tp_{y}) \wedge (tp_{x, y} < tn_{y} + Tn)
$$

$$
\blacksquare
$$

#### All live peers will consider a live notifier live

We prove this by showing that no live node can consider a live notifier dead.

TODO proof

### Multiple peers, multiple notifiers

Every notifier is independent, so we can apply the above approach here as well, by namespacing all time stamps to the appropriate notifier.

TODO write out the summary.
