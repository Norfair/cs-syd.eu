---
layout: post
title: Working from home? Wear Clothes!
tags: productivity,work
---

Working from home is an increasingly available option where it used to not be possible at all.
Unfortunately that also means that not as much is known about working from home.
An entirely new set of best practices is required.

<div></div><!--more-->

One of the main advantages of working from home is that you don't have to get dressed.
You can just wear your pyjama's all day.

Unfortunately, working in your pyjama's is not as good an idea as you may originally have thought.
Yes, you save yourself the time it takes to get dressed and you may be much more comfortable, but the hidden disadvantages are overwhelming.

We have been trained to get ready to sleep when we wear our pyjama's.
As a result, working in our pyjama's has a habit of making us rather unproductive/lazy.

When you work in your pyjama's, you're telling your brain that "It's all right. We're going to bed soon. You can doze off now.".

We need to make it a habit to tell yourself to get our clothes, go to the bathroom, wash up and get dressed.
Then get back to work.
