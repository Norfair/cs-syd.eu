---
title: How to get less email
tags: email
---

[I have written about how to manage your email in a stress-free way](/posts/2014-12-21-stress-free-mailboxes.html).
That blog post assumed that you get a lot of email and it treated the symptoms rather than the source of the problem.
In this blog post I will share my favourite ways to get less email.

<div></div><!--more-->

### Why less email?

In some sense, it feels good to receive a lot of communication.
It can make you feel important.
It can be very comfortable to be 'busy', even if you aren't really being productive.
However, the time you spend reading and replying to emails could be spent more productively.


### Consider not having an email address

The best way to not get email is to not have an email address.
There would be no possible way to get any email.

Please consider this option before you read on.


### Getting less email

The email you get can be subdivided into [two categories](/posts/2014-11-02-the-hidden-at-least):

- The email you get because you exist and do things.
- The email that you get because you send email.

You can tackle both.

#### Turn off email notifications

If your phone buzzes every time you get an email, not only will you be interrupted a frequently, you will also be much more likely to respond to those emails.
If your correspondents know that you will see every email that you get immediately, they will use email for direct communication as if it was a text message, and you will receive a much more email.


#### Turn off automated email from the services you use

Software services often use emails [to get your attention back](https://cs-syd.eu/posts/2016-05-29-reducing-the-number-of-slotmachines.html).
Most of those also have a settings menu where you can turn off these notifications.
Turning off these notifications can get rid of a lot of email.


#### Use spam filters and blacklists more aggressively

Email providers actually have built-in mechanisms to deal with email that you don't want.
I have not found it very effective to 'train' the filters by clicking 'this is spam for me', but blocking recipients has been very effective.


#### Send less email

A lot of the email you get are replies to email you sent.
Reducing the amount of email you send will therefore reduce the amount of email you get.

Sometimes email is used to answer questions to which the answer can help many more people than just the person sending the email.
[Email is where keystrokes go to die](https://www.youtube.com/watch?v=IWPgUn8tL8s), so instead of writing a long reply to a question, consider blogging about the answer and sending the link as a reply.
This way your reply will reach many more people.


#### Reply more slowly

If you cannot get around answering an email, consider waiting a few days to answer it.
This way you can at least not get any replies to your reply until then.


#### Receive email less frequently

If you have turned off your email notifications, now you have turned your email client back into a slot machine.
You can fix this by only having your email client download the email X times per day, where you can set X to be as small or as large as you want it to be.
The smaller you can make X, the less frequently you will be able to be distracted by your email.
This idea ensures that you can only send at most one reply per person per 24/X hours.
It will also ensure that you cannot have quickfire conversations via email that you should really call about anyway.

#### Send email less frequently

This is the latest trick that I discovered, and it has made a big difference.
You can configure your system so that any email that you want to send gets placed in a queue, and the email only gets sent when you 'flush' the queue.
This way you can ensure that your email only gets sent Y times per day and this has a number of benefits.
There is a build-in lag between when you write your email and when you send it.
This lag will ensure that you will not feel an artificial sense of urgence to reply.
Lastly, the lag will teach anyone who sends you an email to not expect a reply within 24/Y hours.
The smaller you can make Y, the less stressful email gets.

Sometimes an email can make us angry, and tempt us to send a reply that would not be in our best interest.
In that case this lag has nice side effect that you can now write your furious reply, to get the satisfaction of having done so, before deleting it and sending a nicer email.

You can do this with [offlineimap and msmtp-queue](https://wiki.archlinux.org/index.php/msmtp#Miscellaneous) and it has the added benefit that you can 'send' email when you do not have an internet connection.
Of course you can set it up so that you can still bypass the automation in emergencies.
