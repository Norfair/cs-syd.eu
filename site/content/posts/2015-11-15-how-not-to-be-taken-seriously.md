---
layout: post
title: How not to be taken seriously.
tags: communication, annoyance, definitions, human language,
---

While trying to engage in meaningful, insightful discussions, I have often caught myself not taking the other person seriously.
I've been trying to pin down why this would be the case, so that I may reflect on the matter.

<div></div><!--more-->

Keep in mind that the greatest problem with these examples is that there is no good way to figure out the _real_ meaning of what is said.
One would have to make assumptions that are detrimental to the discussion if the following points were ignored.

Before you read on, I would like to clarify that my intention is to understand what my conversation partner is saying.
I am genuinely interested and my intentions are friendly.
It is precisely _not_ my intention to be purposefully annoying.

Here is a list of ways to not be taken seriously.

#### Use meaningless words/buzzwords.
After [the post about definitions](http://cs-syd.eu/posts/2014-09-21-confusing-definitions), it quickly became clear to me how few words have a good definition and how many words are used without them being defined at all.

Words without a definition don't have a clear meaning.
Because in discussions we try to convey ideas and information clearly, one could argue they have no useful meaning at all.

Examples of these kinds of words and phrases are
"a dynamic person",
"an open atmosphere",
"premium",
"bio",
"open",
"drugs",
"smart",
"natural",
"some",
etc...


#### Use words with inflated semantics.
Using words generously too many times will diminish their meaning.
In meaningful discussions, using words that have gone through this process can confuse your conversation partner as they cannot tell whether you are using the word in the true, original sense or in the new, meaningless sense.
One then has to assume that their conversation partner is trying to use the word in the original meaning of the word and that will come with great annoyance if they are not.

Examples of words with this problem:
"awesome",
"unique",
"totally",
"basically",
"incredible",
"really",
"very",
"absolutely",
"unbelievable",
"global",
"famous",
"ultimate",
"terrific",
"literally",
"can",
"perfect",
etc...


#### Use sentences that are manifestly false.
The implication of using a sentence with a universal quantifier $\forall$ is that one only needs to provide a single counterexample for the entire sentence to be false.

Examples of such sentences:

- Everyone has two hands.
- Nobody thinks that the earth is flat anymore.
- It always rains on November 2!

#### Speak depth-first.
An argument naturally follows a tree-structure.
That is why we use sections, subsections and subsubsections as we do.

The way we traverse that tree is a crucial part of transferring the information.
Speaking in [depth-first](https://en.wikipedia.org/wiki/Tree_traversal) order is a surefire way to confuse your conversation partner.

Coming up with a good example of this point is hard because this usually only happens in speech.
A good sign that this is happening is that the discussion seems to go completely off topic after a few sentences while the speaker still thinks they are explaining themselves.

#### Use words with a precision higher than you can support.
Again, one has to assume that their conversation partner is saying what they are on purpose.
Using words that indicate a higher precision than the speaker is willing to commit to, will case the conversation partner to wrongly assume that higher precision.

Examples:

- using 'in two days' to mean 'soon'
- using '90%' to mean 'most'
- using 'at 12:00' to mean 'around noon'


#### Use examples if you cannot word the abstract idea.
Making a convincing argument requires you to be able to express the core idea _before_ you give any examples.
Giving examples first, or only, can confuse your conversation partner because examples are rarely generally applicable enough to get the idea across.
Moreover, they are usually much more easily contradicted or their point circumvented.

This situation can be recognized by hearing the equivalent of "Oh, but you know what I mean!"

#### Use logical connectives incorrectly and/or make logically insensible claims.
Using logical quantifiers and connectives correctly is vital if you want to engage in intelligent discussions.
Using them wrong makes it seem as though you are either ignorant or too emotionally unstable to concentrate on the discussion.
Your conversation partner then has to correct your logical mistake, or ensure you meant something other than what you said otherwise, which distracts from the discussion at hand.

Example sentences:

- "I always bring an umbrella when it rains, but it didn't rain so I didn't bring an umbrella."
- "All birds are not penguins." (instead of "Not all birds are penguins.")




