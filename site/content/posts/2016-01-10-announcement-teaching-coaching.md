---
layout: post
title: "Announcement: Teaching/Coaching"
tags: teaching,programming
---

Today I am announcing my latest ambition.
This year I will be teaching programming to a select few students so that they may experience the wonderful world of code.

<div></div><!--more-->

### Motivation

Programming is not sufficiently taught in High schools.
To study programming, we either get a higher education or we learn it ourselves in our spare time.

The problem remains that programming is very hard to get into.
Knowing where to start looking is arguably the hardest part.

That is why I have decided to coach a few people into programming.
I'll help them take the first few steps and guide them through the initial struggles.


### The plan

The goal is threefold.

- Show the student what programming is like.
- Get the student to write some toy software for themselves.
- Get the student up and running to the point where they know where to go if they want to learn more.

The coaching sessions are entirely *free of charge*.

### The student profile

- You have never programmed before.
- You are *not* enrolled in any education where programming is essential.
- You are motivated and genuinely curious.
- You are enthusiastic about maths.
- You read and write English.
- You know how to communicate via email.
- You are willing to spend at least 4 hours/week on this project.
- You have regular access to a computer (just a phone is fine too, but harder).

### Applications

To apply to be one of the students, [send an email to me](/contact.html), with the following information:

- A description of your background in maths.
- A description of your background in computing.
- A description of the computer system you will be using. (1. Operating system, 2. whether it's a phone, a laptop or a desktop, 3. whether it's yours personally)

These pieces of information are only for me to better tailor the way I explain the material to the student.
There is no requirement to have any background in either maths or computing.
We can work from the ground up if need be.
I happen to also like to teach maths.

I do *not* need to know:

- Who you are.
- Your race.
- Your gender.
- Your veteran status.

Make sure the subject of your email is `[Programming Student Application]: your name here`

The deadline is February 14 2016.
