---
title: 2017; Year in review
tags: review, year
---


2017 started off busy as usual.
I had moved into a new apartment in Zürich with a friend.
I was a full-time computer science student at ETH, Zürich.
I had just set up a Legal entity that would allow me to do freelancing work on the side while studying.

<div></div><!--more-->

### Highlights

- I took 16 flights between Switzerland, Belgium, the UK and Germany.
- I lost about twenty pounds.
- I taught Functional programming at ETH as a teaching assistant for the second time.
- I had my first experiences leading people on software projects.
- I spent a few months writing Haskell at ETH at the department of architecture.
- I wrote a thesis on a topic I proposed myself: signature inference for property discovery.
- I gave seven tech talks.
- I finished studying and now officially hold a degree as a master in computer science.
- I declined an offer for my thesis work to be continued in my own funded company.
- [I collected 65 new failures.](/cv/fail.pdf)
- I started working at [FP Complete](https://fpcomplete.com), fully remotely, doing exactly the kind of work I enjoy.

2017 was the best year of my life so far, continuing the tradition since 2012.


### Job search

In 2017 I started looking for a full-time job for the first time.
I learnt that rejection will occur much more frequently than I expected, and that this is nothing to be afraid or ashamed of.
My search took eight months and I got rejected by about fifty companies. 
By some even multiple times.
Eventually I found my dream job, where I now love my work.

### Health

Having only one full-time job allowed me to start looking into what it means to live healthily.
I started spending a lot of time investigating nutrition, exercise and sleep.
I plan too continue to do so in 2018.

### Focus for 2018

For 2018, the plan is to focus on health, human interaction, productivity and technical advancement, in that order.
