---
layout: post
title: Be careful recruiters, you're being selected too!
tags: work,recruitment
---

My studies are slowly reaching their end and I'm having more and more experiences with recruiters.
Companies as well as universities make applicants jump through so many hoops that they seem to forget that they are being selected as well.

<div></div><!--more-->

I get to see both sides of the story because someone close to me is in recruiting.
Let me first say that there are many more bad candidates than there are bad recruiters.
I just hope that recruiters make an effort not to treat every candidate as a bad candidate.
Treat a candidate with the same professional respect with which you would treat a coworker.
They may just become one.

I have fairly high standards, both for myself and of my work.
My workplace is important to me. I will be representing it and investing in it.
I therefore respectfully reserve the right to reject any company that I've applied to based on our interactions during the selection process.
In the same way I respect that companies retain high standards with respect to their candidates.


### Make sure you know what you're doing

Recruitment is not at all something you should be doing without the proper training.
Unfortunately, many recruiters are not educated to be a recruiter.

To be a recruiter, more is required than just knowing how you find candidates.
You need insight in the human psychology and the valuable skill of communication.

As a recruiter, you are the first person an outsider will be in contact with.
For the duration of the selection of the candidate, whether it should be successful or not, you are the face of the company.


### Listen to yourself

Be consistent in what you say.
Don't tell me "You can ask any remaining questions via email" and then reply to my email with "We won't be going forward with this application. We expected these questions during the conversation."


### Write names correctly

If you can't muster the effort to make sure you write my name correctly in an email, how do you expect me to come work for a company that hired you?
I have standards too.
Oh, and don't tell me `Mr van der Kechkov` is a typo that should have been `Mr Kerckhove`.


### Make sure your system works

When I go to a company's website to apply, or to find a recruiter's email address to email my application to, I expect that your website works.
Of course websites go down every now and then, but this particular site was offline for weeks.
I'm looking at you, NASA.
Needless to say, I won't be applying to work, as a software engineer, for a company that fails to keep its website online.


### Keep to your own deadlines

This goes back to [my opinion on appointments](http://cs-syd.eu/posts/2015-10-25-the-responsibility-of-an-appointment.html).
If you tell me that you'll have an answer by Monday, take responsibility for that appointment.
Don't send me an email the next Friday.
The least you could do is write me an email saying you don't have an answer yet.
Don't expect me to be waiting for your email desperately.

### Don't make candidates do pointless mechanical work just to filter out the motivated ones

The initial part of most applications consist of three parts:

- Upload your CV
- Upload a motivation letter
- Fill in a few hundred text boxes with the information that was on your CV anyway.

There are two problems here.

If you expect me to spend hours on my CV and a motivation letter, you had better spend more than seven seconds looking at them.
The good candidates, the ones you want to hire, have a better sense of time management than to spend hours filling out your application form.

The same holds for all the informational text boxes.
I get that you want to filter out all the people who aren't motivated enough to do these menial tasks.
Unfortunately you're also filtering out the people that aren't desperate enough because they have other options.


