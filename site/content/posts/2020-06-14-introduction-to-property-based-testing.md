---
title: A language-agnostic introduction to property-based testing
tags: Haskell, property testing
---

This article was my second commissioned article, for Human Readable.
Head over there to read it for free during the quarantine:

<div></div><!--more-->

<a class="button is-primary" href="https://humanreadablemag.com/issues/3/articles/property-based-testing-intro">
  Read more
</a>

