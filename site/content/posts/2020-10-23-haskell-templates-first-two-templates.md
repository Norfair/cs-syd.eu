---
title: Haskell Templates: First two templates available
tags: Haskell, templates
---


Earlier this month, I [wrote a post](/posts/2020-10-08-haskell-templates-validation) about validating the idea of selling Haskell templates.
In the time since then, I have made and sold multiple copies of [the first template](https://template.cs-syd.eu/template/NorfairKing/template-optparse) at https://template.cs-syd.eu.

<div></div><!--more-->

Today I am releasing the second template: A [command-line tool](https://template.cs-syd.eu/template/NorfairKing/template-cli).

The template features everything you need for a command-line tool project anyway:

* Haskell code for a multi-command CLI tool
* Per-command integration tests
* End-to-end tests
* Option parsing & Option parsing tests
* Nix build
* CI
  * Stack-based CI
  * Nix-based CI
* Pre-commit hooks

The template also comes with a readme that explains how to use the template to hit the ground running quickly.
This way you can get started with your business logic in under 30 minutes instead of taking hours to build everything yourself.

<a class="button is-link" href="https://template.cs-syd.eu">
  Read more
</a>
