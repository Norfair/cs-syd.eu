---
title: Self-management with Smos: Archiving
tags: smos, self-management
---

This is the second post about using Smos for self-management in practice.
It explains how to deal with finished projects.

<div></div><!--more-->

### Keeping data about completed projects

When completing a project, you may be tempted to just delete the file, but this is less than optimal for a number of reasons:

- You may need to review what happened later.
- You may need to keep track of how much time you spent (see [Clocking](https://smos.cs-syd.eu/smos-query/clock)).
- You rob yourself of the dopamine that comes from finishing the project and seeing the project as done.

So we want to keep the data around, but still out of the way so that it does not show up between all the unfinished projects.

What you can do using `smos-archive` is to both get the project out of the way _and_ keep the file around in an archive.

### The `smos-archive` tool

When you finish a project, simply run `smos-archive /path/to/foobar.smos` to archive it.
If any entries inside the file are left unfinished, or any open clocks remain, you will be prompted to cancel them all automatically first.

<script
  id="asciicast-350170"
  src="https://asciinema.org/a/350170.js"
  async>
  data-autoplay="true"
  data-loop="1"
  data-cols="80"
  data-rows="25">
  </script>

The file is then moved to the `archive` directory within your workflow.
The `smos-query` reports then automatically either ignore the archive or do not, depending the appropriate default.

For example, when gathering clocking data, the archive is not ignored but when gathering the next action report, the archive is ignored.


### References

For more information about Smos, see [the documentation site](https://smos.cs-syd.eu/smos-archive).
