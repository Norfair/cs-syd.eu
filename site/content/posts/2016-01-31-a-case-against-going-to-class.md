---
layout: post
title: A case against going to class
tags: school, university, class
---

Few decisions in my recent past have been as controversial as the decision not to go the most classes at university.
This post describes my personal reasoning behind this decision.

<div></div><!--more-->

Note that these arguments concern university classes for mathematics and computer science courses.

### An entirely outdated system

One person standing in front of a class of 25-300 people speaking on a subject they have more expertise in.
That concept is hundreds of years old and it has barely changed.

The concept of one-to-many teaching is massively outdated.
Not to mention that getting information from just that one source is an anti-pattern.
The education system is in dire need for an update.

### The right pace

This configuration of teaching has the inherent problem that the teacher can only be at the right pace for at most one student, assuming that everyone learns at a different rate.
This means that I am usually either bored in class or hopelessly lost, but rarely following along.


### The university problem

No-one has ever really explained the intended function of a university to me.

I've gotten as far as this:
To provide higher education, experts in their field are required to explain what only they know well enough.

In recent years the work pressure for these professors has increased massively.
As a result, a lot of professors don't have time to, or don't like to teach.

I've been in classes where the professor was just reading the slides and ignoring every student.
I've been in classes where the professor quit teaching after half of the class, running out as if from a fire.
These were bad, but the worst part is that most professors don't seem to make any effort to improve their didactic skills.

As a result, classes are generally of bad didactic quality.

### Maybe you'll miss something important!

No. No I won't.
Mathematics and computer science both have two inherent advantages.

1. Exams are usually graded in an objective manner.
   Any solution is either correct or incorrect, there is little discussion possible and no opinions are involved.
2. All the course material is plentifully available online.

[stackoverflow](http://stackoverflow.com), [the math stack-exchange](http://math.stackexchange.com/) and [Coursera](https://www.coursera.org/) are abundant resources of knowledge and inter-personal learning.
If that wasn't enough, for any recent subject, the best universities in the world publish all their lecture material online.
This gives you plenty of sources of learning material and, in the case of recorded lectures, lets you learn at your own pace (e.g. 2x speed hamster-like sounding professors).

### Conclusion

Now you know why I personally don't attend classes.
For the record: I do not intend to persuade or encourage anyone else to start skipping their classes.

Classes as they currently exist are becoming redundant.
I personally predict massively distributed one-to-one (not necessarily human-to-human) learning via the internet, combined with small-scale many-to-many educational gatherings.
This allows anyone to learn at their own pace from a myriad of sources while automatically encouraging better teaching.


