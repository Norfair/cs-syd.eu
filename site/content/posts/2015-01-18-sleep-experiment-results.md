---
layout: post
title: "A sleep experiment: The results"
tags: sleep, experiment
---

This post contains my personal experience with a new sleep behaviour that I've tried.
If you haven't read [the post about the setup](/posts/2015-01-08-sleep-experiment-setup.html), go read it first.

<div></div><!--more-->

#### A notepad
[Thinking paper](/posts/2014-08-24-the-magic-of-thinking-paper.html) is not just an abstract concept.
Next to my bed, there is a notepad and a pen on my night stand.
I've made it a habit to write anything I think of, while lying in bed, that I might want to remember, on the pad and tear of the piece of paper so that I've got a clean sheet again.

It's come so far that I wake up next to few pieces of paper every morning.
They contain a wide variety of pieces of information.
This little notepad has helped me to recollect my experience with this experiment immensely.

There are often four to five notes on the notepad.
Strangely, I don't remember that I've written most of the notes.
Usually two of them are useless, but the others aren't.

#### First couple of days
The first 3 qdays, as I expected, I couldn't sleep but I went to bed anyway.
It was very important to let my body get used to the new schedule by having the discipline to keep to it.

After a few days, it begins to feel like everything is one long day.
Usually sleep let's us "increment the day counter" and complete a day.
The border between one day and the next has become vague.
For the sake of sanity, I have to accept that the qday is my new measure of time.

I'm teaching myself to relax very quickly so that I can fall asleep easily.
The usual relaxation exercises help, but what also works is to pretend that my muscles do not respond anymore.
If I keep telling myself that i cannot move my limbs, I won't move them and they'll relax very easily.

As time goes on, I notice that I can't sleep on uneven qdays, but also that I 'sleep in' on even qdays.
This is already better than not being able to sleep at all. 
I'm noticing that my body is adjusting.


#### A week in
After 26 qdays, I crashed.
I had had some more exercise than usual and I ended up sleeping for six hours, messing up my entire schedule.

Sleeping in was a setback, but I went to bed at the scheduled time and I adjusted to the schedule again in only two qdays.
Again, it was very important to let my body get used to the schedule by staying in bed during the scheduled sleep phases.
There were times that I had to stay in bed even when I was not tired, and get out of bed even when I was.


#### Getting used to the schedule
At this point, time doesn't feel like one long day anymore.
Now it feels like separate (short) days: qdays.

I'm finally getting used to the new schedule and the results are amazing.

- I get to do over ten hours of work on a daily basis, or about 3 hours every qday.
- I never feel tired anymore.
- I fall asleep really easily.
- I dream more often and more vividly.
- I am able to wake up very easily.
- I am usually in a very good mood.
- I procrastinate less often.

This last result has a peculiar reason though.
Usually when I procrastinate, I think something along the lines of "I'll do that tomorrow".
Now, "tomorrow" is at most 6 hours away.
This is not far enough into the future to remove the stress of the task, so I won't procrastinate.
Procrastination any further into the future will fill like a mess anyway.

#### Frequently Asked Questions

- When do you eat?

Every qmorning I prepare a meal for the qday, which I will be eating bits of throughout the qday.
I do try to eat somewhat healthy, but I don't really make a distinction between breakfast and dinner anymore.

- Are you out of your mind?

_Yes_.


#### Conclusion
This experiment has been a fun experience, but I'll be going back to my usual (polyphasic) sleep schedule.
It is obvious that it is not possible to maintain a sleep schedule like this one, as well as social life.
However, to get a lot of work done in a short amount of time, this schedule is truly amazing.
I consider this 'experiment' a great success and I will go back to this schedule in the future whenever I can.


