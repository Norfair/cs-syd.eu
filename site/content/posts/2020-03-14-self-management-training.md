---
title: Announcement: Self-management training
tags: self-management, training, announcement
---

Today I am announcing [my new self-management training offering](/self-management).

<div></div><!--more-->

A lot of my work for the last few years has been in the area of self-management.
It started almost eight years ago and the concept has been on my mind ever since.


I have flipped between [Taskwarrior](https://taskwarrior.org/) and [Emacs' org
mode](https://orgmode.org/) for years until I realised that I would have to
make my own tools.
Since then I've made [Intray](https://intray.cs-syd.eu), [Tickler](https://tickler.cs-syd.eu) and [Smos](https://smos.cs-syd.eu) 
and become an expert on the topic of self-management.


I have started training people professionally as part of my consulting work and gathered enough experience to confidently publicly announce [my new self-management training offering](/self-management).
At the time of writing, it consists of two options.

1. A private 2h seminar within an organisation
2. Individual 1-1 training, with a discount for more than five people

Self-management is a complex and very personal topic.
You can read any number of books on the topic, but nothing will ever beat in-person 1:1 coaching.
My tools will not necessarily work for everyone, so together we will evaluate which tools will work best for each individual.

Should you or your organisation be interested, then [click here to learn more](/self-management).
