---
title: Talk: Writing a text editor in Haskell with Brick @ F(by) 2020
tags: Haskell, Brick, text editor, talk
---


I gave a two-hour workshop at F(by) 2020 in Minsk on 2020-01-24.

<div></div><!--more-->

This is the first time that I managed to record the whole thing and upload it:

<iframe
  width="560"
  height="315"
  src="https://www.youtube.com/embed/Kmf3lnln1BI"
  frameborder="0"
  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
  allowfullscreen>
</iframe>

Slides and code are available [on GitHub](https://github.com/NorfairKing/writing-a-text-editor-in-haskell-with-brick)
Feedback is always welcome via email.
