---
title: The polyball prank
tags: polyball, prank
---

Last year I attended the 2018 polyball at ETH.
Its domain `https://polyball2018.ch` gave me an idea.

<div></div><!--more-->


> The Polyball is the most prestigious public event of the ETH Zurich. It has a long tradition since the 1880s and takes place annually at the end of November. The Polyball is the biggest decorated ball in Europe with approximately 9’000 visitors and is a classic dance event. 

<small>
  source: wikipedia
</small>

### An opportunity

It is a fabulous event and I really enjoyed it, but the tickets were quite pricey: >80 CHF (80 USD) each.
I noticed that the domain for their website had the year in it: `https://polyball2018.ch` so I figured that I would check whether the next year's domain was still free.
To my surprise, it was.
Needless to say I quickly bought it and set up the DNS (and SSL) such that anyone who would visit the domain would end up on this site instead.


#### First contact

I thought that the organisers would contact me sooner or later, to get the domain for their event.
No one contacted me at first, and I noticed that the website for the 2019 event was hosted at `https://polyball2018.ch` again.

So I contacted the organisers via their website:

``` email
Date: Wed, 11 Sep 2019 22:24:32

Dear Polyball organisers.

I am, as of yet, the owner of the polyball2019.ch domain.
I reserved the domain last year, and I would like to give it to you in exchange for four tickets to the 2019 polyball.
```

Their reply came the next morning:

``` email
Date: Thu, 12 Sep 2019 00:19:30

Dear Mr. Sydney
  
Thank you for your offer.
Unfortunately we have to disappoint you, since our decision for this year's domain already fell.
This year we will use www.polyball2020.ch
Our marketing division came to the conclusion that it gives the event a more futuristic touch.
It is also the better long term investment, since we can reuse it next year again.

Best regards,

XXXXXX XXXXX

Head of Domain, Polyball

Gesendet mit BlueMail

Am 12. Sep. 2019, 00:05, um 00:05, XXXXXXXXXXXX <XXXXXXX@ethz.ch> schrieb:
>Lol
>
>⁣Gesendet mit BlueMai
>
>Am 11. Sep. 2019, 23:44, um 23:44, XXXXXX@kosta.ch schrieb:
>>Lol
>>
>>
>>Am 11.09.2019 23:24 schrieb XXXXXXX <XXXXXXXXXXXXXXX@kosta.ch>:
>>
>>Lol
>>
>>---------- Forwarded message ----------
[...]
```

<small>
  Senders were anonymised. `ethz.ch` is the ETH domain, and `kosta.ch` is the domain of the polyball organisers.
</small>

Note that three people had already found the prank funny at this point (the `Lol` responses),
and that they all did that after midnight.
Mission success, as far as I am concerned.

#### A gamble

There are a few interesting parts about their reply:

> This year we will use www.polyball2020.ch

This seems rather unlikely, but the event is at the end of the year, so it is not entirely impossible.

> Our marketing division came to the conclusion that it gives the event a more futuristic touch.

This is exactly the kind of thing I could see a marketing 'division' come up with.

> It is also the better long term investment, since we can reuse it next year again.

This seems like bait.

> Head of Domain, Polyball

This is fishy.
That's a position I've never heard of, and it is particularly cirklejerk-y.

I checked whether `polyball2020.ch` is still available, and it was.
At this point I'm 50% convinced that they are trying to bait me into buying `polyball2020.ch` as well.
However, at a price of 40 USD, I was willing to take that chance, so I bought `polyball2020.ch` too.

### Landing page

I figured that just redirecting to my webpage was a bit too blunt, so I set up a little landing page
based on the `Host` header in the http request:

<div class="columns is-centered">
  <div class="column has-text-centered">
    <img src=/assets/polyball-prank/landing.png alt="Landing page">
  </div>
</div>

More to come, ... hopefully.
