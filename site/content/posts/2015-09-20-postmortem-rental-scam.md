---
layout: post
title: "Postmortem: Rental Scam"
tags: rental, scam, property, postmortem, flat
---

My first night in Zurich went from stressful to horrible.
In retrospect, I could have prevented it.

<div></div><!--more-->

## The story

I was going to work in London until Friday 11 SEP in London.
The next Monday would be my first day of university at ETH in Zurich, Switzerland.
This meant that I had to find accommodation in Zurich remotely.

We had been looking for accommodation for two months when finally things began to look up.
My girlfriend had found a post on facebook saying there was this woman in Zurich with a room for rent.
She sent me her contact details and I began communicating with this woman.
I had chatted with the woman via skype and exchanged emails.
She was known as Nerea Sanz and was of Spanish origin.

She sent me a contract and after we both signed it, I transferred the first two months of rent and the deposit.
Because her father was living in the UK, I transferred the money in pounds to a UK bank account.
We chatted some more afterward and all was well for me to arrive at the flat.
We had agreed to meet at 18:00 at the flat.

On Saturday I flew to Zurich with my three big pieces of luggage.
At the airport I took a taxi to the flat.
I arrived at a big building with hundreds of rooms but there was nobody there.

I had no Internet connection and no cell service because I was new to Zurich.
After ten minutes of looking around in confusion and twenty minutes of ringing the apartments bell I started becoming very frustrated.
Someone who lived in the building was kind enough to let me in past the front door into the lobby.

The building had WiFi, so I started mailing Miss Sanz and went to her door.
I started knocking on the door but nobody answered.
I knocked on the neighbor's door but they told me they'd never seen anyone living in the apartment.

It was at that moment that I noticed that my wallet was gone.
I had left it in the taxi.
I had nowhere to go at this point.
I didn't have my ID, nor any bank/credit card and no money.

I had become desperate.
The neighbor was kind enough to bring me to a youth hostel and made sure I could stay there for the night.
He had saved my day.
At the hostel I found out he was a Googler.
What are the odds!

The next day I went to the police to report the scam.


## Reflection

The entire situation made me feel very stupid.
There are definitely ways in which I could have prevented this situation.
I find it important to reflect on them.

Taking no time to find accommodation and to rest between working and studying seemed like something I would just have to put up with if I wanted to do both.
It may still be, but it caused a very stressful period where I was vulnerable to being scammed.
I had to find accommodation urgently, I was not around for any viewings and I was desperate to get it over with.

I don't have a facebook account.
My girlfriend sent me the information about the post.
At this point I could have already known that this was going to be a scam.
The information about 'Nerea' was entirely different from the information on facebook.
On facebook she was known as Bouzidi Soroya.
Of course I didn't find out about that until afterward.

In a skype chat, she told me her webcam and microphone were broken.
This is not necessarily a red flag but it should definitely have warned me.

This is a screenshot of her skype profile.
Needless to say I didn't have a close look at this profile before I noticed it was a scam.
Let's see if you can find what's wrong with it.

![The skype profile of the scammer](/assets/postmortem-rental-scam/skype.png)

Geneva, Swaziland?!
If that wasn't a giveaway, look at the phone number.
It's 14 digit's long! [There are no phone numbers that long.](https://en.wikipedia.org/wiki/National_conventions_for_writing_telephone_numbers)
The biggest red flag here is the '1 contact(s)'.
It should have stopped me immediately.
The profile was clearly created just for the scam.

Looking her up online, I should have noticed that 'Nerea Sanz' is nowhere to be found online.
There are no digital records of her whatsoever.


## Conclusion

I lost a lot of money during the scam and a lot of time afterwards.
I don't think something like this will happen to me again in the near future and I hope this post will make other people more suspicious as well.
I am very thankful for everyone's help, especially the Googler that got me to the hostel and the family I'm staying with right now.
