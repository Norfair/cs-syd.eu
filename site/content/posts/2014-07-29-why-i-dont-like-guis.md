---
layout: post
title: Why I don't like GUIs
tags: GUI, CLI, frustrations, keyboard, productivity, software, terminal
---

If you look at my system setup, you will notice that I use as few Graphical User Interfaces as possible.
I actively avoid using them, and this is why.

<div></div><!--more-->

### What GUIs are for
I will not go over what GUIs are in a lot of words.
I will, however, [define](/posts/2014-09-21-confusing-definitions.html) GUIs, so that I may talk about it.

Wikipedia defines GUIs as:

> A type of interface that allows users to interact with electronic devices through graphical icons and visual indicators such as secondary notation, as opposed to text-based interfaces, typed command labels or text navigation

It also mentions the following:

> GUIs were introduced in reaction to the perceived steep learning curve of command-line interfaces (CLIs), which require commands to be typed on the keyboard.


### What isn't a concern for me
I am a computer scientist. It is my job and my passion to work with computers.
I am, therefore, willing to invest time and energy to make my work efficient and pleasant.

I use (arch) Linux as my operating system, which means that I have access to a terminal.
Even more so, I do almost anything in my terminal. It teaches me more about my system.

The steep learning curve of a command line is something that I do not mind.
I have put many hours into using a terminal and CLIs. Currently I find GUIs even harder to learn to use.

For the record: This post is about why *I* don't like to use GUIs.
I'm not trying to convince you that GUIs are bad, nor that you shouldn't use them or that anyone should share this opinion.


### Speed
Speed is my biggest concern with GUIs. 

Most GUIs actively use the mouse for operation.
This often means that developers don't take the time to also implement keyboard shortcuts.
As a result, the user is often obligated to use the mouse to interact with the UI.
Moving your hand away from the keyboard to the mouse, moving around with it, and moving your hand back to your keyboard is a nuisance.
It is very slow and can waste a lot of time of someone who uses his system every day for hours on end.

It is estimated that using a mouse is approximately *4 times slower* than using the keyboard.
This means, that if you use your system for eight hours a day, and you spend 25% of that time using UIs, you will save approximately an hour and a half a day, just by not using GUIs.
That's an extra *three quarters of a month per year* you could spent productively (or otherwise). 


### Weight
GUIs can cause performance issues on low-end machines.
This may seem like a minor concern for most people, but a GUI is meant to serve as an interface between functionality and the user.
When simply interfacing with a program, I want to make sure that the program can run quickly without the hindrance of GUI overhead.

Sometimes you only have access to a terminal, In SSH sessions for example.
It comes in handy to know a lot about using the terminal then.


### Scripts
GUIs, without an official API, can hardly every be scripted with.
On the other hand, CLIs can be scripted with almost indefinitely.
If you do the same sequence of operations often, you might write a shell script.
In this situation, knowing a lot about using the terminal also comes in handy, because shell scripts usually have exactly the same syntax.
You could never make this kind of shortcut yourself when you're using a GUI.

GUIs often don't produce any good error messages.
Sometimes they display a pop-up message that briefly explains what went wrong, but often times the program just crashed and you have to look in the log files to see what happened.
When using CLIs on the other hand, error messages are instantly in front of you when something went wrong.
It might happen that a CLI doesn't print any error messages, but then that's just developer ignorance.


### CLI alternatives for GUIs
Here are some CLI alternatives for things that most people use GUIs for.

These are the ones that I regularly use.

* Email: mutt
* Text editing: vim, emacs
* File explorer: terminal
* Chat: weechat, irssi
* RSS reader: newsbeuter
* Music: MPD with ncmpcpp

Note that emacs is really a GUI, but it didn't use to be so most people don't use it as a GUI.

You can use w3m, Lynx or Elinks to browse the internet in your terminal, but I don't usually recommend that.
You can use a browser plugin like vimperator or pentadactyl to get more keyboard shortcuts in your browser.


### Conclusion
In summary, the real reason I don't use GUIs is that they're not user-friendly _to me_.
I understand that most people won't share this opinion, or want to use a setup like mine, but I encourage everyone who uses a computer heavily to consider it.
