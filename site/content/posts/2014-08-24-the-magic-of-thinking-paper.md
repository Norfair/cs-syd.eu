---
layout: post
title: The magic of thinking paper
tags: thinking paper, magic, creativity
---

During my time at the university, I discovered the concept of "thinking paper".
I've shared this idea with everyone I've worked with ever since, and now I want to show you.

<div></div><!--more-->

In the first year at the university of Leuven, everyone was given a goodie bag.
In this goodie bag, there were the usual things, a map of the city, lot's of informational flyers, etc...
The goodie bag also contained a clipboard.
It is with that clipboard that this whole idea started.

### The basic idea
Thinking paper is just paper on a clipboard that you always keep around you when working.
Anytime you're thinking about anything creative or technical, use the paper.
You could write things down, or you could draw things, but the real magic comes from being able to extend your thoughts to the paper

![A clipboard with paper](/assets/the-magic-of-thinking-paper/clipboard.png)

### The magic
Thoughts on paper are useless to anyone who isn't the author of them, simply because they wouldn't know what the writing meant.
They are no more useful to anyone else, just like thoughts in someone else's mind.

To the author, however, thoughts on paper that they understand can be of great value.
Thinking paper tends to work like a [programmer's rubber duck](http://en.wikipedia.org/wiki/Rubber_duck_debugging).
You get to explain your thoughts to some other entity (that happens to be you too) through the paper and get feedback on them.

Secondly, thoughts on paper _last_. You look back at the paper whenever you need to.
Be warned. The longer you want to keep the thought on paper effectively, the better the though has to be written down.
By "better", I don't necessarily mean more detailed.
I simply mean "easier to re-understand from scratch again".
The mind forgets. It doesn't only forget the thought, but it also forgets the semantics of how you wrote it down.

### More magic
Using thinking paper when working alone is fine, but it works even better when _you don't work alone_.
You can't just share your thinking paper, the other person probably won't understand any of it unless you explain what you draw.
If you work together, keep your thinking paper at hand.
You will notice that, just by holding a pen next to your paper, you will start to write things down or to draw.
The paper then provides a visual clarification of the thought you're trying to convey.

In this case, it's a poor man's whiteboard.
However, I argue that it's even better than a whiteboard if you're working in a team of less than five people.
The transaction cost of writing on paper on a clipboard is much lower than that of writing on a clipboard
with dry-eraser markers that are rarely even there.
People rarely write on whiteboards, which means that they are usually not comfortable writing on a vertical platform.

### Making your own thinking paper.
Thinking paper is not much different from regular paper, but it has some unique properties.

#### Thinking paper is mobile.
You should be able to carry your thinking paper with you anywhere.
This is why I discovered the concept thanks to the clipboard.
I could bring my entire stack of papers with me in an instant and share my thoughts easily.

Note that you can 'implement' thinking paper in other ways.
You don't have to stick to a pen, paper and a clipboard.

#### The paper is plentifully available.
Just having one piece of paper on your clipboard doesn't do it.
You need to be able to throw away a dozen sheets of paper without running out.
If you can't just write down anything you think of, you will unconsciously try to save the space you have left.
This will inhibit your creativity, as you only want to write down 'the good stuff'.
You don't just get good ideas, you have to get a lot of ideas, and filter the good ones out.

#### It is readily available.
You won't write anything down if you don't have the paper in front of you and the pen in your hand.
You need to be able to start writing or drawing as soon as you start thinking.
The writing should be done _while_ thinking, not afterwards.
Everything should also be ready before you start.
Don't go asking for a pen because you happen to have forgotten yours and interrupt your thinking in doing so.
Carry some spares.

#### It is easily usable.
The pen you're writing with should be comfortable.
Different pens make you have different ideas.
This may sound weird, but try taking notes with a fancy pen instead of a pencil stub, and you'll notice.
The paper you're writing on should be fixed, so that it doesn't slide away.
Don't just write on loose sheets of paper as that causes all kinds of problems.
The paper should be supported, so that the writing can be steady.
This is where a stack of paper comes in handy again. writing on a sheet ontop of wood usually causes less readable text than writing on a sheet ontop of a stack of papers.

Again, a clipboard provides the perfect environment for this, but you can make it work in other ways too.
Some people use a text editor for this, but I'd recommend against this, because in a text editor you can't mix drawings with text easily (although emacs probably has a mode for this somewhere).
