---
title: "Everything in moderation" is nonsense
tags: quitting
---

Because of [my focus on health last year](/posts/2018-01-28-2017-year-in-review), I have heard one particular phrase a lot.
It is a phrase that I have to respond to before writing any more about my quitting list.
The phrase in question is "Everything in moderation".
I have a lot of time to think about it, and the conclusion is that this phrase is _just nonsense_.

<div></div><!--more-->

### Background

Part of focusing on my health, this year, has been to make better food choices.
In particular, I have quit certain foods.
One of things that I have quit is _alcohol_.

You can debate whether alcohol is bad, but that is really not the point of this post.
The reason that I bring this up is because I hear this phrase a lot:

> Everything in moderation

### Nonsense

Note: I will sound quite anal for a moment, but bear with me.
I promise that it will make sense by the end.

If we look at this phrase by itself, disregarding the connotations, it is obviously nonsense because 'Everything' includes genocide, nuclear fallout, asbestos, etc.
You can have asbestos in your building, but you do not want _a little bit of asbestos_ in your building.
You just want _no asbestos_ in your building.

Of course you will now argue that 'Everything', in the situation at hand, refers to stuff you eat.
Let us consider why the phrase is still nonsense.
You can eat plutonium, but you will never want a moderate amount of plutonium in your diet.
Plutonium is radioactive. Eating plutonium has all sorts of nasty effects.
You just want _no plutonium_ in your diet, because it is a bad idea.

### Alternative

What we can learn from this is that when it comes to, for lack of a better phrase, *bad things*.
_You do not moderate._
When it comes to bad things, for the sake of your own health and conscience, you either never start or _you quit entirely_.

So what about moderation?
Moderation in not bad, _but_ you should only moderate good things.
Too much of a good thing is bad as well, which is why you should moderate good things.

We need a better phrase:

> Moderate the good, quit the bad.

### Do not moderate the bad

At this point you are probably thinking "You are just being annoying right now, there is a big difference between eating plutonium and drinking a glass of wine.".

If you agree with me about not moderating plutonium, the next step of my argument is to show you that the difference between adding plutonium to your diet and having a beer after work is not as big as you might think.

Here are some dissimilarities:

- Eating plutonium is not social convention and not part of our culture.
- People do not suggest plutonium at a bar.

Note that all of these are socio-cultural, and keep in mind that slavery used to be acceptable.
Now we do not think about moderating slavery, we just want no slavery in our societies.

Next, look at some similarities between alcohol and plutonium:

- They cause cancer and many other long-term problems.
- In the short term they have nasty effects as well, including vomiting.

Are these two really so dissimilar?
Now replace alcohol by cocaine or heroine.
Are they still dissimilar?
You probably draw a line somewhere, but more interesting than the question of where to draw the line is to ask yourself _why_ you draw the line there.
Is it purely because of social convention?
Then maybe you want to reconsider.
