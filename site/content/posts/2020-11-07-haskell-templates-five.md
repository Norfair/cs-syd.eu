---
title: Haskell Templates Launch
tags: Haskell, templates
---

After experimenting with Haskell templates for a few weeks, I am excited to officially launch [my Haskell Templates](https://template.cs-syd.eu).

<div></div><!--more-->

A few people asked to hear about what goes into a project like this, so in this post I would like to tell you about the project's history.

# Phase 0: The templates idea

I started this project because I saw templates for front-ends in many places, most notably for Wordpress.
I asked myself why this kind of thing does not exist for back-end code.
Indeed, there are good reasons for using a template, and both front-end and back-end have those in common:

* You have to do very similar things every time you start a project.
* Your time is worth money, so you can save money by buying a template.
* When you buy a template, you can be sure that the expert who wrote it made sensible choices.

# Phase 1: Validating the concept of a template

To get started, I first had to know whether anyone would be interested in buying a template.
There were going to be things that I had not thought of, and this first step was all about finding out what they were.

I threw together a landing page using [Mailchimp](https://mailchimp.com). (This is still visible at https://template.cs-syd.eu/landing for posterity.)
Anyone visiting this page could see what the templates were going to be about, and they could leave their email address to be notified as soon as the first templates would be available.

I put a link to it on twitter and on [my blog](http://localhost:8000/posts/2020-10-08-haskell-templates-validation) and got about a dozen email addresses and quite a bit of feedback.
Most of the feedback was either about the legal aspect of buying code; "What would you be allowed to do with it?" or people telling me that "this template thing" looked like a good idea.
I still had no idea whether anyone would spend money on a template, but there was definitely interest so I decided to go on.

# Phase 2: Giving away a template for free

To be able to ask people whether they would want to spend money on a template, I would have to be able to show them one.
I put together [a very small template with just option parsing](https://template.cs-syd.eu/template/NorfairKing/template-optparse) because it is the smallest template that I could think of that I regularly use for myself.
I put together the first version of [the template website](https://template.cs-syd.eu) to sell this option parsing template, but I did not build the payment flow yet to save time.
I used what I know best to put together the website without wasting any time: Yesod, Sqlite, Nix and NixOps.
I also made sure that a customer would not have to sign up to anything in order to buy a template.
Then I made the option parsing template free and told people about the new website in all the regular places.

Within the first twenty four hours, five people "bought" the template.
I made sure to contact each of them for feedback and use that feedback to improve both [the template itself](https://template.cs-syd.eu/template/NorfairKing/template-optparse) and [the templates website](https://template.cs-syd.eu).
At this point I was convinced enough that this was worth a shot.

# Phase 3: Paid templates

To get started accepting payments, I used [the Stripe client generator that my students built](https://github.com/Haskell-OpenAPI-Code-Generator/Stripe-Haskell-Library).
It worked nicely, after some little improvements (that I managed upstream), so I started selling [the first template](https://template.cs-syd.eu/template/NorfairKing/template-optparse).
I put something on [the Haskell subreddit](https://www.reddit.com/r/haskell/comments/jbltk0/cs_syd_haskell_templates_the_first_template_is/) and got a few sales.

I made a second template and, while testing it out, quickly noticed that a user would spend an infeasible amount of time changing the word "Template" to the name of their project in.
To fix this problem, I built [a Template filler tool](https://github.com/NorfairKing/template-filler) that does this for you.

I made sure that each template contains detailed instructions for how to use it, where to find everything, and diagrams that can clarify the project's structure.

# Phase 4: More templates

I have spent the last few weeks writing [three](https://template.cs-syd.eu/template/NorfairKing/template-api-server-with-auth-and-cli) [more](https://template.cs-syd.eu/template/NorfairKing/template-local-first-app-with-sync-server) [templates](https://template.cs-syd.eu/template/NorfairKing/template-tui), for a total of five:

- [OptParse](https://template.cs-syd.eu/template/NorfairKing/template-optparse)
- [CLI](https://template.cs-syd.eu/template/NorfairKing/template-cli)
- [API Server](https://template.cs-syd.eu/template/NorfairKing/template-api-server-with-auth-and-cli)
- [Synchronisation server](https://template.cs-syd.eu/template/NorfairKing/template-local-first-app-with-sync-server)
- [TUI](https://template.cs-syd.eu/template/NorfairKing/template-tui)

I have some more templates in mind.
[If you have any more requests, you can request a template here](https://forms.gle/hLs727zhpZDBCcAx8)

- Web Server
- A complete Api-first project with a CLI, a TUI, an API server and a web-server

At the time of writing, I have made my first 100 USD off this project.
That amount of money has not made it worth my time yet, but the experience definitely has.

# Next steps

At this point I want to use this project to practice marketing and to see where it can go.
I have some more ideas, but they depend on the amount of interest (and sales) I can generate.

If anyone is interested in selling some of their own templates, please [contact me](/contact) so we can work together.


<a class="button is-link" href="https://template.cs-syd.eu">
  Go to the templates website
</a>
