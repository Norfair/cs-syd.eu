---
layout: post
title: The responsibility of an appointment
tags: responsibility,appointments,time
---

Moving to Switzerland has made me realise that a lot of my frustrations concerning appointments have been mostly a consequence of the culture I was surrounded by.
I have been in countless discussions about people being on time for their appointments.
So many that I think it's worth explaining where the frustrations came from.

<div></div><!--more-->

I have to stress that this post is about my personal concerns with people being late in our appointments.
I am trying to clearly communicate what my expectations are concerning appointments.
I do this for the sake of transparency and the prevention of needless further arguments.
Feel free to have your own expectations and to ignore mine entirely.

I will purposefully not go into the personal consequences of being late.
How others handle their life is none of my business.
Stealing my time on the other hand, that is an issue that involves me.
As such I will explain why it bothers me.


### A perception of tardiness

> appointment
> (/əˈpɔɪntm(ə)nt/)
> noun,
> an arrangement to meet someone at a particular time and place.

Let's you make an appointment to meet your friend at time X at place Y.
You both take time out of your lives to be at that particular spot at that particular time.

If you are late, you are forcing your friend to spend extra time.
Time that is not spent in a useful manner.
Usually just waiting for you.

This is a word for this behaviour:

> steal
> (/stiːl/)
> verb
> take (another person's property) without permission or legal right and without intending to return it.

It is *stealing*.
Time is the most valuable thing any of us will ever posses.
(In fact one could argue it's the _only_ thing a person will really posses.)
Stealing time is even worse than stealing money because you have a chance to recover money but you will never recover lost time.

This whole problem seems to be very culture dependent.
I'm not quite sure what it depends on but it seems to be worse in some place than in others.
In Switzerland it's part of the culture to be on time.
The amazing public transport is just one example.
Action has been taken to ensure that people are on time.
For example, classes start at quarter past the hour instead of on the hour.
This helps people to be on time for class.


### Quick replies
Answers to frequently discussed questions

- It's always possible that I am late due to external factors. Surely I shouldn't be blamed for others' mistakes? Trains could be late, busses could be canceled, etc...

Of course you could be unlucky using whatever mode of transportation you have chosen, but that does *not* exempt you from the responsibility of the appointment.
If you aren't willing to accept the risk of using externally gouverned transportation, you should not *make* the appointment in the first place.
You could also just make better arrangements.
Bringing external factors into consideration requires the contemplation of the risk they bring with them.
Take responsibility for the risks you take and don't blame someone else if they don't work out.
Most of these risk can be mediated by taking the earlier bus or the earlier train.

In the same way, I will take the responsibility for the risk I take by making an appointment in the first place by not arranging any more appointments.


- If you know I'm going to be late 10 minutes every time we meet, why not just show up 10 minutes later?

First of all, I will not accommodate for your disrespect.
If your tardiness is this bad, I will just stop arranging appointments with you.
I value my own time more than an appointment with someone who shows this kind of disrespect to me.
Second: If your tardiness is this predictable, why not come 10 minutes earlier and be on time?

Moreover, what you are describing here is a [non-cooperative game](https://en.wikipedia.org/wiki/Nash_equilibrium).
If you don't come on time, that gives me an incentive to come later too.
That will, in turn, give you more of an incentive to come late.
This effect has been greatly studied and the result is a [Nash equilibrium](https://en.wikipedia.org/wiki/Nash_equilibrium) where no-one ever comes on time and it becomes impossible to make appointments anymore.

> Basketball practice starts at 3, you are late as of 2:55
> - Coach Carter

