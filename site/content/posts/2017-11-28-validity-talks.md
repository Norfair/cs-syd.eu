---
title: Validity Talks with Patat
tags: talk, validity, safety, testing, property testing, testing
---

I have the pleasure of [speaking at the HaskellerZ meetup](https://www.meetup.com/HaskellerZ/events/245248358/) and at the Google office for an internal tech talk.

<div></div><!--more-->

In these talks, I point to this page for the audience members to find more material on validity and validity-based testing.

* I have written about [Validity and Validity-based testing before](https://cs-syd.eu/posts/2016-07-17-custom-validity-and-validity-based-testing-in-haskell).
* The `validity` repository has a very thorough [README](https://github.com/NorfairKing/validity#readme) that will walk you through an example and contains the motivation for the concepts and packages as well.
* [The slide deck for the presentation](https://github.com/NorfairKing/validity/blob/development/presentation.md) is also available in that repository.

<small>
  I particularly enjoy public speaking; technical or otherwise. If you would like me to speak at your company or conference, please [contact me](/contact.html).
</small>
