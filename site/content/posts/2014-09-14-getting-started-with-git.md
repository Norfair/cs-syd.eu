---
layout: post
title: Getting started with git
tags: git, aliases, getting started, tutorial, programming, dotfiles, aliases
---

This is not a short introduction to git. 
I will not explain what git is, what it does or how to use it.
This is a post for anyone who wants to learn git, and doesn't know where to start.
Note that a basic understanding of [version control](http://en.wikipedia.org/wiki/Revision_control) is assumed.

To learn something quickly, you will want to learn from different sources.
Keep in mind that you will still need about [20 hours](https://www.youtube.com/watch?v=5MgBikgcWnY) to learn git to a level that allows you to collaborate with other developers. If you're going to be a professional developer, these are going to be the best spent hours of your entire education.
I personally recommend starting a small personal project with git.

<div></div><!--more-->

### Resources
If you look for resources for learning about git, you will probably find the following ones in order.
The order in which you find them, however, is not the order in which you should look at them.
Here's why:

- [The git book](http://git-scm.com/book)

Scott Chacon wrote a beautiful book about the workings of git: [Pro Git](http://git-scm.com/book)
This book explains every part of git in depth.
I like it as a manual, but for first use, I'd recommend against reading this entirely.

- [Git tutorials](https://www.atlassian.com/git/tutorial)

Ah, tutorials, the bread and butter for any programmer...
These tutorials try to visually explain what git can do for you.
They are made for developers who are coming to git from subversion, but they're made very accessible to anyone.
The drawback to the approach of these tutorials is that you should already know _why_ you want to use git before reading them.

- [The git reference documentation](http://git-scm.com/docs)

If you ever need to find the exact syntax and semantics of a git command, this is the place to go.
The same information can also be found using the 'man' command.
The reference documentation is great once you know how git works, but, for now, focus on understanding how git works, rather than on how you should use it.


- [Linus Torvalds on git](https://www.youtube.com/watch?v=4XpnKHJAok8)

Torvalds made the first version of git.
In this talk, he explains why you, as a developer, should be using git.
In his usual sarcastic way of talking, he also explains what was wrong with the version control systems before git, and how git improved on them.
This is, in my not-so-humble personal opinion, the best place to start.

- [Git for computer scientists](http://eagain.net/articles/git-for-computer-scientists/)

Knowing why something works, rather than only knowing how it works, is essential to truly understanding it.
In the case of git, someone wrote an article on the inner workings of git at the conceptual level.
You can use git without ever reading this, but you will want to, when you get familiar with the tool.

### Beginner pitfalls
There are some things that you shouldn't ever do with git.
Some of these things are very common mistakes for beginners

- Working without a development branch (or working without branches at all)

Working with branches is what makes git so powerful as a tool.
Branches allow you to develop a feature separately from what the other developers are doing.
They also allow you to have a stable branch (usually master) where everything is (semi-)guaranteed to work, and a development version that might be unstable.
Lastly, branches allow you to really play around with an idea. The power to use branches gives you an incentive to be creative.

- Committing binaries

Git is made to manage changes in source code at the granularity of lines.
This works because these changes are often really small compared to the size of the entire file.
Binaries don't use lines, and they often change entirely when you change a small part of the code.
Committing binaries will give you a lot of potential merge conflict, and headaches as a result.

- Committing IDE configs

IDE setting are specific to your setup.
If more than one person commits their settings, the team will be merging these manually constantly.
Examples of these include eclipse's '.metadata' directory, as well as IntelliJ's '.idea' directory and netbeans' 'nbproject' directory.

- Not using git ignores

Thinking about which files not to commit becomes cumbersome.
There are ways to have git automatically ignore certain files.
At the level of the user, you can configure git to only ignore the files you never want to commit, like IDE configs.
At the project level, you can ignore files on a per repository basis.
This is useful to exclude binaries and such.

- Not committing often enough

Some beginners are concerned with 'wasting' something by committing too often.
They end up committing only at the end of their work session, which makes for a horrible git history.
Encapsulating commits nicely is essential for easy merging. 

- Undescriptive commit messages

It's really easy to lose your cool and start writing [incomprehensible commit messages](http://www.commitlogsfromlastnight.com/), especially when you're getting frustrated.
While this is fun to watch happen as a bystander, because those messages tend to be hilarious, this situation is less than helpful for the project.
Before committing anything, you should really have read [a guide on how to write good commit messages](https://wiki.openstack.org/wiki/GitCommitMessages#Example_1_4).

### Basic configurations
There are some config options that you should set before starting.
Git will be overwhelming at first, these should help out with getting used to the workflow.

For any collaboration, you should set your name and email address.

``` bash
git config --global user.name "Full User Name"
git config --global user.email user@example.com
```

To write commit messages, git uses vi by default. You might want to change this setting.

``` bash
git config --global core.editor emacs
```

To resolve merge conflicts, git uses vimdiff by default. You will probably want to change this.
There are many mergetools out there, I like [meld](http://meldmerge.org).

``` bash
git config --global merge.tool meld
```

### Gitignores
There are some files that you should always ignore. These go in your private gitignore file.
Most users put this file in their home directory.

``` bash
git config --global core.excludesfile ~/.gitignore
```

``` bash
$ cat ~/.gitignore

# Editor files #
################
*~
.*.sw[op]
\#*\#

#compiled source #
###################
*.com
*.class
*.dll
*.exe
*.o
*.so
 
# Packages #
############
# it's better to unpack these files and commit the raw source
# git has its own built in compression methods
*.7z
*.dmg
*.gz
*.iso
*.jar
*.rar
*.tar
*.zip
 
# Logs and databases #
######################
*.log
*.sql
*.sqlite
 
# OS generated files #
######################
.DS_Store
.DS_Store?
._*
.Spotlight-V100
.Trashes
ehthumbs.db
Thumbs.db
```


You will want to look at [the gitignores repository](https://github.com/github/gitignore) for more specific gitignores.
You will want to add more of these ignores, specific for your workflow.
For example: If you're using eclipse to build a java application, add the specific eclipse and java ignores from the gitignores repository.
    
### Aliases

When you start using git from the command line, you will notice that the commands you type are too long, compared to their frequency of use.
I have written some aliases for git commands that I use most often. You can just put them in your shell's aliases file.


``` bash
alias gs='git status'
alias gd='git diff'

alias ga='git add'
alias gaf='git add -f'

alias gam='git commit --amend'
alias gc='git commit'

alias gp='git push'
alias gpl='git pull --rebase'

function gr() {
    git tag -a $1 -m $2
}
alias gr=gr
alias gpr='git push origin --tags'

alias gm='git merge --no-ff'
alias gmd='gm development'
alias gmm='git merge master'

alias go='git checkout'
alias gb='git branch'
alias gcm='git checkout master'
alias gcd='git checkout development'
alias gnb='git checkout -b'

alias gpb='git push --set-upstream origin'

alias gi="vim .gitignore"

alias gl='git log --graph --decorate --abbrev-commit --all --pretty=oneline'
alias gls='git log --graph --decorate --abbrev-commit --all --pretty=oneline --stat'

# Use with caution or not at all.
alias g='git add . && git commit -a'   
```

