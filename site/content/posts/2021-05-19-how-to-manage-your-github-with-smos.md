---
title: How to manage your work on GitHub with Smos
tags: self-management, smos
---


[Smos has recently gotten a new companion tool called `smos-github`, which allows for easier integration of your self-management workflow with GitHub.](https://docs.smos.online/changelog/all#smos-github-0.0.0)
In this post I will propose a sample workflow for developers to keep track of, and follow up on, the issues and pull requests that they are involved in.
We assume a basic working knowledge of how to implement GTD in Smos.

<div></div><!--more-->

### Managing software projects using self-management projects

The term 'project' is a bit overloaded here, because in software "smos" would be considered one project.
However, using a granularity as coarse as that will prove unhelpful in the context of self-management.
Instead, we recommend using a single issue and/or pull request as the granularity of a project in self-management.
For example, you might have an `our-app/static-builds.smos` or `our-app/issue-256.smos` project file in your workflow, but not just an `our-app.smos` project file.

### Pieces to manage

Writing code involves more than just writing committing code to a GitHub repository.
A sustainable real-world development process involves tracking issues, discussions, code review, CI, CD, follow-up with the stakeholders, etc.
The self-management approach that you use should be able to deal with all of these pieces, not just the code.


### An example project

To explain the workflow that I propose, I will use [this issue](https://github.com/NorfairKing/smos/issues/202) as an example.
This issue was opened by someone who noticed a bug in `smos-sync-client` on Mac OS.
After a bit of back and forth, they created a PR with a fix which required review.
The problem was fixed and the fix added to the changelog.

From a self-management perspective, the requirements are:

- When I first get the issue, I need to process it to decide what to do with it
- When I write a comment and expect a response, I need to be able to wait for a response in such a way that I can forget about it until 1. the answer comes in or 2. I need to send a reminder.
- When I write a PR, I need to be able to wait for a review in such a way that I can forget about it until 1. I get a review or 2. I need to send a reminder.

Using smos, these requirements can be fulfilled using the `WAITING` state, the `url` property and optionally the new `smos-github` tool.

### Workflow using Smos

When I first get the issue, GitHub sends me a notification to my inbox.
From there I can process it and write up a smos project.

``` plain
──[ Editor: workflow/projects/smos/issue-202 ]──
  TODO Issue 202 [0/1]
  goal: Fix syncing on macos
  ❯ NEXT Investigate to see if I can find the bug
```

After a bit of investigating, I decide that I need to ask the bug reporter some questions, so I write my questions into the issue discussion and use the `convDoneAndWaitForResponse` action (`<space>nw`, by default) to mark my investigation entry as `DONE` and create a `WAITING` entry to wait for a response.
I also add the `url` property and refer to the comment that I'm waiting for a response on.

``` plain
──[ Editor: workflow/projects/smos/issue-202 ]──
  TODO Issue 202 [1/2]                                                              
  goal: Fix syncing on macos
    DONE Investigate to see if I can find the bug
  ❯ WAITING for a response from the issue reporter                                  
    url: https://github.com/NorfairKing/smos/issues/202#issuecomment-830430193  
```

Now I can select the entry with the `url` property and activate the `convOpenUrl` action (`<space>ou`, by default) to open the issue in my browser to see if I have gotten a response yet.
However, we can let `smos-github` do this for us.

``` plain
$ smos-github list
file                          state    header                                  owner        repo  number  state   update
projects/smos/issue-202.smos  WAITING  for a response from the issue reporter  NorfairKing  smos  202     open    waiting
```

Now the entry will be treated as any other `WAITING` entry: It will show up in your waiting report and it will show up in your work report when it's overdue.
On top of that, the `smos-github list` command will show you in the `update` column whether it is your turn to do something.
Indeed, when I get a response to my question, the `update` column will show `ready` like this:

``` plain
$ smos-github list
file                          state    header                                  owner        repo  number  state   update
projects/smos/issue-202.smos  WAITING  for a response from the issue reporter  NorfairKing  smos  202     open    ready
```

(In practice, you don't _need_ the `smos-github` tool, but it can be very helpful to check on all GitHub issues and pull requests during your weekly review.

Now that I have enough information to continue, I have a next action again, and I add the link to the latest comment to it:

``` plain
──[ Editor: workflow/projects/smos/issue-202 ]──
  TODO Issue 202 [2/3]
  goal: Fix syncing on macos
    DONE Investigate to see if I can find the bug
    DONE for a response from the issue reporter
    url: https://github.com/NorfairKing/smos/issues/202#issuecomment-830430193
  ❯ NEXT make a PR with the fix
    url: https://github.com/NorfairKing/smos/issues/202#issuecomment-830641556
```

Once I've made my PR, I can add another waiting action and link to it with the `url` property again:

``` plain
──[ Editor: workflow/projects/smos/issue-202 ]──
  TODO Issue 202 [3/4] 
  goal: Fix syncing on macos
    DONE Investigate to see if I can find the bug
    DONE for a response from the issue reporter
    url: https://github.com/NorfairKing/smos/issues/202#issuecomment-830430193
    DONE make a PR with the fix
    url: https://github.com/NorfairKing/smos/issues/202#issuecomment-830641556
  ❯ WAITING for a review on this PR
    url: https://github.com/NorfairKing/smos/pull/203  
```

This last entry `WAITING` entry works the same way as any other `WAITING` entry, just like before, and `smos-github list` will show `ready` when the PR has received a review.

Now the back and forth can continue until the PR is merged, the issue closed, and the smos project archived.
