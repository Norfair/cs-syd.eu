---
title: Announcing yesod-static-remote
tags: yesod,  Haskell
---

While making some web services, I noticed that there was a little piece of functionality that I would need almost every time.
The new [`yesod-static-remote`](https://hackage.haskell.org/package/yesod-static-remote) library scratches this little itch in under 50 lines of code.

<div></div><!--more-->


### Context 

Every web service I write seems to use at least one external resource.
This could be some copy of jquery, some version of bootstrap or similar, and usually not just one of them.
These usually do not change during the lifetime of a deployment.

Let's take bootstrap as an example.
We then add something like this in the `<head>` of every page:

``` html
<link
  rel="stylesheet"
  href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
```

There are some practical problems with this scenario.

First, it becomes impossible to develop the web service without an internet connection.
Indeed, even running a local copy requires the external resource now.
I spend a lot of time on airplanes so this matters to me.

Second, your web service now relies on an external web service to serve up this external resource.
This is just another point of failure.
Some users may even turn off external resources by default, in which case your site may not render very nicely for them now.

The big advantage of this approach, however, is that you do not have to worry about serving up the resources from your own server.
You do not need to download it or put it in the right place, the browser will just take care of that for you.


### Introducing `yesod-static-remote`

The new `yesod-static-remote` library turns your runtime CDN usage into compile-time CDN usage.
The idea is that you specify the `href` of your external resource at compile-time, and the library will take care
of downloading and caching it.
You then use `yesod-static` to bake it into your server binary.

Here is an example usage:

``` Haskell
import Yesod.EmbeddedStatic
import Yesod.EmbeddedStatic.Remote

mkEmbeddedStatic
  development
  "myStatic"
  [ embedRemoteFileAt
      "static/bootstrap.css"
      "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
  , embedFileAt "logo.jpg" "static/logo-H.jpg"
  ]
```

As you can see, it fits in right next to your normal `Yesod.EmbeddedStatic` usage.
The external file will be downloaded at compile-time and cached locally.
This way you can use it for local development and you can serve it up from your own server with the same ease.


The `yesod-static-remote` library is available [on GitHub](https://github.com/NorfairKing/yesod-static-remote) and [on Hackage](https://hackage.haskell.org/package/yesod-static-remote).

