---
title: How to turn off the nix flake registry
tags: nix, flake, registry
---


This is a quick tutorial on how to turn of the nix flake registry.

<div></div><!--more-->

### What is the nix flake registry?

The nix flake registry is a per-machine or per-user mapping of names to flake identifiers.
For example, `nixpkgs` represents `git://github.com/NixOS/nixpkgs`.
You can list yours using `nix registry list`.

It is what lets you write `nix run nixpkgs#firefox` instead of `github:NixOS/nixpkgs#firefox`.
It also lets you use `nixpkgs` in your flake's `outputs` function without declaring it in its `inputs` set:

``` nix
{
  inputs = {};
  outputs =
    { self
    , nixpkgs
    }: { };
}
```

### Why would you want to turn it off?

To make sure all inputs are specified explicitly.


### How to turn it off

In your NixOS configuration:

``` nix
nix.registry = {};
nix.settings.flake-registry = "";
```

In your home manager configuration:

```
home-manager.users.<my-user>.xdg.configFile."nix/registry.json".text = builtins.toJSON { version = 2; };
```


<br>
<br>
<small>
If anyone wants to upstream this to more official Nix documentation, they're welcome to do so.
</small>
