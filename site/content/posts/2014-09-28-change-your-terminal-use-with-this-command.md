---
layout: post
title: Change the way you use your terminal use with this function
tags: terminal, aliases, dotfiles, CLI
---

I am a great fan of using transaction costs to improve your life.
Using a terminal instead of a graphical interface has a high transaction cost for most (beginner) linux users.
In this post, I will show you a way to reduce the transaction costs of using your terminal with one function.
A basic understanding of [aliases](http://tldp.org/LDP/abs/html/aliases.html) is assumed.

<div></div><!--more-->

### ls -whatever

If you look through the .bash_history file of most (beginner) linux users, you either see nothing at all, or something like this.

``` bash
cd path
ls -l
cd to
ls -l
cd files
ls -l
<some_command>
```

The user executes 'ls', with some option(s), after every 'cd'.
The first thing you can do, is abbreviate this 'ls -l' to something shorter.
Here's an example of a set of ls aliases

``` bash
alias l='clear ;\
ls -l -X\
 --color=auto\
 --group-directories-first\
 --human-readable\
 --time-style="+%d/%m/%y - %R"'

alias la='l --almost-all'
```

This 'l' alias lists the current working directory in long color format with human-readable file sizes, a better time-style and with the directories before the files. It also sorts entries by extension instead of by file name.
There's no harm in using the long options.
In fact, using the long options instead of the short ones makes the aliases easier to read and to modify.
Since the entire purpose is to not type the entire command, but to put it in an alias file, this is the handier option.

### cdls

If you look at the bash history again, you will see that most (beginner) users execute _'ls' after every successful 'cd'_.
I will now show you how you can do that automatically.

This was my personal first attempt, but it won't do.

``` bash
alias cd='cd && ls'
```

Using this alias will cause you to go back to your home directory and list it every time.

Instead, you will need to use a function.
Again, there's no need to make this code short.
It just has to be readable.

``` bash
# Perform 'ls' after successful 'cd'
cdls() {
  builtin cd "$*"
  RESULT=$?
  if [ "$RESULT" -eq 0 ]
  then
    l
  fi
}
alias cd='cdls'
```

You can just put this in your .bash_aliases (, or even your .bashrc file).

The most used command used to be at least six characters long.
Now it's no more than three charactes long and you can add any number of options to your regular 'ls'.
Not only will this trick save you a lot of time, but it will encourage you to use your terminal more often.
Of course, if you still don't want to use your terminal, that's fine, but this way, it will at least be easier.
