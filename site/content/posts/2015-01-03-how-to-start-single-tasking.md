---
layout: post
title: How to start single-tasking
tags: single-tasking, productivity, work, multitasking
---

As [I've mentioned before](/quotes/2014-10-26-the-less-you-do-the-more-of-it-you-can-do---scott-hanselman.html), multitasking is a bad idea.
However, as I've also discovered, people don't usually know _that_ they're multi-tasking or *how to stop* it.

I've recently made an effort to start single-tasking and I think I've been quite successful, though maybe a bit radical.
My productivity has increased immensely, as well as my motivation to work.

<div></div><!--more-->

#### Interruptions
There are two kinds of interruptions.

- External interruptions: Some external factor, a person, notification, etc interrupts your work.
- Internal interruptions: You interrupt yourself. This might happen because you start daydreaming, or because you thought of something else you needed to do. 

The first thing you will need to do to start single-tasking is to set up a space where you don't get interrupted externally.
The perfect place would of course be a personal office, but many of us are not afforded that luxury.
You can also work in a not-so-private place if you have to as long as you make it clear that, as long as you are working, you are not to be interrupted.
A tip is to wear headphones to signal that you're busy.

Next, you will make it a habit not to interrupt yourself while you're working.
I personally suggest the [pomodoro technique](http://pomodorotechnique.com) to get started.
You work for 25 minutes and take a break for 5 minutes afterwards.
You exercise your attention by taking note of how many times you were interrupted, both internally and externally.
The next time you work, you will try to decrease that amount.

#### No more mindless browsing
Not everyone works at a computer all the time, but if you do, here are some more tips.
I found I used to waste a lot of time mindlessly browsing facebook, twitter, reddit, etc...

The first thing you can do to get out of this (frankly ridiculous) habit, is to put a filter on those websites.
[Leechblock](https://addons.mozilla.org/en-US/firefox/addon/leechblock/) for firefox and [Nanny](https://chrome.google.com/webstore/detail/nanny-for-google-chrome-t/cljcgchbnolheggdgaeclffeagnnmhno) for chrome let's you choose when you want to not be able to access a certain website.
You could start with limiting your time on the time wasters to a few minutes per hour.
Even better. You can make it impossible to get on the time waster-sites during your work-hours.
If you care to go a little bit more radical you could just delete your accounts.

Honestly, I can really recommend the last suggestion.
Deleting, or even temporarily deactivation your account makes you realise how much you try to access the sites.

Granted, this doesn't really involve single-tasking if you don't visit these sites while you're working, but it makes it a lot easier to start single-tasking.

#### The problem with windows
The last factor in single-tasking I will touch on, is the problem of windows.
I don't (necessarily) mean the operating system.
I mean the rectangles on your screen that contain the user interface of a program.
The fact that windows exist makes it ever so easy to start multi-tasking.

To really single-task, you should keep only keep the windows open that you use.
Get rid of the chat windows that lurk in your task bar, ready to interrupt you and make sure your work fills the entire screen.
(In fact, i recommend getting rid of the task bar entirely.)

The best suggestion I have for that last part is to get a tiling window manager.
Of course, as a big Haskell fanboy, I recommend [Xmonad](http://xmonad.org).

