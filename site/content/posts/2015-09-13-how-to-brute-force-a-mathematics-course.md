---
layout: post
title: How to brute-force a mathematics course
tags: mathematics, study, work, brute-force, tutorial, definitions, theorems, notes
---

Mathematics courses are *hard*.
Exams are usually more difficult than you expected, even if you expected them to be tough.
Usually people think that you have to be really smart to understand mathematics and that most people could never do it.

I'm here to show you that there is a way to study mathematics that requires no intuition whatsoever, just a lot of hard work.
Of course this is coincidentally my personal study method.

<div></div><!--more-->

## A brute force approach

If you are the kind of person that doesn't need to study a lot to pass the exams for mathematical courses, this post is not for you.
Go and enjoy yourself otherwise!
This post is for the student who is struggling with mathematical courses but has to pass them nonetheless.
It is probably best used as a last-resort study-guide.


### Myself and mathematics

[I absolutely love mathematics](http://cs-syd.eu/quotes/2015-03-29-if-people-do-not-believe-that-mathematics-is-simple-it-is-only-because-they-do-not-realize-how-complicated-life-is---john-von-neumann.html) but I don't have any 'native' intuition for it.
Everything I know has to be explained carefully and exactly before I can start to comprehend it.
I don't consider myself a pure mathematician but I like their attitude towards pureness and proofs.
Compared to mathematicians, at least at my university, I am usually more curious about the material but a lot slower at comprehending it.

Going to class doesn't do it for me.
Classes usually go too fast for me to keep up with them and in exercise sessions, teaching assistants tend to skip over too many steps at once for me to follow what is happening.
I struggled with this kind of course for a while.
I needed to find another way.

### Goals

First things first, what are the goals here?
There could be several goals to achieve when considering a math subject.

- Passing the class
- Getting very good grades
- Studying mathematics purely
- Satisfy the personally need for knowledge on the subject
- Being able to put it on my CV
- Making your parents proud
- Impressing the teaching assistant
- ...

Mine are usually the first and the fourth.
Whatever your goals may be, it is important to make note of them.


### Structure of the course

Usually a mathematics course will consist of at least one lecture a week and perhaps some exercise sessions.
I don't necessarily recommend going to any classes but they may be more useful to you than they are to me.

Professors commonly use self-written course material that is available at the start of the course.
Usually professors don't get along with me well as I have a nasty habit of asking questions that seem trivial to anyone with a significant amount of intuïtion in the course.

At this point, it might seem as though all the odds are against me, but there at least one positive point about the structure of mathematics courses: the exam.
Exams are usually open-book, in the best case the exam is even an all-you-can-carry-open-book exam.
Further more, last-year's exam questions are mostly made available by last-years students.

### On to the hard work!

I want to give you one last warning: This is going to sound like a lot of work.
That's because it is.

#### Structure of the course material

Out of personal experience, this is how the course material is structured:

- The book consists of chapters that are sorted by increasing complexity and grouped by subject.
- A chapter consists of sections that build up to the main results of the chapter
- Chapter/sections contain the motivation for the chapter/section, definitions, lemma's, theorems, propositions and exercises.

Important to note here is that good books only use information that has already been provided.
This is usually worded as "the book builds the theory up from a solid foundation".

This method will make heavy use of the structure of the course material to describe a studying algorithm.

#### Studying algorithm

The studying algorithm is based on the assumption that you will create a 'database' of the work you've done for the course.
You will keep *every bit of work* and it's essential that you become *proud* of the work you've done.
Your notes will be neat and you shouldn't feel the need to throw away any of them.

For every part of the course-material you will perform the following steps (in this order).
You don't have to do every step of one section before you go onto the next, but you do have to do the steps sequentially on a per-section basis.

The algorithm assumes that you write your notes in such a way that new material is easily added in between older parts.
Digital notes (see my examples at the bottom of this post) are a perfect medium for this.
The notes should ideally become so complete that someone can study the subject from only your notes.

##### Step 1: reading
Read the text, focusing on discovering the goals of the section and marking every section that has a future action attached to it.

##### Step 2: consolidation
Copy the definition to your notes, as well as the lemma's, propositions, theorems, etc...
Don't just stare at the symbols like a tourist stares at Egyptian hyroglyphs.
Try to understand what you're copying but focus on having a complete collection.

##### Step 3: proofs from the material
Try to build the proofs in the material from scratch.
Use the proofs in the materials if necessary.
Again: It's important that your notes become a complete collection.
Make sure that no gaps are left in the theory.
If there are proofs missing in the book make sure you make them yourself and add them to your collection.

If a proof is skipped because it's outside of the scope of the course, make sure you note that and create a reference to the proof.
Don't just leave it out.

##### Step 4: exercises and extras
Make all the exercises. Every. Single. One.
For every definition, if applicable, explain why every precondition in the definition matters.
For every theorem, proposition, etc, give a counterexample of the theorem/proposition when you leave out one of the conditions.
If you think of something that could be a theorem, prove it or give a counterexample.

It is important that you not only understand why you are 'allowed' to take the steps that you take but also that you *reference the relevant theorems*.

##### Step 5: exam questions
Find the exam questions from the previous years and make them.
Make sure your solution is correct and explain it in great detail.
Make sure not to skip over any steps in your reasoning.
Add them to the collection.

##### Step 6: Open-source your work.
There's no point in being selfish about your academic work.
You won't succeed more if you deny others your help.
Share the fruits of your labor.

### Examples from my own work
My personal collection of notes may serve as an example of this method.

Remember:
Intuition is not something you have.
Intuition is built by making the trivial explicit.
