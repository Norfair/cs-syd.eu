---
title: Make your phone less addicting by eliminating color
tags: phone, addiction
---

This post contains a quick tip to make your phone less addicting: remove the colour.

<div></div><!--more-->

One of the things that makes your phone so much more engaging is that it has all these bright colours.
In particular, all the little colourful notification bubbles catch your attention.

This is how you can set your phone to simulate what a colourblind person would see, i.e. turn the screen to greyscale only.

### Step 1: enable developer options

[This page](https://developer.android.com/studio/debug/dev-options) contains plenty of information about how to do this for many different versions of android.
In particular: 

* Open the Settings app.
* Scroll to the bottom and select About phone.
* Scroll to the bottom and tap Build number 7 times.
* Return to the previous screen to find Developer options near the bottom.

### Step 2: Find and turn on developer options

<img src="/assets/make-your-phone-less-addicting/1.png" alt="Step 1" width="350">

### Step 3: Find 'simulate colour space'

<img src="/assets/make-your-phone-less-addicting/2.png" alt="Step 2" width="350">

### Step 4: Select 'monochromacy'

<img src="/assets/make-your-phone-less-addicting/3.png" alt="Step 3" width="350">
