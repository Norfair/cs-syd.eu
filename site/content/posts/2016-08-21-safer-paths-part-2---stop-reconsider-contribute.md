---
layout: post
title: Safer paths, part 2 - Stop, Reconsider, Contribute
tags: paths,Haskell,safety
---

Last week I published [a paths library](https://hackage.haskell.org/package/safepath).
I showed it to the Haskell subreddit and learned from their reaction.

<div></div><!--more-->

With good intentions I set out to create a safer way to handle paths in Haskell.
After a few days of coding and lots of testing, I had a first draft of a solution.
Some polishing later, I put it up on Hackage, wrote a blog post about it and put the blogpost [up on reddit](https://www.reddit.com/r/haskell/comments/4xpmpz/cs_syd_safer_paths/).

### Stop

I got very reasonable not-so-positive reactions.
The main reaction pointed me to [the path library](https://hackage.haskell.org/package/path).
This library solves the same problem (and does it better) and is much more widely used.
Subsequent reactions made me reconsider what made `safepath` different in the first place.
I decided to stop work on `safepath` for now.


### Reconsider

<!-- Naturally, after a few comments, [the xkcd strip on standards](https://xkcd.com/927/) came up. -->

There are aspects in which `path` is superior to `safepath`.
Most notably: it performs better, is already widely used and has some more advanced safety features.
The biggest (and arguably only) advantage of `safepath` was that `safepath` is tested much more thoroughly.

If I was going to make `safepath` better than `path`, I was going to have to copy a lot of features and then convince people to start using `safepath` instead of `path` in the future.
It would have been a lot of extra work, just for a more thoroughly tested safe paths library.

That is why this post's title is not "Safer paths, part 2 - `safepath` IO operations" as I had planned last week.

### Contribute

Instead of stubbornly continuing work on `safepath`, I am currently trying to contribute tests to the paths library.
You can follow the progress [here](https://github.com/chrisdone/path/pull/36).
When this is done, I will deprecate `safepath` and point any users towards `path` instead.
