---
layout: post
title: The magic of audiobooks
tags: productivity, audiobooks
---

I used to enjoy reading books.
Nowadays, I don't enjoy reading books enough to take time out of my day to do so, but I would really enjoy reading books if I had more time.
On the other hand, I am taking time out of my day for some other things that I don't enjoy.

I have found a way to still read books, while making otherwise unenjoyable time enjoyable and educational.
Allow me to show you the magic of audiobooks.

<div></div><!--more-->

### Time spent on virtually useless necessities
The main unenjoyable part of my day is commuting.
I don't like 'transporting' myself.
I can't bring any value to anything while riding a train, a car, or a bike.
Moreover, commuting is something that I call a 'brain-dead task'.

It is frustrating to realise that I have to spend time on brain-dead task while I don't have enough time to do some of the things I would actually enjoy.
I started looking for a way to combine a brain-dead task with something enjoyable.

The first thing to note is that during most of commuting, your hands might not be free, but your mind is.
You could, of course, listen to music and that would certainly be enjoyable, but that still isn't productive yet.

### The concept of an audiobook
An audiobook is a recording of someone who reads a book aloud.
Listening to an audiobook doesn't require your hands to be free, but you should be able to focus on the book.
You could even make sure that you don't disturb anyone else by using headphones.

People usually read quicker than an audiobook is read, as an audiobook can only be read at speaking rate.
Then again, listening to a good speaker is sometimes more enjoyable than listening to the little voice in your head.

### Perfect complements
```
  Commuting   <> Audiobooks    
  -----------------------------
  Unenjoyable <> Enjoyable     
  Hands on    <> Hands free    
  Brain-dead  <> Requires focus
```

As it turns out, listening to audiobooks is perfectly complementary to commuting.
You can do both, with minimal loss of accuracy on either.

### Where to get audiobooks
If you're the person who likes to read non-fiction, (auto)biographies, and other informative books, [Librivox.org](https://librivox.org/) is the place to go to.
Librivox.org contains books in the public domain that are read by volunteers.
They are free to download.

[Audible.com](https://www.audible.com) is the place to go to for any other kind of audiobook.
Audible is to audiobooks as amazon is to books.

Personally, I recommend the following audiobooks:

- [The Power of Habit](https://www.audible.com/pd/Science-Technology/The-Power-of-Habit-Audiobook/B007C64916)
- [The autobiography of Benjamin franclin](https://librivox.org/the-autobigraphy-of-benjamin-franklin-ed-by-frank-woodworth-pine/)
- [The Story of Abraham Lincoln](https://librivox.org/the-story-of-abraham-lincoln-by-mary-a-hamilton/)
- [Talent Is Overrated](https://www.audible.com/pd/Business/Talent-Is-Overrated-Audiobook/B002V9ZG70)
