---
layout: post
title: Confusing definitions
tags: definitions, mathematics, human language
---

After countless hours of discussions, arguments and even fights, I have realised that many of those would never have been necessary if we had _defined_ the words we were using beforehand.
The amount of confusion that has arisen from simply having a different understanding of what specific words mean is enormous.

I started thinking about how this issue could be resolved elegantly, or even at all.
The next discussion I engaged in, I specifically asked my conversation partner what (almost) every word they used, meant.
No longer than 5 minutes later, they got annoyed.
Then, they said this: "I can't tell you the definition of every word. You can't possibly expect me to know the dictionary by heart!"

I was at least a little surprised by their reaction.
Of course no one should be able to recite the dictionary by heart.
This was entirely not what I meant!

<div></div><!--more-->

### Back to the basics

Since we even had a different understanding of the word "definition", the next obvious thing for me to do was to look up the definition of the word "definition".

[Wikipedia](http://en.wikipedia.org/wiki/Definition) says the following:

> A definition is a statement of the meaning of a term (a word, phrase, or other set of symbols).

[Google](https://www.google.be/search?q=define+definition&ie=utf-8&oe=utf-8&aq=t&rls=org.mozilla:en-US:official&client=firefox-a&gfe_rd=cr&ei=jLUFVMG1OIqtOd6DgPgM) says something similar:

> A statement of the exact meaning of a word, especially in a dictionary.

Now, we can't just assume that these definitions are 'correct' - by which I mean "commonly accepted" - but they do seem quite correct.
These answers are, however, a little vague with respect to the implications of a definition, or how exactly a definition works.
Any person, in my not-so-humble opinion, should thorougly understand the concept of a definition.
What follows is my attempt at defining a definition.

### The definition of a definition

> When you say "I define X as Y", that is _exactly_ the same as saying "I'm going to talk about X and when I do, what I mean by 'X' is 'Y'".

_That is it._ There's nothing more to it! You can take this quite literally too, I might add.

For example, when you define "$2$" as "$0+1+1$" as [Peano](http://en.wikipedia.org/wiki/Giuseppe_Peano) did, every time you use "$2$", you could replace it (quite directly) by "$0+1+1$".
As you might imagine, defining "$50$" in an analogous manner could reduce the amount of space and time you're using significantly.
If we look at definitions in this way, they exist purely for the sake of efficient communication.
(Oh, the irony!)

This also works for non-mathematical purposes.
Depending on your interests, this is where definitions become interesting on their own.

The most common miscommunication I have personally experienced, stems from a different definition of the world 'always', so I will use it as an example.
Some may define 'always' as 'in every single instance, without any exceptions'.
Others could define 'always' as 'in most, useful instances'.

As you might imagine, this leads to all kinds of confusion and frustrations.
It is exactly why we should understand definitions.

### Considerations and misconceptions
Somehow, some people seem to get offended by this concept.
It seems as though they enjoy the ambiguity, even if it causes these miscommunications.
These are some of the misconceptions about definitions.

#### "Your definition is wrong! 'X' is not 'Y'!"

You might have noticed that, up until now, nothing has been mentioned about the truth.
That's right. Definitions don't have anything to do with truth.
There is no such thing as a wrong definition.
When you define "$1$" as "$2$" - which you certainly can - you're not saying "$1$" _is_ "$2$", or that it's _true_ that "$1$" _equals_ "$2$", but rather that you _mean_ "$2$" when you _say_ "$1$".
Whether a definition is _useful_ is an entirely different question.


#### "Of course you know what the words mean, how could you not?!"
People usually get agitated when you ask them to define a word they're using.
They find it annoying to have to specify something 'that is just so obvious'.
It's important that they realise that asking them to specify what they mean is not meant to be annoying, but rather as a show of interest.
In fact, simply accepting a difference in definitions can then be viewed as annoying, or even malevolent.

As a practical tip, you could say something like this to make sure you're not misunderstood.

> I don't mean to be annoying, but I just want to make sure that I understand what you mean.
> Could you please specify what you mean by "X"?

#### "If you just keep on defining terms, you can never even start talking!"
In a way, this is indeed correct.
The trick is to find some common ground on which you can agree on some definitions, and build your way up from there.
When you have practiced defining the terms you're using, it becomes easier to talk to other people who have done so too.
Note that you are not required to use the same definitions every time you're talking, as long as it is clear what you mean.

I really hope it is clear why it is so important to understand definitions, as well as why it is important to use them correctly.
It should be equally clear that you aren't stupid when you ask for a definition, but rather a worthwile conversation partner.
