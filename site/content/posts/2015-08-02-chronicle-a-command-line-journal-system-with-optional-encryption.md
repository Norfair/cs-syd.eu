---
layout: post
title: Chronicle, A command-line journal system with optional encryption
tags: chronicle, journal, encryption, CLI, terminal
---

"Figure out a way to journal" has been a task on my someday/maybe list for some time now.
After a short search for a good system, I came to the conclusion that I would probably be better off making my own.
I am now proud to present you "Chronicle, the command-line journal system".
It's supposed to be a small tool that does one thing very well.

<div></div><!--more-->

### Requirements
I know that I have very specific personal requirements for my workflow systems and I know that they are often very different from other peoples'.
My ideal journal system ...

- ... [has a command-line interface](http://cs-syd.eu/posts/2014-07-29-why-i-dont-like-guis.html)
- ... has an extremely low transaction cost for writing new entries
- ... has very few dependencies
- ... allows me to use my own editor
- ... has options for encryption
- ... has options for synchronisation
- ... uses a portable and future-proof file format.
- ... is free and open-source.

At this point, it might already be clear why I had to make my own system.
I found only one command-line option: [jrnl](https://github.com/maebert/jrnl), but it already had the python dependency and I didn't like the way it stored its files.
Moreover, I was confident that I could build my own system relatively quickly.

### A new system
Chronicle has a command line interface.
You really only need to know the `enter` command to start using it.

```
$ chronicle enter
```

Chronicle will open your preferred editor and save the new entry when you are done.


#### Alias
To satisfy the transaction cost requirement, I wrote an alias:

```
alias j='chronicle enter'
```

This means that I only need to type a one-character command to start a new journal entry.
That's as low a transation cost I think I can get.


#### Dependencies
Chronicle is just a big bash script.
The only dependency it has, is `openssl`, and that's only if you use the encryption option.

#### File format and synchonisation
Chronicle uses text files and only one of them per entry.
That means you will be able to read those files in 50 years if you decide to keep them.
That also means that you are able to synchonise your entries easily without the possibility of generating conflicts.

#### Encryption
Chronicle has built-in support for encryption.
This way your secrets will be safe for anyone without the password.
If you would like an encryption algorithm that guarantees integrity of your files as well, you could set the `ENCRYPTION_METHOD` as necessary.


#### The name
I chose the name because of this definition of the noun "chronicle":


> chronicle:
>  a factual written account of important or historical events in the order of their occurrence.

I've since realised that this name is far from unique for a program, but I'm sure you'll find Chronicle [on Github](https://github.com/NorfairKing/chronicle) if you would like to use it.

#### The source
The [source](https://github.com/NorfairKing/chronicle) is of course freely available.
Any improvements, suggestions, bug-reports and comments are very welcome.

https://github.com/NorfairKing/chronicle
