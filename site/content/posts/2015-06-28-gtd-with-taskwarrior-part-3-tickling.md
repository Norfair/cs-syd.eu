---
layout: post
title: "GTD with Taskwarrior, Part 3: Tickling"
tags: Taskwarrior, productivity, work, GTD, tutorial, CLI, terminal, tickle
---

In the third part of this series on how to use Taskwarrior to implement GTD, I will show you the third step of GTD: Using a tickler file.

A tickler file system is usually implemented using 43 file folders.
31 for the days of the month and 12 for the months of the year.
Every day you open the folder for that day and receive the 'stuff' in your inbox.

<div></div><!--more-->


I personally like a paper-based tickler file but Taskwarrior seems particularly well equipped to handler a digital tickler file.


### Tickler file requirements
- Tickle items should only be visible on (and after) the day you need to see them.
- For every tickle item, you should know why you get it in your inbox (again).
- For every tickle item, you should know when you put it in your tickler system.
- The transaction cost of using a tickler system should be as low as possible.


### Tickler file with Taskwarrior
Remember that a tickle item is something that has to end up in your inbox at a very specific time.
When implementing a tickler system digitally this can be done much easier than using 43 file folders that you check every day.
Your computer can check your tickler file for you and just put it in your inbox again.
This way you don't even need to develop a habit, which we all know is hard work.

Adding `wait:` metadata to a task ensures that the task won't be visible until the date that you set this metadata to.
For example, when you enter the following command, the task doesn't show up until the following Sunday:

```
task +in wait:sunday Remind Dave to water his plants.
```

Of course we will use an alias to do this more easily.
We used an `in` alias to add 'stuff' to your inbox.
Tickle items are also 'stuff', so we are going to use that `in` alias in the definition of the `tick` alias.

``` bash
tickle () {
    deadline=$1
    shift
    in +tickle wait:$deadline $@
}
alias tick=tickle
```

This `tick` alias allows you to add new tickle items by entering something like this into your terminal.

``` bash
tick monday Put the office plants into the sunlight
```

This way the task will only show up in your inbox the following monday.
Of course you can also use full dates like `2015-06-30`.


#### Thinking it over

Personally, I often encounter a situation where I need to think something over and decide what to do with it later.
This really doesn't have to end up on a task list because then there's the possibility that I'll see it before I'm ready to think about it.
For precisely that situation, I made the `think` alias:

```bash
alias think='tickle +1d'
```

It is important that you don't ever `think` something over twice.
You don't want to create a snooze button!
A good practical tip is to make sure that every `think` item is a simple yes/no question.
