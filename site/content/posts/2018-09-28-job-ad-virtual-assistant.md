---
title: Job Ad: Part-time Remote Virtual Assistant
tags: virtual assistant, productivity
---


Help Wanted: Part-time Remote Virtual Assistant

<div></div><!--more-->

### Job Description: Responsibilities

- Admin work
- Web research
- Email
- Travel planning and booking

### Employment terms

- Remote
- Freelance / Agency only. Must be able to produce invoices.
- Part-time: 5-10h/month
- Flexible hours

### Requirements

#### Essential

- English (fluent)
- Independent
- Good at keeping track of work, getting things done.
- Excellent communication skills
- Trustworthy
- Polite

#### Ideally also

- German (fluent)

### Application process

- [Send an email with your CV](/contact)
- Video chat
