---
layout: post
title: Intray
tags: Haskell, productivity, programming, project
---

I am happy to announce a new little tool called [Intray](https://intray.cs-syd.eu).

<div></div><!--more-->

Intray started as a need I had for a digital GTD in-basket.
None of the existing solutions that I could find had all of the following features:

- Meant for processing items, not storing them.
- Automatable (has an API)
- Has a CLI
- Simple, no bloating features
- Open-source and self-host-able
- A way to quickly add items via my phone (more than 3 clicks is too slow)
- No advertisements
- A way to synchronise between my phone and my desktop computer
- A minimal amount of clicks necessary to add an item: more than 3 is certainly too many. For reference; google keep requires 6 clicks.

With this in mind, I built Intray to those specifications.
It is deliberately simple. You can only add text, look at one of the pieces of text, and delete that piece of text.

Intray is open-source and always will be.
Given that I need Intray myself, it will be free (of charge) to use at https://intray.cs-syd.eu/ until I need to set up additional infrastructure to support all of the users.

[The source code is available on GitHub](https://github.com/NorfairKing/intray) and is written in Haskell.
