---
title: Hacktoberfest 2020
date: 2020-10-01
tags: haskell, smos, programming, hacktoberfest
---

[Hacktoberfest](https://hacktoberfest.digitalocean.com/) is happening again in 2020 and CS SYD will be participating again.

<div></div><!--more-->


Contributions are very welcome, and do not need to be pre-defined.
Below are some good issues to get started with.
Documentation in particular is very welcome, and can be a great way to get started with any given project.

[Last year we had some lovely contributions](/posts/2019-11-14-hacktoberfest-in-review), so I will do my best to help keep up this good work.

If you want to learn more about testing, property testing, GTD, distributed systems or all-round real-world Haskell programming, this could be a great opportunity!

### Projects

The following repositories have some [Hacktoberfest](https://hacktoberfest.digitalocean.com/) issues:

- [Validity](https://github.com/NorfairKing/validity): Validity-based testing
- [Hastory](https://github.com/NorfairKing/hastory): Terminal use optimisation
- [Autorecorder](https://github.com/NorfairKing/autorecorder): Automatic declarative [ASCIInema](https://asciinema.org/) recordings
- [Intray](https://github.com/NorfairKing/intray): A GTD capture tool
- [Tickler](https://github.com/NorfairKing/tickler): A GTD tickler system
- [Smos](https://github.com/NorfairKing/smos): A purely functional semantic forest-based editor for Getting Things Done.

All these projects are real world, ethically sustainable projects written in boring Haskell.


You are always welcome to email me for advice or with questions.
I will be available to help, coach, guide and review.



Happy Hacking!

