---
title: Introduction Self-management with Smos: the waiting-action report
tags: smos, self-management
---

This is the second post about using Smos for self-management in practice.
It explains how to deal with waiting for things to happen and following up on them.

<div></div><!--more-->

### Keeping track of things you are waiting for

When you are waiting for someone to get back to you, it is still _your responsibility_ to make sure that they do and to follow up if necessary.
Your self-management system should be set up to do this well because it is a very common case.
In fact, about one third of your projects will have a "waiting" status at any time.

### Waiting entries in Smos

In Smos, the only thing you need to do to mark something as `WAITING` is activate the `entrySetTodoState_WAITING` action (`tw`).

There is also a convenience action for when you have finished your current task and the next thing on the list should be a `WAITING` entry.
In this case, activate the `convDoneAndWaitForResponse` action (`<space>nw`) and fill in who you're waiting for.

### The waiting report

Now that you are tracking `WAITING` entries, the last step is to use them and review them.
To review the things you are waiting for, run the `smos-query waiting` report.

This report will show you all the things that you are waiting for, in descending order of how long you've been waiting for them.
The entries are even colour coded by whether you should be doing something about them:

<script 
  id="asciicast-348772" 
  src="https://asciinema.org/a/348772.js" 
  async>
  data-autoplay="true"
  data-loop="1"
  data-cols="80"
  data-rows="25">
  </script>


Entries will either be green, blue, yellow or red.
Red means you've been waiting for longer than seven days and you should definitely do _something_.

If that happens, you have some choices:

- Ping whomever you are waiting for.
- Give up on this task, cancel it.
- Choose a different next action.

There is only one thing that you must not do, which is to ignore this issue.

### References

For more information about Smos, see [the documentation site](https://smos.cs-syd.eu/smos-query/waiting).
