---
layout: post
title: A poor man's backup system
tags: backup, harddrive, RAID
---

It has been made clear that digital data is not safe.
I once had a backup system, but the one time I needed a backup, my backup harddrive failed and I lost all my data.

At university, I learned about RAID systems, but they are too expensive and too cumbersome for my purposes.
That's why I made my own little cheap backup system.

I understood that you need to save data in at least two different locations for them to be at least somewhat safe.
I bought two external hard drives with which I built a makeshift RAID1 system.
Here's what I did:

<div></div><!--more-->

#### A filesystem
First things first.
I plugged in my harddrives via USB, but they're not being mounted automatically, so need to locate them.

    $ lsblk
    NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
    ...
    sdc      8:48   0 931.5G  0 disk 
    └─sdc1   8:49   0 931.5G  0 part 
    sdd      8:64   0 931.5G  0 disk 
    └─sdd1   8:65   0 931.5G  0 part 

Oh, right, I'm going to need sudo rights for this.
Let's just get those rights right now, so I don't have to type `sudo` everywhere.

    $ sudo su

I don't like Windows' filesystems, but they're on most harddrives by default, so I'm wiping those off my drives.

    $ wipefs --all /dev/sdc
    $ wipefs --all /dev/sdd

Now that I have two clean disks, I need to partition them again, and add a new filesystem.
I'm just going to make two single-partition drives.
There's no need here to add any more partitions.

    $ fdisk /dev/sdc
    ...
    $ fdisk /dev/sdd
    Command (m for help): n
    Partition type
       p   primary (0 primary, 0 extended, 4 free)
       e   extended (container for logical partitions)
    Select (default p): p
    Partition number (1-4, default 1): 
    First sector (2048-1953458175, default 2048): 
    Last sector, +sectors or +size{K,M,G,T,P} (2048-1953458175, default 1953458175): 
    
    Command (m for help): w
    The partition table has been altered.
    Calling ioctl() to re-read partition table.
    Syncing disks.

Next, I need a fun file system.
My friend recommended [BTRFS](https://btrfs.wiki.kernel.org/index.php/Main_Page), for multiple reasons.

- It's open source (GPL).
- It focusses on fault tolerance and repair.

Honestly, I just wanted to try something other than ext4 for a change.

    $ mkfs.btrfs /dev/sdc
    ...
    $ mkfs.btrfs /dev/sdd
    Btrfs v3.17.1
    See http://btrfs.wiki.kernel.org for more information.

    Turning ON incompat feature 'extref': increased hardlink limit per file to 65536
    fs created label (null) on /dev/sdd
        nodesize 16384 leafsize 16384 sectorsize 4096 size 931.48GiB

Finally, I could mount my system for the first time.
I chose a spot in the root directory, but that doesn't really matter.

    $ mkdir /backup
    $ mkdir /backup/hdd0
    $ mkdir /backup/hdd1

    $ mount /dev/sdc1 /backup/hdd0
    $ mount /dev/sdd1 /backup/hdd1
    
There, now I have two identical empty 1TB harddrives.

At this point, it's very easy to use these as a backup system.
I just write to the first disk, and copy everything over to the second with rsync.
    
```bash
rsync --recursive /backup/hdd0 /backup/hdd1
```

I could have the drives mounted all the time, and put this command into a cronjob, but I don't use my backup system that much, so I just mount the drives whenever I'm making a backup, or restoring one.


