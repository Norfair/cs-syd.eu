---
title: Self-management: Clarify and Process
tags: self-management, smos
---

This is the next post in a series on self-management.
It introduces the clarifying workflow and Smos as a processing tool.

<div></div><!--more-->

### Why clarify and process

Clarify and process are the second and third steps in the GTD overview, respectively.
They happen after the capture step.

In the clarify step, you figure out how you will deal with a piece of input that you have allowed into your life and subsequently captured.
In practice, that means you'll find a piece of paper in your box, an email in your email inbox or an item in your [Intray](https://intray.eu).

Once you have **thought** about what you will do with this piece of stuff, a strenuous exercise, you will want to ensure that you do not have to do that twice.
You then put the results of that thinking in the appropriate place so that you can trust that the rest of your system will ensure that you are reminded of the results in the right place at the right time with the right mindset.

### Where and how to clarify and process

To get started, look at a single piece of stuff and follow the GTD flowchart:

<div class="columns is-centered">
  <div class="column has-text-centered">
    <img src="/assets/processing/flowchart.jpg" alt="GTD flowchart" width="512px">
  </div>
</div>

The following is a short overview of this flowchart, follow this list of posts for how to do the following in practice with real tools:


First, decide whether whatever you are looking at is actionable.
Does anything need to happen?

If nothing needs to happen, either:

1. Throw it away entirely.
2. Put it on your list of things you might someday want to do.
3. Put it in your reference system to be able to look at it again later.


If something needs to happen, brain-dump all the things that need to happen.
Is it more than one thing? Then it is a project.
Name the project and put it on your projects list.


Decide what the very next thing is that you need to do.
If the next action takes fewer than two minutes to do, then just do it immediately.

If it takes more than two minutes, either delegate it to someone else and put it on the list of things that
you are waiting for from other people, or defer it.
When you defer it, put it on your next action list if you want to do it as soon as your context allows for it, or put it on your calendar if it needs to happen at a specific date.
We recommend against scheduling work, but meetings still need to happen at a specific time and not "as soon as possible".

### Why Smos

[Smos](https://smos.cs-syd.eu) is specifically designed to be the perfect system for this workflow.

- Smos is optimised for speed. 
  It has a bit of a learning curve, but that is the price of ultimate velocity.
  It is a keyboard-first tool for processing with optional mouse-interaction.

- Smos uses a future-proof file-format: a subset of simple Yaml.
  This means that your data never becomes unreadable.

- Smos is automatable: the file format is machine-readable and ideal for integration with your own tools.

- Smos is a local-first tool so that you alone control your data.

- Smos synchronises across devices

- All Smos software is entirely free and open-source and can be self-hosted.

- Smos is completely customisable, so that you can make the tool work for _you_.
