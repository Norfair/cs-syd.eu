---
title: Introduction to self-management
tags: self-management
---

This is the first post in a series about self-managament.
It describes the content that I have in mind and the motivation for self-management.

<div></div><!--more-->



### Why manage yourself?

There are three great reasons to engage in self-management:

* To ~reduce~ eliminate work-stress
* To ensure you honour your commitments, both to yourself and to others.
* To get _more_ done.


In work and in life, no other person will even be on your side as much as you can be.
It is your job to take ultimate responsibility for your life.

When it comes to creative endeavours like software development, system administration, managing projects or teams and even running an entire company, stress will be counterproductive.
You may be able to cap bottles on a conveyor belt while stressed, there is no way you will be able to perform good creative work under stress.
Stress prevents good creative intuitive ideas.
Self-management allows you to do better work through stress-free productivity.

Taking responsibility for your work via effective self-management will allow you to have a complete overview of all your commitments, such that you will never again forget about pieces of work.
Having such an overview also lets you be on time and mentally present for your in-person engagements.

Getting _more_ done is a good reason to get into self-management as well, but it should definitely not be the most important one.
You _will_ get _more_ done, but the other aspects will matter much more in terms of your overall effectiveness.

### Table of Contents

Self-management is a big topic about which I have a lot to write, so this series may run long and this list may be updated in the future.
This series will approach self-management from a practical perspective, with concrete tools in mind.
The audience I have in mind is an ambitious and/or stressed engineer, technical leader, manager or executive.

These are the contents that I have in mind.

* Intro: Why manage yourself (this post)
* Pieces and tools
  * Capture (Intray)
  * Tickling (Tickler)
  * Calendar
  * Processing (Smos)
  * Reference system
* Intray for capturing
  * Why Intray & Installation
  * Using intray online
  * Using the intray app
  * Using the intray command-line application
  * Using the intray android App
  * Capturing examples
  * Capturing instant messaging
  * Setting up intray in your prompt
  * Synchronisation and setting up your own intray server
  * Setting up intray using nix-home-manager
* Smos for processing
  * Why smos & installation?
  * Getting started: Using smos as a list/tree manager
  * Getting started with smos for self-management: next actions and the next-action list
  * Processing examples, part 1
  * Waiting-for entries and the waiting-for list
  * Smos for org-mode users, part 1
* Tickler for tickling
  * Why Tickler & Installation
  * Using tickler online
  * Using the tickler command-line application
  * Synchronisation and setting up your own tickler server
  * Setting up tickler using nix-home-manager
* Context-based work with Smos
  * Tags for context-based work
  * Properties for context-based work
  * Working with Smos-query work
  * Processing examples
  * The someday maybe list 
  * The projects list
  * The weekly review
  * The agenda in Smos
  * Clocking in smos
  * Processing examples, part 2
  * Setting up smos using nix-home-manager
  * Synchronisation & Setting up your own smos sync server
* Optimising email
  * Setting up mutt with offlineimap and msmtp
  * Imapfilter to focus email into one account
  * Offlineimap to only download email once per day
  * Msmtpq to only send email once per day

### Acknowledgements

Much of what I describe here draws inspiration from "Getting Things Done" by David Allen.
The biggest difference between that book and this content is that this content is more oriented towards techies, power users, and software developers/system administrators/product leaders/technical leaders/engineering managers in particular. 
This content is also more opinionated and focused on the tooling in practice.
