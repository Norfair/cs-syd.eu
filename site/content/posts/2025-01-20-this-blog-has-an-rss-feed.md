---
title: This blog has an RSS feed!
tags: rss
---

This blog has an RSS feed. You can subscribe to it at [`https://cs-syd.eu/rss.xml`](/rss.xml).
There is also an Atom feed at [`https://cs-syd.eu/atom.xml`](/atom.xml).

