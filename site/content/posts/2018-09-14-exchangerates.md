---
title: Announcing `exchangerates`
tags: exchange rates, accounting, Haskell
---

This post announces the new [`exchangerates`](https://hackage.haskell.org/package/exchangerates) library.
It allows you to fetch exchange rates from the [https://exchangeratesapi.io/](https://exchangeratesapi.io/) website.
Formerly [https://fixer.io](https://fixer.io).

<div></div><!--more-->

Free currency exchange rate API's are surprisingly hard to find.
[https://exchangeratesapi.io/](https://exchangeratesapi.io/) is the only one that I found, so I decided to write a little Haskell client to call the API.
The API is free to use, and the author asks to cache the results as much as possible, so the Haskell client automatically caches all results as much as possible.

The library is laid out as follows:

- [ExchangeRates.Types](https://hackage.haskell.org/package/exchangerates/docs/ExchangeRates-Types.html) contains the types for currencies, rates, etc...
- [ExchangeRates.API](https://hackage.haskell.org/package/exchangerates/docs/ExchangeRates-API.html) contains the raw API types, made with [Servant](https://haskell-servant.github.io/).
- [ExchangeRates.Cache](https://hackage.haskell.org/package/exchangerates/docs/ExchangeRates-Cache.html) contains the caching functionality
- [ExchangeRates.Client](https://hackage.haskell.org/package/exchangerates/docs/ExchangeRates-Client.html) contains the code to actually call the API.
- [ExchangeRates](https://hackage.haskell.org/package/exchangerates/docs/ExchangeRates.html) re-exports all the relevant functions and types to use the library.

To call the [https://exchangeratesapi.io/](https://exchangeratesapi.io/) APi in the simplest way possible, we need to specificy a cache file, and our query.
The following is a minimal example that gets the exchange rates for 2018-01-19 and prints them to standard output:


``` haskell
main :: IO
main = do
    rates <-
        autoRunExchangeRatesClient $
          withFileCache "/tmp/exchangerates.cache" $
            getAtDate (fromGregorian 2018 01 19) (Just EUR) Nothing
    case rates of
        Left err -> die $ show err
        Right v -> print v
```

This little piece of code will call the live API and save the cache with results to `/tmp/exchangerates.cache`.
The next time this piece of code is run, it will read `/tmp/exchangerates.cache` and notice that it does not need to call the live API anymore, and just print the results.
