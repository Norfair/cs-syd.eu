---
title: Talk: Practical property testing
tags: talk, property testing, haskell
---

I gave a talk about practical property testing at FP in the city 2020 online on 2020-09-10.

<div></div> <!--more-->

The talk was recorded and can be found online: 

<iframe
  width="560"
  height="315"
  src="https://www.youtube.com/embed/IsdfoqL1k_U"
  frameborder="0"
  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
  allowfullscreen>
</iframe>

Slides are available [on GitHub](https://github.com/NorfairKing/practical-property-testing-in-haskell)
Feedback is always welcome via email.

