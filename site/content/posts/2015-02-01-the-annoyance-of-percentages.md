---
layout: post
title: The annoyance of percentages
tags: percentage, annoyance
---

In this post I would like to go a very specific rant.
I've been in a discussion lately that, once again, aggravated me more that it should have.

<div></div><!--more-->

#### Stupid formula's
The discussion started when someone asked me to explain a formula to them.

The formula (somewhat simplified), was something along these lines.

The chance, in percentages, to draw a red card from a deck of cards is:

$$\frac{26}{52}\cdot 100$$

Here, $26$ is the amount of red cards in a deck, $52$ is the total amount of cards in a deck and $100$ is ... the amount of percents in a total (?!).

Let me ask you a question first.
For each of the following expressions, tell me whether it's _written in percentages_ or not.

$$ (1)\ \frac{26}{52}\cdot 100 \quad (2)\ \frac{26}{52}\cdot 100\% \quad (3)\ \frac{26}{52} \quad (4)\ \frac{26}{52}\% $$

Let's see.
Obviously the first and last expressions here are just incorrect.
The first one gives you a value that is a hundred times to large, the last one gives you a value that is a hundred times too small.

What about the second and third expression.
Are they written in percentages?

I'm guessing you said 'yes' for the second formula and 'no' for the third.
That is what you've probably been taught in primary school.

The question is, of course (sigh), somewhat vague because 'written in percentages' is not an exact expression.

In this discussion, I argue that both of these expressions are written in percentages and the first one is counterproductive.

#### The meaning of percent symbol
The first thing I have to rant about is the meaning of the percent _symbol_.
The meaning of the percent symbol is congruent with its pronunciation:
$n \%$ literally means $n$ 'per cent' which comes from the Latin 'per centum', meaning "by the hundred".

This means that if you write $45\%$, it really means $\frac{45}{100}$ (or $45/100$ if you want to keep it on one line).
There is absolutely no semantic difference.

#### The use of the percent symbol
Usually a percent symbol is used to indicate that the number is meant relatively.
For example: '$50\%$', or 'one half' of whatever whole is is relative to.
This as opposed to an absolute value.
$0.5$ and $50\%$ are both equal to one half, but the first value absolute and the second one is relative.

The semantics of the percent symbol therefore both contain the meaning of the factor $\frac{1}{100}$ and the relativity of the value.

#### Back to the formula
Let's look at the second formula and rewrite it according to the meaning of the percent symbol:

$$ \frac{26}{52}\cdot 100\% = \frac{26}{52}\cdot \frac{100}{100} =  \frac{26}{52} $$

Do you see where I'm going with this?
This is the third formula!

The value of the second and third formula are equal, but the third formula does not inherently entail the relativity of the value.
One could argue that, because these value concern chances, these are always meant as a relative (to $1$) measure but that's a happy accident.

#### My annoyance
You might have noticed that I'm taking this pretty far.
My annoyance with percentages boils down to the combination of two completely independent pieces of semantics: relativity and some seemingly random factor.

The relativity of a value is a very useful piece of semantics.
This part is great.
It's meaningful.
I actually think there should be a symbol just for that.

The factor $\frac{1}{100}$ that gets added to a value by the percent sign is, in my opinion, completely pointless.
To imagine a percent, you tend to think of 'one out of every one hundred', but one hundred is no more useful than any other (small) number.
One in twenty (a pervingt, if you will) is just as easy to imagine as one in a hundred.
Most of us even automatically simplify $50\%$ to one half and $12\%$ to one eight and at that point the factor becomes useless at best.
Even worse, because we're adding a factor $\frac{1}{100}$ due to the symbol, often we have to add a factor $100$ to get a correct value!

Here's what I propose:
We drop the percent symbol altogether and indicate whether an expression is relative in words.
If an expression is relative, we have to specify to what it is relative in words anyway, so an other symbol may not be necessary.
