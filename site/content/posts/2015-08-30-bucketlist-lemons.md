---
layout: post
title: "Bucketlist: Lemons"
tags: lemons, life, tshirt
---

For the first time ever I can scratch an item off my bucket list:

> Hand out lemons wearing a shirt that says "LIFE".

<div></div><!--more-->

I wanted to celebrate getting an internship at Google with something special.
Because I didn't use to live in an English speaking country most English idioms were unknown to most people.
That meant I couldn't make references to them.
Now I was going to live in London for three months.
The timing was perfect to finally scratch this item off the list.

![Bucketlist: Lemons](/assets/bucketlist-lemons/lemons.jpg)

I gathered some of my friends and my brother who were awesome enough to help me with this.
We went to a market, bought one hundred lemons and headed to St James' Park.

It turns out that handing out free lemons is not as easy as I thought.
At first we just held out our hands holding a lemon and said "Lemon?" and people would look at us with a confused expression on their face or ignore us entirely.
They would then walk past us faster than they could read what was on our shirts.

Handing out one hundred lemons started to look like a difficult task.
We were determined to hand out all the lemons before we went home.

We started successfully handing out lemons when we realised that that only works if you're very enthusiastic about it and allow people to take the time to read the tshirt.
We had to have a very big smile on our faces, approach people head-on and slowly so that they had time to read and loudly, clearly and enthusiastically say "Would you like a free lemon, Sir?"

Even then we got very mixed reactions.
Some people would ignore us, some people would just awkwardly take a lemon and walk on.
Some people would say 'no' with an annoyed tone.
One of my friends would tell them: "Well no lemonade for you then!".

There were also more fun reactions.
Some people would smile when they saw us coming and happily accept a free lemon.
The best reactions came from the people who wanted a picture with us:

![Bucketlist: Lemons](/assets/bucketlist-lemons/lemons2.jpg)

If have a lot more items on my bucket list but I'm very glad to have started scratching items off.
