---
title: Testing for beginners at Monadic Party
tags: announcement, speaking, Haskell, property testing, testing
---

I am pleased to announce that I will be speaking about testing at [Monadic Party](https://monadic.party). 

<div></div><!--more-->

I will be giving six hours of talks about "Testing for beginners".
This is a huge topic and I would want to talk about it for many more hours, but I have narrowed it down to these talks:

### "Testing for beginners, in Haskell" and "Testing, for beginners in Haskell"

Everything you need to know to get started testing, and to get started testing in Haskell

Bugs exist because an incorrect implementation was written instead of a correct implementation.
More bugs exist if an incorrect implementation is more likely to be written than a correct implementation.
In a perfect world, we would make it impossible to write an incorrect implementation.
In reality this is impractical.
The next best thing we can do is to make it less likely for an incorrect implementation to be written than a correct implementation.
Types can do part of this, but not all, and that is where testing comes in.

### Practical Property testing in Haskell

This talk discusses how to implement proper property testing in Haskell.
It moves way beyond the classic toy examples and examined real-world scenarios.
It takes a deep-dive into how to make property tests interact with web services or database systems. 
It also explaines how to use custom generators and testing combinators.
Other topics discussed are as follows:

- Testing with Databases
- Testing with Services
- Writing Custom Generators
- Using Testing Combinators

### An overview of Property testing in Haskell

Property testing has been around for two decades, and the design space is
rapidly being explored, but keeping up with these developments has not been
easy. This talk gives a thorough overview of the different libraries and
explores their tradeoffs. Libraries discussed in this talk include QuickCheck,
Hedgehog, Validity, Quickspec, Easyspec, speculate, leanspec and smallcheck.

### Validity-based testing in Haskell

Writing correct software is difficult. Property testing has the potential to
make safe software easier and cheaper. However, current methods still involve
the costly requirement for programmers to write generators, shrinking functions
and properties. Validity-based testing promises free generators, free
shrinking and cheap properties, to allow property-based testing to fulfill its
true potential.

### Signature inference for functional property discovery

Property discovery is the process of discovering properties of code via
automated testing. It has the potential to be a great tool for a practical
approach to software correctness. Unfortunately current methods remain
infeasible because of the immense search space. We contribute a new
approach to taming the complexity of a state-of-the-art generate and test
algorithm. Our approach runs this algorithm several times with carefully
chosen inputs of smaller size: signature inference. We implement this
approach in a new tool called EasySpec. The results suggest that this
approach is several orders of magnitude faster, fast enough to be practical,
and produces similar results.

