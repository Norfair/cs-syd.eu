---
layout: post
title: How to get up early
tags: waking up, tips, early
---

Ever since I wrote about how [I love getting up early](/posts/2014-08-02-the-magic-of-getting-up-early.html), I have had people asking how I ever did it.
How could I ever get up at 3am when some people only go to sleep at 2am.

I would like to give some concrete tips for anyone that actually _wants_ to get up early.
You might not want to get up at 3am, but you may want to get up an hour earlier in the morning, even if it's just to take a longer shower, or have a more relaxed breakfast.

<div></div><!--more-->

### The obvious tips

- Go to bed earlier.

Yes, it's obvious.
No, you don't really want to.
Yes, it's essential to get up earlier.
You might want to ask yourself this question: "At what point in time, do you not do anything 'useful' anymore?"
It's very important to note that rest is useful too!
For example: after 8PM, I don't do any work anymore, so have another hour or two to relax and then I go to bed.

- Sleep a little less.

Some people actually sleep badly because they can't fall asleep at night.
It is possible to sleep a little less so that you're actually tired and ready to sleep when you go to bed, rather than either staying awake too long or lying in your bed awake for hours.

- Be consistent.

A lack of consistency is the reason that most people don't get up early.
To get up earlier with as much or more energy than before, you have to do it consistently.
You can't just get up earlier once, tired and grumpy, sleep in the next day and conclude that "you're just not a morning person".
To be honest, you will probably be a little grumpy the first few mornings, but after a few days you will wake up with more energy and more time in your day.

### The practical tips

- Set more than one alarm clock.

Ideally, you set at least two alarm clocks.
This has as a primary purpose to increase the transaction cost of snoozing an alarm.
If you set two alarm clocks, and you know that the other one will go off in 5 minutes, you will be less inclined to snooze any of them.

- Position your alarm clock to the other side of the room.

This is another way to increase the transaction cost of sleeping in.
If you have to get out of your bed and walk over to a table, you will already be on your feet and more likely to get up.

### The less obvious tips

- Sleep [polyphasicly](http://en.wikipedia.org/wiki/Polyphasic_sleep).

Sleeping in one continuous sleep phase is the [electric meat](http://matt.might.net/articles/electric-meat/) of sleep behaviors.
Consider going to bed at 10PM, getting up at 3am and getting two more hours of sleep during the day.
I personally need less sleep and get more energy if I sleep twice a day, or more.

- Sleep multiples of about 90 minutes.

It turns out that humans sleep in cycles of about 90 minutes, of which the first three are the most important.
Waking up at the very end of a sleep cycle seems to result in more energy to start the day.
When setting your alarm, keep in mind that sleeping for seven and a half hours may leave you more rested than sleeping for eight hours.

- Learn to empty your mind.

This is probably the hardest tip to apply, because it requires working on your personal stress.
Emptying your mind, however, allows you to fall a sleep quicker and easier.
I have gone from lying awake for half an hour to falling asleep in a matter of seconds.

### Before you get started

Now, before you start getting up early, you have to have made it very clear to yourself why you are doing it.
You might want to take your time to wake up more easily, have a more extensive breakfast or have a relaxing morning walk.
On the other hand, you might want to get an extra hour of work done in the morning.
In any case, it will be useful even to write down why you are getting up early.
In the morning, when your alarm goes off, your brain starts to think of hundreds of good excuses to stay in bed, any of which will sound more attractive than getting up.
At that point, you will need to convince your mind that [it's a good idea to get up](/posts/2014-08-02-the-magic-of-getting-up-early.html).
I promise, though, that getting up earlier is at least worth trying.



