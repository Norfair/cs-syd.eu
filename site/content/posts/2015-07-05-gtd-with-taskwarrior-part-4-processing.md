---
layout: post
title: "GTD with Taskwarrior, part 4: Processing"
tags: Taskwarrior, productivity, work, GTD, tutorial, CLI, terminal, processing
---

In the fourth part of this series on how to use Taskwarrior to implement GTD, I will show you the fourth step of GTD: processing your 'stuff'.
This is where the real heavy-on-brainpower work comes in.

<div></div><!--more-->

Every 'in' item should be processed by following this flowchart:

![A GTD flowchart](/assets/gtd/flowchart.jpg)

That is at least the idea.

### Processing requirements
- Processing your inbox should be very easy to start with.
- Processing is about making decisions, not about starting or finishing tasks.

### Processing with Taskwarrior
To start processing your inbox, type `task in`.
You should now see your inbox items if you've followed this series up to this point.
For every task you see there, go over the following questionnaire.

1. Decide whether the item is actionable.
  - It's not: Decide whether to keep note of it in your reference system, whether you want `think` it over of just delete it.
    (Remember, when using `think`, you should enter a simple yes/no question.)
    Then check off the task.
  - It is: go on.


2. Is there more than one action required?
  - Yes: Start up a new project.
Figure out the desired outcome for the project and add the first `+next` action to it:
Don't forget to add the `+next` tag.

``` bash
task add +next pro:new.project "Set up a new git repository for the fancy new project"
```

Also add as many tasks as you can possibly think of for this project.
Then check off the task.

  - No: Decide what action the `+in` task represents and go on.


3. Decide how long the action is going to take.
  - It will take no longer than *two minutes*: *Do it right now*.
    Then check off the task.
  - It will take longer than two minutes: Estimate how long it would take to complete the action and go on.


4. Are you going to delegate the task?
  - Yes: Modify the task to be a `waiting` task and annotate it with instructions on how to check whether the task is done yet.
Then *delegate the task*.
Let's say the task has ID `n`.

``` bash
task n modify -in +waiting
task n annotate "Check: Call Dave and ask about the car tires." due:+1w wait:+1d
```

I added `due:+1w`, which means that Dave has a hard deadline of one week to complete this task and `wait:+3d`, which means that I can't see this task until tomorrow so that I won't pester Dave about it until at least three days from now.

  - No: Go on.
    

5. Defer the task

You know which *project* the task belongs to.
Add your task to that project.
If it's the `next` action for that project, don't forget to add the `+next` tag and check the `in` task.

``` bash
task n done
task pro:gtd add Call Jamie to schedule a meeting.
```

Make sure the action is really an *action*.
It must be something that you can start doing immediately without thinking about "how?", "where?" or "why?" thanks to the thought you put into it at this moment.

When you get into the habit of asking yourself these questions, there should come a point where you don't need the flowchart anymore.

#### Contexts

Later, when you finally get around to *doing*, it will be very important for your mental health to keep in mind the *context* in which you are working.
Luckily, Taskwarrior makes it very easy to allow you to use your context to your advantage.
For now, all you need to remember is to always add at least one context tag to a tag.
Common context tags are `+@office`, `+@phone`, `+@computer`, `@online`, ...

It might not look like it, but this is actually *very* important.
I would argue it's the most important meta-data on your tasks.

#### Advanced Applied Laziness

##### Research

Often the very next task for a project is just a whole lot of 'looking into something'.
For that reason, here is the `rnd` alias:

``` bash
alias rnd='task add +rnd'
```

Because, for me, looking into something always means looking it up on the internet, I add `+@computer` and `+@online` to the alias.
Moreover, it's always a next action as well:

``` bash
alias rnd='task add +rnd +next +@computer +@online'
```

##### Read and review

Reading something later, when you know very well what it is you want to read is called 'read and review'.
Ideally, you know exactly what you need to do, which drawer you need to open, which file to open, which link to point your browser to,...
Taskwarrior allows you to keep this info in annotation with `task annotate`.

For me, read and review always happens on the internet.
Enter the `rnr` alias:

``` bash
webpage_title (){
    wget -qO- "$*" | hxselect -s '\n' -c  'title' 2>/dev/null
}

read_and_review (){
    link="$1"
    title=$(webpage_title $link)
    echo $title
    descr="\"Read and review: $title\""
    id=$(task add +next +rnr "$descr" | sed -n 's/Created task \(.*\)./\1/p')
    task "$id" annotate "$link"
}

alias rnr=read_and_review
```

If you're not into figuring out what bash code does:
Entering the following command into your shell will result in a new task with a `+rnr` tag with description 'GTD with Taskwarrior, part 4: Processing' and the link as an annotation.

``` bash
rnr http://cs-syd.eu/posts/2015-07-05-gtd-with-taskwarrior-part-4-processing.html
```

Oh, and you might need to install `html-xml-utils`
