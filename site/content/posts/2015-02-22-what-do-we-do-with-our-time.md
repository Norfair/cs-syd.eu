---
layout: post
title: What do we do with our time?
tags: time, productivity, work, reflection, stress, success, experiment, plan
---

Everyone gets exactly the same amount of time: 24 hours in a day and 168 hours in a week.
As it is your life, you are responsible for what happens to your time.

Time management is essential to get anything done.
At some point I had to figure out where my time goes.
What I write here is an old story.
This all started about three years ago.

<div></div><!--more-->

### Introspection

The first thing I did was to write down the time I was sure to spend.
I was amazed by how little that was.
If you spend 8 hours asleep, 8 hours at work and 2 hours on necessities (eating, showering, etc...), that's still 54 hours that you haven't accounted for on a weekly basis.
As a student, I don't work fixed hours, but if you're already unsure about where more that a third of the time goes when working fixed hours, I could only imagine where my time went.
Of course, I still spent those hours somehow, so I had to figure out where they went.

There was no way that I was going to write down what I had done for every minute every day, but I did it for one day to get an idea.
My biggest daily time spenders were the computer and the TV.

Because I spent so much time at a computer, I installed [Mind The Time](https://addons.mozilla.org/en-us/firefox/addon/mind-the-time/) to find out where my time at the computer went.
Once again, I was amazed.
Almost none of the time I spent at the computer was spent productively.

Realising that I am horrendously bad at estimating where my time goes, I start keeping a log of what I spend time on while working.
I thought I was a descent worker.
I was wrong again.
If I only counted actual work, and not the break time, I worked about 40% of my work time.
That's a little over 3 hours per day.
Again, I still spent 7 hours on work every day.
Where did all that time go?!


### Decisions

In this blogpost, I haven't written about one of the most important steps in this process.
This step involves thinking of and writing down what you want to spend time on and what you definitely don't mind not spending time on.
Of course that's very personal, which is why I don't think it's very useful to you (the reader) to read from me.
It's not worth improving your time management if you haven't done this step.


### Fixes

After finding out that I was not only bad at estimating my time spending, but also bad at spending the time, I had to fix some things.

- I stopped watching TV altogether and [I haven't regretted that at all.](/posts/2015-03-01-why-i-dont-like-tv.html)
That's at least an hour a day saved.

- On the computer, I installed [Leechblock](https://addons.mozilla.org/en-us/firefox/addon/leechblock/) to block the sites that I didn't want to want to visit.
I deleted my facebook account. That was liberating!
Another hour saved.

- I started using [Emacs and Org-mode](http://doc.norang.ca/org-mode.html) which allowed me to log the time I spent on work.
Not taking enough breaks became a bigger problem around about this time.
During work, I started using [the pomodoro technique](http://pomodorotechnique.com/).
This ensured that the time I spent working was actually spent on work and that I regularly took breaks too.
The fact that I clocked my work on every task made my work very time-focused instead of focused on finishing task.

Now I use [taskwarrior](http://taskwarrior.org/) to handle tasks.
This makes sure that I don't try to clock everything anymore.
Task warrior is more focused on finishing tasks (and has a CLI, yay!).

Changing the way I work has saved me countless hours.
I now spend more than 75 procent procent of my working time (including breaks) on actual work.


### Reflection

Looking back, I can't imagine not having gone through this anymore.
I have become convinced that it is essential to be conscious of one's time to manage it.
Being motivated to do good work is definitely not enough to spend your time wisely.

I still have 24 hours in a day, but I get more done and I feel less stressed.
