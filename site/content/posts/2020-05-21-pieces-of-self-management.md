---
title: Pieces and tools of self-management
tags: self-management, intray, tickler
---

This is the second post in a series about self-management.
It describes the pieces that are necessary in a good self-management workflow, and list tools to implement these pieces.

<div></div><!--more-->


The whole self-management workflow consists of the following steps:

1. Capture
2. Clarify
3. Organise
4. Review
5. Engage

To implement these steps, we will need certain tools.
Which tools you use are up to you, but we recommend and describe the following here:

1. [Intray](https://intray.cs-syd.eu) for capturing
2. [Tickler](https://tickler.cs-syd.eu) as a tickler system
3. [Smos](https://smos.cs-syd.eu) for processing, someday-maybe, review, and doing
4. Any calendar of your choice, I use Google Calendar for now
5. Email as a necessary tool in modern work
6. A phycisal inbox

Chapters will group workflows by tool and by piece.
The chapters are vaguely ordered by what you may want to learn when.


### Capture

Capturing ensures that all the stuff that you have allowed into your life is funneled through your self management system, instead of existing as vague external sources of stress.
Make no mistake about this: If you do not capture a piece of 'stuff', it _will_ be a (possibly uncoscious) source of stress.
This ensures that you can trust that you will give each piece of stuff the appropriate amount of time and attention but not more.

### Clarify & Organise

Clarifing your input consists of thinking about what it means to finish dealing with a piece of stuff that you have allowed into your life.
Remember, no one else is going to do it for you, so it is up to you to do this thinking.
However, you only want to do this thinking once, so after clarifying you will want to organise the results of your thinking appropriately.
This ensures that you never have to do that thinking multiple times, and that you can keep your head free for actual work when you want to work.

### Review

The review ensures that your life is alligned toward your goals instead of vaguely fumbling about, and that your self-management system stays up-to-date and functional to reflect that.

### Engage

This part is the actual work.
You will use your self-management system to make intuitive gut-feeling decisions about what is best for you to work on right now given your location, how much time you have, your brainspace, and the priority of your tasks (in that order).
