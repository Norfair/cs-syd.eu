---
layout: post
title: "GTD with Taskwarrior, part 5: Doing"
tags: Taskwarrior, productivity, work, GTD, tutorial, CLI, terminal, processing
---

This fifth part of the series on how to use Taskwarrior to implement GTD is on the most important part of productivity: Doing your work.

<div></div><!--more-->

Up until now, how have you been deciding what to do?
Even if you knew what to do, how did you know there's nothing better for you to do at that moment?

GTD offers you a concrete method to make informed decision on what to do when you work.

1. Have everything you know you've decided you need to do ready to evaluate.
2. Narrow down the amount of tasks to choose between as much as possible.


### Context

The most important step in choosing what task to work on is to realise that there are tasks you don't even need to consider.
If you're on an airplane and you have some time to work, there's absolutely no way that you're going to be able to water your office plants.
Ideally, tasks that lie out of your current context should not even present themselves in the range of opportunities.

Taskwarrior makes this part very easy.

Remember that in the last part I told you to add a context tag to every task?
Now we're going to use them.

First, Identify the different contexts that you work in.
For example, at work I'm always at a computer connected to the internet with a phone next to me, so the `+@computer`, `+@phone` and `+@online` tags go together:

```
context.work=+@computer or +@phone or +@online
```

At home, I currently have no internet connection:

```
context.home=+@home -@online
```

This might not be the most practical example but you get the idea.

Because you've already tagged all your tasks, setting a context takes care of the rest:

``` bash
task context home # At home
task context work # At work
```

Now Taskwarrior will no longer show any tasks that are not available to work on in your current context.

Note that I'm assuming that you've either synchronized Taskwarrior across your home- and work computer, or you're using a laptop.

### More narrowing down

You can take the 'narrowing down the options' step of GTD down a lot further than just contexts.
Be wary however, this could increase the transaction cost of using your system if you're not careful.

#### Brain power

Here's another example.
You've just done an exam and your brain feels like it's toast.
There's no use in seeing a task like `Write a review of Marc's paper on monoids in the category of endofunctors` (wink).
A task like `Water the office plants` would ideally present itself at the top of your task list.

You can use Taskwarrior's used defined attributes (UDA) to accomplish this.

Start by defining a UDA for brainpower:

```
uda.brainpower.type=string
uda.brainpower.label=Brainpower
uda.brainpower.values=H,M,L
uda.brainpower.default=M
```

You can omit the default value if you'd rather be sure to enter it yourself.
If you end up with a task without a `brainpower` attribute, it's possible that you might run into some sorting problems later.

You can then add the `brainpower` consideration to your next report:

```
report.next.description     = Most urgent tasks
report.next.columns         = id, start.age, entry.age, urgency, depends, priority, project,tags, recur, scheduled.countdown, due.remaining, until.remaining, description, brainpower
report.next.filter          = status:pending and +next
report.next.labels          = ID,Active,Age,Urgency,Deps,P,Project,Tag,Recur,S,Due,Until,Description,Brainpower
report.next.sort            = brainpower-,priority-,urgency-
```

This will sort the tasks on your list in a decreasing order by brainpower first.


#### Estimate

Similar to the way we define the `brainpower` UDA, you can also define a UDA for the time estimated to complete the task:

```
uda.estimate.type=numeric
uda.estimate.label=Estimate
```

You could define a default for the estimate as well but I'd recommend against that.
There's no good default value for a task anyway.
This does mean that you have to specify an estimate for _every task_.

I'll leave it to you to extend your `next` report to include an estimate.


#### Using your UDA's

To create a new task, you now specify a `brainpower` and an `estimate`:

```
task add "Call Jamie and ask him why my office plants were dead after my holiday weekend." es:5 brain:L
```

To evaluate what task you should work on next, assess the state of mind you are in and estimate the amount of time you have right now.
Let's say you're ready to do some heavy-on-brainpower work but have only 10 minutes before your next meeting starts:

```
task next brainpower:H estimate:10
```

The next report should now show you a significantly smaller amount of tasks to choose from.
Even better, you are sure that you only see tasks that you can actually make progress on within the next ten minutes.
