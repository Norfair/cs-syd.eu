---
title: Announcement: Worldwide empathy for medics
tags: empathy, medics
---

Today I am announcing [Worldwide Empathy for Medcs](https://worldwideempathyformedics.org).

<div></div><!--more-->

During the COVID-19 crisis, we are all doing our part.
For most of us that part is easy: stay at home and do not come into contact with anyone if you can avoid it.
For some, it means working day and night while risking their lives.
These medics deserve support, empathy and a listening ear.
For this purpose, we created [Worldwide Empathy for Meds](https://worldwideempathyformeds.org).
This is a service for caregivers to find someone to talk to during crisis.
Feel free to share this with any doctors you may know.
You could be indirectly saving a life.
