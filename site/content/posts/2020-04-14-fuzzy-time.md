---
title: Fuzzy-time
tags: Haskell, fuzzy-time, cursors, smos
---

This article was my first commissioned article, for Human Readable.
Head over there to read it for free during the quarantine:

<div></div><!--more-->

<a class="button is-primary" href=https://humanreadablemag.com/issues/2/articles/dealing-with-friday-the-13th>
  Read more
</a>
