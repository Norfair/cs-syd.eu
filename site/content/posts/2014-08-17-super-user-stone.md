---
layout: post
title: How to synchronise and back up your dotfiles with the Super User Stone
tags: SUS, dotfiles, aliases
---

I use Linux on multiple systems.
I want to share some config files across my systems, and use some config files only for specific systems.
I made the Super User Stone for precisely this purpose.
You can find the source on [github](https://github.com/NorfairKing/super-user-stone) available for use.

In this post, I will show you how to synchronise and back up your dotfiles with the Super User Stone.

<div></div><!--more-->

### Your dotfiles
If you're a linux user, you probably at least have a '.bashrc' and a '.bash_aliases' file in your home directory. 

    /home/user
    |- .bashrc
    |- .bash_profile
    |- ...

I will now show you how you can backup and synchronise these files on multiple systems.


### The sus depot
The first thing you will need is a SUS depot.
This is a directory with all your dotfiles.
It is structured like this:
    
    - depot
    |- shared
    |- hostname1
    |- hostname2
    |- configurations.sus

In the shared folder, you put all config files that you want across all file systems.
For every system, you make a directory named after the hostname of the system.
SUS uses the config file in the host name directory if it exists.
It uses the shared config file otherwise.

A very simple SUS depot could look likes this:

    - depot
    |- shared
    | |- bashrc
    | |- bash_aliases
    |- laptop
    | |- bash_aliases
    |- configurations.sus

In this example, both the bashrc and the bash_aliases file are deployed.
On the 'laptop' system, the bash_aliases is deployed from the laptop folder instead of the shared folder while the 'bashrc' file is deployed from the shared folder.
On any other system, both files would be deployed from the shared folder.

### The .sus file
The Super User Stone need to know where to deploy the config files to.
This is where the 'configurations.sus' file comes in.
It is just a text file and it is structured as follows:

    [<destination directory>]
    source_file_name: destination_file_name

In the example above, the 'configurations.sus' would look like this:

    [~]
    bashrc:         .bashrc
    bash_aliases:   .bash_aliases

In this case, the complete deployment happens like this (on the 'laptop' system).

    /path/to/sus/depot/shared/bashrc        -> /home/user/.bashrc
    /path/to/sus/depot/laptop/bash_aliases  -> /home/user/.bash_aliases

### How to synchronise and backup your depot
Now that you've set up your SUS depot, you can synchronise it.
There are a few options for synchronisation.

- _[Git(hub)](http://github.com)_:
    Put your depot in a (private) repository.
    This has the advantage that you keep all history.
    The disadvantage is that you have to commit, push and pull the repository whenever you change a config file.

- _[Dropbox](http://dropbox.com)_:     
    Put your depot in a folder in your dropbox.
    A dropbox comes with the advantage of instantaneous synchronization but you might have to deal with synchronisation conflicts.

- _[Bittorrent Sync](http://getsync.com)_:
    This is a newer synchronisation program.
    Bittorrent sync works in a distributed manner.
    If all your systems are on your own network, you're not even using your provider's network, which saves bandwidth.

- _Unsynchronized backup_:    
    You can, of course, also just keep your depot on an external hard drive or a USB stick.
    This way, changes don't get synchronised, but you can still share your config files.

### Deploying SUS
You've set up your depot, and configured it.
It's now time to deploy for the first time.

By default, the deployed files are symlinked to the appropriate files, but you can also copy them.
To deploy SUS, run the deploy script.

    /path/to/super-user-stone/deploy.sh -d /path/to/sus/depot
   
If you did everything correctly, you should now see something like this:

    Super User Stone
    
    Configurations:
      SUS configurations config file existence
      Parse Succes
    
        /path/to/sus/depot/shared/bashrc        -> /home/user/.bashrc
        /path/to/sus/depot/laptop/bash_aliases  -> /home/user/.bash_aliases


### What's in the name?
The name 'Super User Stone' is an allusion to the philosopher's stone from the famous Harry Potter series.
The philosopher's stone is said to transform any metal it touches into solid gold.
By analogy, the Super User Stone is supposed to transform any system into _your_ system.
