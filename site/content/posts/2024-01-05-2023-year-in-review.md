---
title: 2023; year in review
tags: review, year
---

Another year, another "year in review" blogpost.

<div></div><!--more-->

### Highlights

- I moved in with my lovely girlfriend.
- [I collected 134 new failures, a new record!](/cv/fail.pdf)
- [I worked as a research and development engineer.](/cv)
- I built an off-site backup system
- We taught salsa dancing in two countries, danced in three more, and gave a workshop at Google.
- I wrote 12 blogposts.


### Work & time off

I had some time off for the first part of the year and got to use my "emergency fund".
I put that in quotes because I no longer call it my emergency fund.
Psychologically, I found it unfortunately named because I could never tell if my current situation was enough of an emergency to warrant touching the emergency fund.
These days I call it my "time-off"-fund, so it's clear that it's fine to use when I have some time off.

I spent quite a lot of time strengthening the habits that I want to have: Learning, exercise, building relationships.
And of course I also spent a lot of time writing software for fun (not so much profit, sadly).

### New home and moving in

Just after my time off, I moved into a new place with my lovely girlfriend.
It's been wonderful because she is amazing.

![A picture of me holding a paint brush](/assets/2023-year-in-review/moving-in.jpg)

### Software & Writing

#### Smos

[Smos saw three releases in 2023](https://docs.smos.online/changelog).
Notable upgrades include:

* [`smos-jobhunt`](https://docs.smos.online/smos-jobhunt), for automating some parts of jobhunting
![Smos Jobhunt Tool](/assets/2023-year-in-review/smos-jobhunt.png)

* [the new booking interface](https://smos.online/book/syd), for letting people book meetings with you

![Smos Booking Interface](/assets/2023-year-in-review/smos-booking.png)

* Massive improvements to the fault-tollerance of `smos-calendar-import` thanks to an improved ICal library

#### ICal

I've talked about [my `ical` library](https://github.com/NorfairKing/ical) in [last year's "year in review" blog post](/posts/2023-01-03-2022-year-in-review) already.
Back then it was already an improvement but still nowhere near complete.

[This year I managed to complete the library and write about it!](/posts/2023-04-26-ical)
I really got to find out how well a concept could be specified, only for that to be completely ruined by the real implementations that we find in the wild.

![Example ICal Event](/assets/2023-year-in-review/ical-event.png)

However, this now-complete implementation helped me solidify both Smos's ICal integration, as well as the ICal integrations on [Social Dance Today](https://social-dance.today)

#### Social Dance Today

What started as a little "let's see what happens" project has grown into my most popular project so far!

The site has increased in popularity in the past year, thanks to some new data sources and features.

![Graph of the users on Social Dance Today](/assets/2023-year-in-review/social-dance-today-users.png)

The site lists parties all over the world.
It's already the most knowledgable social dance event site in Switzerland, The Netherlands, Belgium, some other parts of Europe, and some cities in the US.

![World map of parties all over the world](/assets/2023-year-in-review/social-dance-today-worldmap.png)

Sticking to my hard requirements of "no evil features!" has really had me think through how to design new features.
Afterall, I'm making this site because I'm fed up with the way other (profit-driven) sites compromise on their users' experience.

The site is made to be a tool instead of an engagement-sink.
It's specifically designed for users to get the information they're looking for and leave in the shortest time comfortable.
For example, one new feature I made this year is the bookmark feature:

![Social Dance Today Bookmarks Overview](/assets/2023-year-in-review/social-dance-today-bookmarks.png)

This feature lets users put a calendar subscription link into their calendar client of choice, and have any bookmarked party show up in their calendar automatically.

![Social Dance Today Calendar](/assets/2023-year-in-review/social-dance-today-calendar.png)

Users don't even have to come back to the site to recall information about the party.
This way the site helps to save time instead of being a place to spend time.

#### Nix CI

One night I way laying awake as a I do on days that I don't exercise and got the idea for this project.
I was up until 04 in the morning writing down my ideas for it.

A few days later I had a prototype and some more afternoons and weekends later I had an eventful, if rather unsuccessful, launch!

![Nix CI Page](/assets/2023-year-in-review/nix-ci.png)

The idea, I maintain, is great: Zero-config, locally reproducible, CI for Nix.
No more endless updating of GitHub Actions or GitLab Pipelines.
CI that "just works"

![Nix CI Page Benefits](/assets/2023-year-in-review/nix-ci-zero-config.png)

The launch was rather eventful because some people found some security issues in early versions of the tool and immediately tried to exploit them without even warning me or anything.
I know they were being nice rather than malicious because their exploit involved showing me [nyan cat videos](https://www.youtube.com/watch?v=2yJgwwDcgV8)
I felt a bit violated but they made me rethink the whole thing and it's much more secure now.
In hindsight that was probably the best case scenario for a launch, but at the time I certainly didn't think so.

These days all those issues are fixed of course, but the tool is still invite-only for now just in case.
If you'd like to try it out, please [book me](https://smos.online/book/syd)!


#### Really safe money

[A few years ago I wrote about how to deal with money in software.](/posts/2020-07-28-how-to-deal-with-money-in-software)
[In 2022 I wrote even more about it](https://cs-syd.eu/posts/2022-08-22-how-to-deal-with-money-in-software), and wrote [a reference implementation](https://github.com/NorfairKing/really-safe-money) of my recommendations as well.
This year I noticed some missing pieces and filled them in.

These days the library boasts almost three million tests and some gorgeous documentation:

![Really Safe Money Docs](/assets/2023-year-in-review/really-safe-money.png)

#### Declarative routeros

Having moved into a new place, I decided I would "do things right this time" when setting up the home network again.
I asked Init7 to upgrade my connection to 25Gbps symmetrical (which is the same price as 1Gbps symmetrical) and got myself a CCR2004-1G-12S+2XS.

I ended up having to mod two noctua fans onto the router because it couldn't handle the heat with its two tiny little fans (on the wrong side of the case) and sounded like a jet engine taking off.

![Router Modification in Progress](/assets/2023-year-in-review/modded-router.jpg)

But the bigger issue for me was that configuring this thing involves clicking around in a web interface.
[My record of disliking clicking goes back to 2014](https://cs-syd.eu/posts/2014-07-29-why-i-dont-like-guis), so I had to do something about that!

I was learning Rust at the time, so I ended up writing [`declarative-routeros`](https://github.com/NorfairKing/declarative-routeros/).
It lets you specify the configuration of the router as a single script file, and then instructs the router to reboot itself without any configuration and set itself up using this script.

Now I can declaratively configure my home network, and [you can too](https://github.com/NorfairKing/declarative-routeros/).


#### Ruffling feathers

I managed to get some more people angry at me by delivering bad "news".

[While learning Rust, I found out that Rust is way behind on Haskell when it comes to testing.](https://github.com/NorfairKing/testing-in-rust)
This was really surprising to me, because people had been evangelising Rust to me as if it were better than Haskell in every way.
Needless to say, people were vocal about me sharing this information.

The next, and even more dramatic, ruffling of feathers happened when I published
[Nix Does Not Guarantee reproducibility](https://github.com/NorfairKing/nix-does-not-guarantee-reproducibility).
The idea that we should not say that Nix guarantees reproducibility, when it doesn't, upset many more people than I expected, honestly.


#### Building an off-site backup system

I pride myself on being able to build robust systems, so I already had a very redundant backup system.
Until the fall of 2023, however, all of the redundant copies were in my home.
So I decided I wanted some off-site backups as well.
I put two more computers with three drives (in total) in Belgium at my parents' place.
Now I have more than ten redundant copies on more than five machines in two countries.

![My Off-site backup system](/assets/2023-year-in-review/backup-system.jpg)

#### Ocean sprint

I got to join the [NixOS Ocean Sprint](https://oceansprint.org/) in Lanzarote with some coworkers.
We managed to hack on some interesting things, but more importantly:
It was wonderful to put some faces to the GitHub usernames I'd seen around for years.

![Oceansprint Web Page](/assets/2023-year-in-review/oceansprint.png)

### Dance

We celebrated our three-year anniversary at [Rhythmia](https://www.rhythmia.ch/) this year.

![Rhythmia Team Page](/assets/2023-year-in-review/rhythmia.png)

We got to teach at a conference for the first time, in Berlin:

![Teaching Salsa in Berlin](/assets/2023-year-in-review/dance-berlin.jpg)

We also got to teach at Google in Zürich: 

![Teaching Salsa at Google](/assets/2023-year-in-review/dance-google.jpg)

I had a nice time traveling to dance again.
I got to dance in half a dozen countries, and even got to spend two weeks dancing in Croatia!

![The view in Croatia](/assets/2023-year-in-review/croatia.jpg)


### Languages

I found myself interested in continuing to learn more languages.

#### Greek

I tried picking up Greek, "because I already know the letter anyway", only to find out that Greek is really complicated and most of the letters are pronounced completely differently from what I expected.

I figured I'd use Duolingo to get started.
Duolingo quickly got me hooked on keeping my streak alive.
I kept going to keep my streak alive long after I lost my initial interest in learning a new language, so I guess their tactics worked.

Along the way, I got to translate some interesting phrases:

<div class="columns is-centered">
  <div class="column has-text-centered">
    <img width=300px src="/assets/2023-year-in-review/duolingo1.png" alt="Duolingo Screenshot">
    <img width=300px src="/assets/2023-year-in-review/duolingo2.png" alt="Duolingo Screenshot">
    <img width=300px src="/assets/2023-year-in-review/duolingo3.png" alt="Duolingo Screenshot">
    <img width=300px src="/assets/2023-year-in-review/duolingo4.png" alt="Duolingo Screenshot">
  </div>
</div>

#### Sign Language

Along with Greek, I also started to learn (American) sign language (ASL) in 2023.
I used "Lingvano", which is like Duolingo but for sign language.

This turned out to be much more interesting, and much more useful than expected.
Sign language has many interesting features that I find are missing in other languages, for example:

* There are different signs for "we" (you and me) and "we" (me and other(s))
* There's a difference between "why" (with which cause) and "why" (for what purpose)

Sign language turns out to be useful in a variety of circumstances:

* At parties with loud music
* On the tram when there are loud and unexpected noises and announcements
* At loud restaurants
* While you have your mouth full of food
* While brushing your teeth
* While the alarm is going off during a bank robbery

They also combo very well with my new earplugs.

![Dance sign](/assets/2023-year-in-review/sign-language.png)

### Custom earplugs

Because of how much time I spend in noisy environments such as salsa parties, I decided to look into hearing protection.
I had some custom earplugs made to wear at the parties.

The process of having them made involves having a silicon cast made of the ear canal.
This is entirely pain free, and it is very pleasant to not be able to hear any noise for a minute or two.
In this moment, I realised immediately what a good idea this had been.

I now wear them at parties, but also on public transport, at restaurants, and anywhere someone might have brought a loud child.
I've found that they calm me down.

![Custom earplugs](/assets/2023-year-in-review/earplugs.jpg)

Initially I was worried that I would lose them easily, but they come with a bag that has a hook that I can use to hang them onto the inside of my jacket.


### Fries

There are no Belgian fries places (frituur) in Switzerland.
If you're Belgian you're probably very surprised by this.
If you're not Belgian you might not even know what a [frituur](https://en.wikipedia.org/wiki/Friterie) is.

Anyway, this has bothered me for years.
It's gotten to the point that I'm seriously looking into whether it is commercially viable to open a frituur in Zürich myself!

The first step was to get the ingredients together.
I needed the right potatoes, the right fat, and the right spices, as well as a fryer.
Strangely enough, getting the machine was the easiest of the four.
Getting the right potatoes and fat proved more difficult.
The potatoes need to be a specific strain that's not grown much, and the fat needs to be white ox tallow.
(This also means that the fries aren't vegetarian.)

Initial results were promising: I was able to make proper Belgian fries at home.
The next step will be to source the ingredients from somewhere closer so that I don't have to bring them from Belgium myself.

![Custom earplugs](/assets/2023-year-in-review/fries.jpg)

### Conclusion

2023 was a big upgrade for my life quality.
I still really enjoy the "try whatever seems interesting at the time" strategy.

Greetings from Zürich!

![Snow in Zürich](/assets/2023-year-in-review/snow.jpg)
