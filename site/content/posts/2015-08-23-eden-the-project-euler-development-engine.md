---
layout: post
title: "EDEN: the project Euler Development ENgine"
tags: euler, EDEN, engine, development, languages
---

I've done it again.
I've massively over-engineered a solution to a simple personal annoyance.
This time it's a build system.

<div></div><!--more-->

I like solving project euler problems but I don't like having to figure out how to compile my solutions.
When solving project euler problems I want to focus on actually solving the problems.
I don't want to be bothered by the fact that I have to think about how to run the program.

Moreover, when doing a long-term project I want to ensure that the project stays consistent throughout the entire process.
Lastly I really want to have some documentation on what's happening in the solutions.

Enter EDEN.
EDEN takes care of all the hassle of solving project euler problems.
It can build, run, test and publish your solutions, libraries and writeups.
Even better, all dependencies are automatically reduced to a minimal tree and resolved in parallel.

Of course I'm already using this for [my project euler solutions](https://github.com/NorfairKing/project-euler).

I'm not going to give a lengthy demo here.
There is a [getting started guide](https://github.com/NorfairKing/eden/blob/master/doc/getting-started.md) on github.
I'll just show you my aliases for `eden` and it will become clear immediately how powerful EDEN is.

```
alias eg='eden generate'
alias egs='eden generate solution'

alias eb='eden build'
alias eba='eden build all'
alias ebl='eden build library'
alias ebp='eden build problem'
alias ebs='eden build solution'

alias er='eden run'
alias era='eden run all'
alias erp='eden run problem'
alias ers='eden run solution'

alias et='eden test'
alias eta='eden test all'
alias etl='eden test library'
alias etp='eden test problem'
alias ets='eden test solution'

alias ep='eden publish'
alias epa='eden publish all'
alias epp='eden publish problem'
alias eppa='eden publish part'
alias epl='eden publish library'
```

More documentation can be found [on github](https://github.com/NorfairKing/eden).
