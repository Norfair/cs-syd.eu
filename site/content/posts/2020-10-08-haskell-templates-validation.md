---
title: Haskell Templates
tags: Haskell, templates
---

I have had the idea of selling **Haskell templates** for a while now, and I am now getting around to validating this idea.
I have also been trying to learn more about indie-hacking so here goes my first proper attempt.

<div></div><!--more-->

Please have a look at my quick landing page, here:

<a class="button is-primary" href="https://template.cs-syd.eu">
  Landing page
</a>

and fill in my little feedback form here:

<a class="button is-info" href="https://forms.gle/wBMVWyYLmUSyUvMj8">
  Feedback form
</a>

Please share this post so that I can gather more data.
