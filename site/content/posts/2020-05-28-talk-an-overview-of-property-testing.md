---
title: Talk: An overview of property testing
tags: talk, property testing, haskell
---

I gave a 20-minute talk about property testing at F(by) 2020 in Minsk on 2020-01-25.

<div></div> <!--more-->

The lovely people from SPACE have recorded it and uploaded it to youtube:

<iframe
  width="560"
  height="315"
  src="https://www.youtube.com/embed/x-zxvTkapcU"
  frameborder="0"
  allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
  allowfullscreen>
</iframe>

Slides are available [on GitHub](https://github.com/NorfairKing/an-overview-of-property-testing)
Feedback is always welcome via email.

