---
layout: post
title: Math notation inspired by functional programming
tags: notation,Haskell,definitions
---

Programming has been inspired by mathematics ever since the concept was discovered.
In this post I propose some math notation inspired by functional programming.

<div></div><!--more-->

Mathematical notation has the useful and complex property that it is only ever read by humans.
Humans that are generally good at accurately guessing what the author meant without the need to parse or compile the expression.

Because computers generally don't have this ability, programmers have to be extra clear about what exactly they mean.
As a result, higher order functions have become much more common in functional programming than in mathematics.

I propose to bring these common higher order functions to mathematics to ease the human 'parsing' effort that is required when reading mathematical notation.

### Map

First off, the `map :: (a -> b) -> Set a -> Set b` function for sets.
In mathematical notation, we often find set comprehension of the following pattern

$$\{ f(a) \mid a \in A \}$$

In words: the element-wise applications of a function $f:\ A \subseteq dom(f) \rightarrow B$ to a set $A$.

I propose the following notation:

$$\overset{f}{\Box}A \quad\equiv\quad \{ f(a) \mid a \in A \}$$

It should be pronounced as "$f$, mapped over $A$", "map $f$ over $A$", or "$A$, under the element-wise application of $f$".

A direct consequence of this notation is that we can write subsequent element-wise applications of functions as follows:

$$\overset{g}{\Box}\overset{f}{\Box}A \quad\equiv\quad \overset{g \circ f}{\Box} A$$

You can use this yourself by adding the following LaTeX macro to the prelude of your document:

``` latex
\newcommand{map}[2]{\overset{#1}{\Box}{#2}}
```

Possible alternatives: $f \Box A$, $\Box_{f} A$ or $(\Box(f))(A)$.

### Filter

Next is the `filter :: (a -> Bool) -> Set a -> Set a` function for sets.
In mathematical notation, the relevant pattern looks as follows:

$$\{ a \in A \mid P(a) \}$$

In words: all the elements of $A$ for which a predicate $P$ on $A$ holds.

I propose the following notation:

$$\overset{P}{\triangleleft}A \quad\equiv\quad \{ a \in A \mid P(a) \}$$

It should be pronounced of "$A$, filtered through $P$", "filter $A$ through $P$" or "the elements of $A$ with property $P$".

The set of elements for which the conjunction of predicates $P$ and $Q$ holds could then be written as follows:

$$\overset{P}{\triangleleft}\overset{Q}{\triangleleft}A \quad\equiv\quad \overset{P \wedge Q}{\triangleleft} A$$

You can use it yourself with the following macro in the prelude of your LaTeX document:

``` latex
\newcommand{filter}[2]{\overset{#1}{\triangleleft}{#2}}
```

Possible alternatives: $P \triangleleft A$, $\triangleleft_{P} A$ or $(\triangleleft(P))(A)$.
