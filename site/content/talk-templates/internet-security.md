---
title: Internet Security
category: Popular
audience: Non-technical users of technology
description: Staying safe online is difficult and complex. This talk walks you through the most important and actional aspects.
---

Staying safe online is difficult and complex.
This talk walks you through the most important and actional aspects.

### Past occurrences

- Workshop: Internet Security @ ETH 2020-12-03
- Workshop: Internet Security [Slides](https://docs.google.com/presentation/d/1RnZUWaNxXIKNKiTDnCsqVJsYXRAO_doMBGtxStQ2tVY/edit?usp=sharing) @ ETH 2019-09-19

