---
title: Writing a text editor in Haskell with Brick
category: Technical
type: Workshop
audience: Haskell Programmers with an interest in editors
description: Have you ever wanted to write a text editor in an hour, then this workshop is for you!  In sixty minutes, we build a small text editor in Haskell together.
---

Have you ever wanted to write a text editor in an hour, then this workshop is
for you!  In sixty minutes, we build a small text editor in Haskell together
using [brick](https://hackage.haskell.org/package/brick).  We will build up the
entire program from scratch, explaining the necessary concepts such as
[cursor](https://hackage.haskell.org/package/cursor) along the way.
