---
title: Validity and Validity-based testing
category: Technical
audience: Haskell Programmers and/or Programmers that are interested in cheaper safety
description: Writing correct software is difficult. Property testing has the potential to make safe software easier and cheaper. However, current methods still involve the costly requirement for programmers to write generators, shrinking functions and properties. Validity-based testing promises free generators, free shrinking and cheap properties, to allow property-based testing to fulfill its true potential.
---

Writing correct software is difficult. Property testing has the potential to
make safe software easier and cheaper. However, current methods still involve
the costly requirement for programmers to write generators, shrinking functions
and properties. Validity-based testing promises free generators, free
shrinking and cheap properties, to allow property-based testing to fulfill its
true potential.

### Examples


- Validity and Validity-based testing @ Google Zürich - 2017-12-08 [Video](https://www.youtube.com/watch?v=cPE577X4kIY) [Source](https://github.com/NorfairKing/validity)

  <iframe width="854" height="480" src="https://www.youtube.com/embed/cPE577X4kIY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

- Validity and Validity-based testing @ HaskellerZ - 2017-11-30 [Presentation](https://github.com/NorfairKing/validity/blob/development/presentation.md) [Source](https://github.com/NorfairKing/validity)

- Validity and Validity-based testing @ DFinity - 2018-09-04 [Presentation](https://github.com/NorfairKing/validity/blob/development/presentation.md)

- Validity and Validity-based testing @ Monadic Warsaw - 2018-09-25 [Presentation](https://github.com/NorfairKing/validity/blob/development/presentation-backward.md)

  <iframe width="854" height="480" src="https://www.youtube.com/embed/eIs9qNh17SM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
