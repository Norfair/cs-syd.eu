---
title: Property Testing
category: Technical
audience: Professional Programmers
description: This talk introduces property testing to the general programmer.
---

This talk introduces property testing to the general programmer.
It does not assume any background in testing.

What are we trying to do here?
We are trying to solve a business problem.
It turns out that it may be a good idea to use code.
Great, hire the developers, set up a manager and start cranking out code, right?

What happens if you put out code that does not work?
What happens if the problem that you set out to solve is not as solved as you promised?

Motivation: 2 problems:
- Coverage: When do you stop testing? How many tests should you write?
- Developer Cost: Does anyone like it? How many tests do you write?
Who _enjoys_ making sure that their code works?
What percentage is your test coverage?



SUPER COOL DEMO
