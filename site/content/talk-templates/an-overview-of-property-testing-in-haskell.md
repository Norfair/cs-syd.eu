---
title: An overview of property testing in Haskell
category: Technical
audience: Haskell Programmers and/or Programmers that are interested in cheaper safety
description: Property testing has been around for two decades, and the design space is rapidly being explored, but keeping up with these developments has not been easy. This talk gives a thorough overview of the different libraries and explores their tradeoffs. Libraries discussed in this talk include QuickCheck, Hedgehog, Validity, Quickspec, Easyspec, speculate, leanspec and smallcheck.
---

## Abstract

Property testing has been around for two decades, and the design space is
rapidly being explored, but keeping up with these developments has not been
easy. This talk gives a thorough overview of the different libraries and
explores their tradeoffs. Libraries discussed in this talk include QuickCheck,
Hedgehog, Validity, Quickspec, Easyspec, speculate, leanspec and smallcheck.

## Bio

Tom Sydney Kerckhove is a Tech lead at FP Complete.
He has used Haskell for years and is an expert on property testing.

