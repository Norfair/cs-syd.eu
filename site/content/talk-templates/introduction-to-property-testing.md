---
title: Introduction to Property Testing
category: Technical
audience: Professional Programmers
description: This talk introduces property testing to professional programmers.
---

This talk introduces property testing to professional programmers.

### Examples

- Introduction to Property Testing @ Relex, Helsinki 2018-10-25
