---
title: Declarative Infrastructure
category: Technical
audience: DevOps Engineers
description: In this talk we apply the concepts and best-practices from programming language theory and function programming to DevOps to uncover new DevOps best practices.
---

The field of programming has had decades to figure out how to build
safe, reliable, fast and cheap software. Now that we, as programmers,
have discovered that we will need many machines to do the job, instead
of just one big machine, we are faced with a new problem.
How do we set up all this infrastructure?
In this talk we apply the concepts and best-practices from programming
language theory and function programming to DevOps to uncover
new DevOps best practices.

### Examples

- Declarative Infrastructure @ Relex, Helsinki 2018-10-26
