---
title: Signature Inference for Functional Property Discovery
category: Research
audience: Haskell Programmers and/or Researchers in Safety or Testing
description: Property discovery is the process of discovering properties of code viaautomated testing. It has the potential to be a great tool for a practicalapproach to software correctness. Unfortunately current methods remaininfeasible because of the immense search space. We contribute a newapproach to taming the complexity of a state-of-the-art generate and testalgorithm. Our approach runs this algorithm several times with carefullychosen inputs of smaller size: signature inference. We implement thisapproach in a new tool called EasySpec. The results suggest that thisapproach is several orders of magnitude faster, fast enough to be practical,and produces similar results.
---

Property discovery is the process of discovering properties of code via
automated testing. It has the potential to be a great tool for a practical
approach to software correctness. Unfortunately current methods remain
infeasible because of the immense search space. We contribute a new
approach to taming the complexity of a state-of-the-art generate and test
algorithm. Our approach runs this algorithm several times with carefully
chosen inputs of smaller size: signature inference. We implement this
approach in a new tool called EasySpec. The results suggest that this
approach is several orders of magnitude faster, fast enough to be practical,
and produces similar results.

### Past occurrences

- Signature Inference for Functional Property Discovery @ Yow!LambdaJam 2018-05-22 [Profile](http://lambdajam.yowconference.com.au/profile/?id=tom-sydney-kerckhove) [Slides](http://yowconference.com.au/slides/yowlambdajam2018/Kerckhove-SignatureInferenceForFunctionalPropertyDiscovery.pdf) [Source](https://github.com/NorfairKing/thesis)
  <iframe width="854" height="480" src="https://www.youtube.com/embed/SM8d1HFy-UQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

- Signature Inference for Functional Property Discovery @ LambdaDays 2018-02-22 [Video](https://www.youtube.com/watch?v=cABf2wN9UE0) [Source](https://github.com/NorfairKing/thesis) 
  <iframe width="854" height="480" src="https://www.youtube.com/embed/cABf2wN9UE0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

- Signature Inference for Functional Property Discovery @ Haskell Exchange 2017-10-12 [Video](https://skillsmatter.com/skillscasts/10629-signature-inference-for-functional-property-discovery) [Source](https://github.com/NorfairKing/thesis)

- Signature Inference for Functional Property Discovery @ HaskellerZ 2017-07-27 [Video](https://www.youtube.com/watch?v=CIyJ4q205iM) [Source](https://github.com/NorfairKing/thesis)

  <iframe width="854" height="480" src="https://www.youtube.com/embed/CIyJ4q205iM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

