---
title: Practical Property testing in Haskell
category: Technical
audience: Beginner Haskell Programmers
description: This talk discusses how to implement proper property testing in Haskell.
---


This talk discusses how to implement proper property testing in Haskell.
It moves way beyond the classic toy examples and examined real-world scenarios.
It takes a deep-dive into how to make property tests interact with web services or database systems. 
It also explaines how to use custom generators and testing combinators.
Other topics discussed are as follows:

- Testing with Databases
- Testing with Services
- Writing Custom Generators
- Using Testing Combinators


### Examples

- Practical Property Testing in Haskell @ FP Complete 2018-05-09 [Blogpost](https://www.fpcomplete.com/blog/practical-property-testing-in-haskell) [Video](https://www.youtube.com/watch?v=5Jfa5-D7vNw)

  <iframe width="854" height="480" src="https://www.youtube.com/embed/5Jfa5-D7vNw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

