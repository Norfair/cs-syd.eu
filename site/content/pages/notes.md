---
layout: page
title: Notes
tags: notes, math
description: An overview of my notes with links to their sources
---

I have a weird way of studying mathematical subjects.
I make notes, lots of notes.
Those notes are available for anyone to use, correct or help out with.

#### English

- [The Notes](https://github.com/NorfairKing/the-notes):<br>
  All my english notes in one repository.
  They're written in Haskell using HaTeX to allow for subset-publishing.
  Any subset of the notes can be published seperately as long as it's internally coherent.
- [ETH Algolab](https://github.com/NorfairKing/eth-algolab-2015)
- [CN Cards](https://github.com/NorfairKing/cn-cards):<br> A card game about computer networks


#### Dutch

I used to write my notes in Dutch.
These are notes for specific courses at KU Leuven in Belgium.
For more general notes, refer to `the-notes` above.

- [Analyse I](https://github.com/NorfairKing/analyse-notities)
- [Algebra I](https://github.com/NorfairKing/algebra-notities)
- [Automaten en Berekenbaarheid](https://github.com/NorfairKing/ab-notities)
- [Gegevensbanken](https://github.com/NorfairKing/Gegevensbanken-Tutorials)
- [Kansrekenen](https://github.com/NorfairKing/kansrekenen-notities)
- [Lineaire Algebra](https://github.com/NorfairKing/lineairealgebra)
- [Meetkunde I](https://github.com/NorfairKing/meetkunde-notities)
- [Numerieke Wiskunde](https://github.com/NorfairKing/all-you-can-carry)
- [Toepassingen van Meetkunde](https://github.com/NorfairKing/TMI-Notities)
- [Wiskunde II](https://github.com/NorfairKing/Wiskunde-II-Oplossingen-van-Oefeningen)

