---
title: Thesis
description: An overview of links to my thesis documents
---

- [EasySpec Github Repository](https://github.com/NorfairKing/easyspec)
- [Thesis Github Repository](https://github.com/NorfairKing/thesis)
- [Thesis PDF](https://github.com/NorfairKing/thesis/blob/master/pdfs/thesis-tom-sydney-kerckhove.pdf)
- [Public Talk Slides](/assets/easyspec/public-presentation.pdf)
- [Public Talk video](https://www.youtube.com/watch?v=CIyJ4q205iM)
- [Academic Talk Slides](/assets/easyspec/academic-presentation.pdf)
- [Public Talk Video (HaskellerZ 2017)](https://www.youtube.com/watch?v=CIyJ4q205iM)
- [Public Talk Video (Haskell Exchange 2017)](https://skillsmatter.com/skillscasts/10629-signature-inference-for-functional-property-discovery)
- [Public Talk Video (Lambda Days 2018)](https://www.youtube.com/watch?v=cABf2wN9UE0)
- [Public Talk Video (YOW! Lambda Jam 2018)](https://www.youtube.com/watch?v=SM8d1HFy-UQ)
