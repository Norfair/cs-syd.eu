---
layout: page
title: Talks
tags: notes, math
description: An overview of the talks I have given
---

<div></div><!--more-->

This is a list of the most recent talks that I have given. Should you be interested in having me as a speaker, please see [my speaking page](https://cs-syd.eu/speaking).

- NixOS tests @ Zurich Nix Meetup - 2022-10-27 [Slides](https://github.com/NorfairKing/nixos-tests-talk)
- Bits and Bobs I've learnt about Leadership @ Festival of Forms - 2022-09-30
- Testing @ Platonic Systems - 2021-11-04
- Internet Security @ ETH Zürich - 2020-12-03
- Practical Property Testing @ FP in the city - 2020-09-10 [Slides](https://github.com/NorfairKing/practical-property-testing-in-haskell) [Video](https://www.youtube.com/watch?v=IsdfoqL1k_U&)
- An overview of property testing in Haskell @ F(by) - 2020-01-24 [Slides](https://github.com/NorfairKing/an-overview-of-property-testing) [Video](https://www.youtube.com/watch?v=x-zxvTkapcU)
- Writing a text editor in Haskell with Brick @ F(by) - 2020-01-23 [Slides](https://github.com/NorfairKing/writing-a-text-editor-in-haskell-with-brick) [Video](https://www.youtube.com/watch?v=Kmf3lnln1BI)
- [Cooperative agreement](https://github.com/NorfairKing/mergeful) @ Simanalytics & Relex - 2019-12-04
- [Cooperative agreement](https://github.com/NorfairKing/mergeful) @ HaskellerZ - 2019-11-28 [Video](https://www.youtube.com/watch?v=MkbhHmAk47k)
- Haskell in practice @ HSR - 2019-11-26 [Slides](https://docs.google.com/presentation/d/1xMeGCZRXBEfZzDyNV1vNKLF1cXMclImW_q2SnvvXJ48/edit?usp=sharing)
- Workshop: Internet Security @ ETH - 2019-07-18 [Slides](https://docs.google.com/presentation/d/1RnZUWaNxXIKNKiTDnCsqVJsYXRAO_doMBGtxStQ2tVY/edit?usp=sharing)
- Testing for beginners @ Monadic party - 2019-06-20 [Video 1](https://www.youtube.com/watch?v=QhaxlpjSd-M) [Video2](https://www.youtube.com/watch?v=6VvZH4IRMvY) [Video3](https://www.youtube.com/watch?v=abqwutZt9H8) [Video5](https://www.youtube.com/watch?v=IYJ9QgweNyA)
- [Validity and Validity-based testing](https://github.com/NorfairKing/validity) @ Helsinki Haskell meetup - 2019-02-19 [Slides](https://github.com/NorfairKing/validity/blob/master/docs/presentation-backward.md)
- Testing in Haskell @ Hochschule für Technik Rapperswil - 2018-12-12
- Safe programming in Industry @ Hochschule für Technik Rapperswil - 2018-12-12
- Building Terminal User Interfaces with Haskell @ FP Complete - 2018-11-27 [Video](https://www.youtube.com/watch?v=qbDQdXfcaO8)
- Declarative Infrastructure @ Relex, Helsinki - 2018-10-26
- Introduction to Property Testing @ Relex, Helsinki - 2018-10-25
- [Validity and Validity-based testing](https://github.com/NorfairKing/validity) @ Monadic Warsaw - 2018-09-25 [Presentation](https://github.com/NorfairKing/validity/blob/development/presentation-backward.md) [Video](https://www.youtube.com/watch?v=eIs9qNh17SM)
- [Validity and Validity-based testing](https://github.com/NorfairKing/validity) @ DFinity - 2018-09-04 [Presentation](https://github.com/NorfairKing/validity/blob/development/presentation.md)
- Testing for beginners @ Monadic Party 2018-06-13 - 2018-06-15 [Video1](https://www.youtube.com/watch?v=S3BRNsbk6Ks) [Video2](https://www.youtube.com/watch?v=qre77C9rve4) [Video3](https://www.youtube.com/watch?v=ht25V9Ty2DE)
- [Signature Inference for Functional Property Discovery](https://github.com/NorfairKing/thesis) @ Yow!LambdaJam 2018-05-22 [Profile](http://lambdajam.yowconference.com.au/profile/?id=tom-sydney-kerckhove) [Slides](https://yowconference.com.au/slides/yowlambdajam2018/Kerckhove-SignatureInferenceForFunctionalPropertyDiscovery.pdf)
- [Ethisch Ontwerp van Gebruikerservaringen](https://docs.google.com/presentation/d/1RHRNl9SYe5mi27KrMzoF__8W2qYNm3oAJUpTtu-PZKs/edit?usp=sharing) @ Marnix Ring Kempenland 2018-05-17
- Practical Property Testing in Haskell @ FP Complete 2018-05-09 [Video](https://www.youtube.com/watch?v=5Jfa5-D7vNw)
- [Signature Inference for Functional Property Discovery](https://github.com/NorfairKing/thesis) @ LambdaDays 2018-02-22 [Video](https://www.youtube.com/watch?v=cABf2wN9UE0)
- [Safe Software in industry @ KU Leuven 2018-02-19](https://dtai.cs.kuleuven.be/seminars/practical-safe-software-haskell-industry-tom-sydney-kerckhove)
- [Validity and Validity-based testing](https://github.com/NorfairKing/validity) @ Google Zürich 2017-12-08 [Presentation](https://github.com/NorfairKing/validity/blob/development/presentation.md) [Video](https://www.youtube.com/watch?v=cPE577X4kIY)
- [Validity and Validity-based testing](https://github.com/NorfairKing/validity) @ HaskellerZ 2017-11-30 [Presentation](https://github.com/NorfairKing/validity/blob/development/presentation.md)
- [Signature Inference for Functional Property Discovery](https://github.com/NorfairKing/thesis) @ Haskell Exchange 2017-10-12 [PDF](/assets/easyspec/public-presentation.pdf) [Video](https://skillsmatter.com/skillscasts/10629-signature-inference-for-functional-property-discovery)
- [Signature Inference for Functional Property Discovery](https://github.com/NorfairKing/thesis) @ HaskellerZ 2017-07-27 [PDF](/assets/easyspec/public-presentation.pdf) [Video](https://www.youtube.com/watch?v=CIyJ4q205iM)
- [Super User Spark: A safe way to never worry about your beautifully configured system again](https://syd_kerckhove@bitbucket.org/syd_kerckhove/sus-talk.git) @ CCC-ZH 2016-04-20
- [An introduction to Haskell](https://bitbucket.org/syd_kerckhove/ccczh-intro-to-haskell) @ CCC-ZH - 2016-01-16
- [Artificiële Intelligentie, een Introductie (dutch)](https://bitbucket.org/syd_kerckhove/marnix-ai/overview) @ Marnix Ring - 2015-04-22
