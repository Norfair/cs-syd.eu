{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Site.Application where

import Site.Foundation
import Site.Handler

mkYesodDispatch "Site" resourcesSite
