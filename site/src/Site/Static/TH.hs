{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveLift #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Site.Static.TH where

import CMark
import Data.Data
import Data.Semigroup
import qualified Data.Text as T
import qualified Data.Text.Lazy as LT
import Data.Time
import Import
import Instances.TH.Lift ()
import Language.Haskell.TH.Syntax hiding (Quote)
import Skylighting
import qualified System.FilePath as FP
import Text.Blaze.Html (Html, preEscapedToHtml, (!))
import qualified Text.Blaze.Html as B
import Text.Blaze.Html.Renderer.Text (renderHtml)
import qualified Text.Blaze.Html5 as Html
import qualified Text.Blaze.Html5.Attributes as HtmlA

deriving instance Lift Day

data MdFile a = MdFile
  { mdTitle :: !Text,
    mdLastUpdated :: !(Maybe Day),
    mdAttributes :: [(Text, Text)],
    -- Raw contents
    mdContents :: !Text,
    -- Scrubbed
    mdBody :: !Text,
    mdWordCount :: !Int,
    mdEstimatedReadingTime :: !Int,
    mdRendered :: !Text,
    mdExtra :: !a
  }
  deriving (Typeable, Lift, Functor)

mkValFunc :: (String -> Text -> [(Text, Text)] -> a) -> (Text -> Text -> MdFile a)
mkValFunc func urlText rawContents =
  let urlString = T.unpack urlText
      (attributes, contents) = splitContents rawContents
      att k = lookup k attributes
      maybeAtt k =
        case lookup k attributes of
          Nothing ->
            error $
              unlines
                [ "The post with url",
                  urlString,
                  "Does not have an attribute with key",
                  T.unpack k
                ]
          Just b -> b
      title = renderTitle $ maybeAtt "title"
      mupdated =
        flip fmap (att "last-updated") $ \t ->
          case parseTimeM True defaultTimeLocale "%F" (T.unpack t) of
            Nothing -> error "Could not parse last-updated"
            Just lu -> lu
      rendered = renderMarkdown contents
      a = func urlString contents attributes
   in MdFile
        { mdTitle = title,
          mdLastUpdated = mupdated,
          mdAttributes = attributes,
          mdContents = contents,
          mdBody = renderBody contents,
          mdWordCount = length (T.words contents),
          mdEstimatedReadingTime = estimateReadingTime contents,
          mdRendered = rendered,
          mdExtra = a
        }

data PostExtra = PostExtra
  { postTags :: ![Text],
    postDate :: !Day,
    postDescription :: Text,
    postExerpt :: !Text
  }
  deriving (Typeable, Lift)

type Post = MdFile PostExtra

sortPosts :: [(a, Post)] -> [(a, Post)]
sortPosts = sortOn (Down . postDate . mdExtra . snd)

{-# INLINE keyFunc #-}
keyFunc :: Path Rel File -> Text
keyFunc = T.pack . FP.dropExtension . toFilePath . filename

{-# INLINE postsValFunc #-}
postsValFunc :: Text -> Text -> Post
postsValFunc = mkValFunc $
  \urlString contents attributes ->
    let maybeAtt k =
          case lookup k attributes of
            Nothing ->
              error $
                unlines
                  [ "The post with url",
                    urlString,
                    "Does not have an attribute with key",
                    T.unpack k
                  ]
            Just a -> a
        tags = parseTags $ maybeAtt "tags"
     in case parseDayFromUrl urlString of
          Nothing -> error "unable to parse url day"
          Just day ->
            PostExtra
              { postTags = map T.toCaseFold tags,
                postDate = day,
                postDescription = renderDescription contents,
                postExerpt = renderMarkdown $ exerpt contents
              }

exerpt :: Text -> Text
exerpt t =
  case T.splitOn "<!--more-->" t of
    (e : _) ->
      let s = T.strip e
       in T.strip $ fromMaybe s $ T.stripSuffix "<div></div>" s
    _ -> t

data QuoteExtra = QuoteExtra
  { quoteDescription :: Text,
    quoteQuote :: !Text,
    quoteAuthor :: !Text,
    quoteTags :: ![Text],
    quoteDate :: !Day,
    quoteExerpt :: !Text
  }
  deriving (Typeable, Lift)

type Quote = MdFile QuoteExtra

sortQuotes :: [(a, Quote)] -> [(a, Quote)]
sortQuotes = sortOn (Down . quoteDate . mdExtra . snd)

quotesValFunc :: Text -> Text -> Quote
quotesValFunc = mkValFunc $
  \urlString contents attributes ->
    let maybeAtt k =
          case lookup k attributes of
            Nothing ->
              error $
                unlines
                  [ "The quote with url",
                    urlString,
                    "Does not have an attribute with key",
                    T.unpack k
                  ]
            Just a -> a
        quote = maybeAtt "quote"
        author = maybeAtt "author"
        tags = parseTags $ maybeAtt "tags"
     in case parseDayFromUrl urlString of
          Nothing -> error "unable to parse url day"
          Just day ->
            QuoteExtra
              { quoteDescription = renderDescription contents,
                quoteQuote = quote,
                quoteAuthor = author,
                quoteTags = tags,
                quoteDate = day,
                quoteExerpt = renderMarkdown contents
              }

data DraftExtra = DraftExtra
  { draftTags :: [Text],
    draftDescription :: !Text
  }
  deriving (Typeable, Lift)

type Draft = MdFile DraftExtra

sortDrafts :: [(a, Draft)] -> [(a, Draft)]
sortDrafts = sortOn (mdTitle . snd)

draftsValFunc :: Text -> Text -> Draft
draftsValFunc = mkValFunc $ \_ contents attributes ->
  let maybeAtt k = lookup k attributes
      draftTags = parseTags $ fromMaybe "" $ maybeAtt "tags"
      draftDescription = renderDescription contents
   in DraftExtra {..}

parseTags :: Text -> [Text]
parseTags = filter (not . T.null) . map T.strip . T.splitOn ","

-- 5 characters per word
-- 200 words per minute on average
estimateReadingTime :: Text -> Int
estimateReadingTime = (`quot` (5 * 200)) . T.length

renderTitle :: Text -> Text
renderTitle = scrubMarkdown

renderDescription :: Text -> Text
renderDescription = scrubMarkdown . exerpt

renderBody :: Text -> Text
renderBody = scrubMarkdown

scrubMarkdown :: Text -> Text
scrubMarkdown rawText =
  let opts = [optUnsafe]
      n = commonmarkToNode opts rawText :: Node
   in scrubNode n

scrubNode :: Node -> Text
scrubNode = go
  where
    go :: Node -> Text
    go (Node _ typ nodes) =
      ( case typ of
          TEXT t -> t
          CODE t -> t
          LINK _ t -> t
          IMAGE _ t -> t
          SOFTBREAK -> " "
          _ -> ""
      )
        <> foldMap go nodes

renderMarkdown :: Text -> Text
renderMarkdown contents =
  let opts = [optUnsafe]
      n = commonmarkToNode opts contents :: Node
   in renderNode n

smallestHeadingLevel :: Node -> Int
smallestHeadingLevel = getMin . go
  where
    go :: Node -> Min Int
    go (Node _ nt subs) =
      ( case nt of
          HEADING level -> Min level
          _ -> mempty
      )
        <> foldMap go subs

renderNode :: Node -> Text
renderNode topLevel = LT.toStrict . renderHtml $ go topLevel
  where
    minHeadingLevel = smallestHeadingLevel topLevel
    go :: Node -> Html
    go n@(Node _ nt subs) = case nt of
      DOCUMENT -> foldMap go subs
      PARAGRAPH -> Html.p (foldMap go subs)
      TEXT t -> B.text t <> foldMap go subs
      HEADING level ->
        let wrappingTag =
              -- We normalise the html heading levels here
              -- We need h1 for the heading, so we want to start at h), which
              -- is where the + 2 comes from.
              let pretendLevel = level - minHeadingLevel + 2
               in case pretendLevel of
                    1 -> Html.h1
                    2 -> Html.h2
                    3 -> Html.h3
                    4 -> Html.h4
                    5 -> Html.h5
                    _ -> Html.b
         in wrappingTag $ foldMap go subs
      HTML_INLINE code -> preEscapedToHtml code <> foldMap go subs
      HTML_BLOCK code -> preEscapedToHtml code <> foldMap go subs
      SOFTBREAK -> " "
      LINEBREAK -> Html.br
      CODE code -> Html.code $ B.text code
      BLOCK_QUOTE -> Html.blockquote $ foldMap go subs
      STRONG -> Html.b $ foldMap go subs
      EMPH -> Html.em $ foldMap go subs
      -- Here's the custom image handling
      IMAGE url title ->
        if not (T.null title)
          then error $ show title
          else
            Html.div ! HtmlA.class_ "columns is-centered" $
              Html.div ! HtmlA.class_ "column has-text-centered" $
                Html.img ! HtmlA.src (B.textValue url) ! HtmlA.alt (B.textValue (foldMap scrubNode subs))
      LINK url title ->
        if not (T.null title)
          then error $ show title
          else Html.a ! HtmlA.href (B.textValue url) $ foldMap go subs
      LIST attrs ->
        let node = case listType attrs of
              BULLET_LIST -> Html.ul
              ORDERED_LIST -> Html.ol
         in node $ foldMap go subs
      ITEM -> Html.li $ foldMap go subs
      -- Here's the custom server-side syntax highlighting.
      CODE_BLOCK language code ->
        -- Supported languages here: https://github.com/jgm/skylighting/tree/master/skylighting-core/xml
        case language of
          "plain" -> Html.pre $ B.text code
          "console" -> Html.pre $ B.text code
          "" -> Html.pre $ B.text code -- TODO error on this case
          _ ->
            let tokenizerConfig = TokenizerConfig {syntaxMap = defaultSyntaxMap, traceOutput = False}
                syntax = case syntaxByName (syntaxMap tokenizerConfig) language of
                  Nothing -> error $ "Unknown programming language for highlighting: " <> show language
                  Just s -> s
                sourceLines = case tokenize tokenizerConfig syntax code of
                  Left err -> error $ "Failed to parse source code: " <> err
                  Right sls -> sls
             in Html.div ! HtmlA.style (B.textValue $ T.pack $ styleToCss espresso) $
                  Skylighting.formatHtmlBlock Skylighting.defaultFormatOpts sourceLines
      _ -> error $ "Unsupported node: " <> show n

-- n -> Html.code $ B.text $ T.pack $ show n

splitContents :: Text -> ([(Text, Text)], Text)
splitContents cs =
  let threeDashes = "---"
      parts = T.splitOn threeDashes cs
   in case parts of
        "" : ts : rest ->
          let attLines = T.lines ts
              tags =
                flip mapMaybe attLines $ \l ->
                  let column = ":"
                   in case T.splitOn column l of
                        [] -> Nothing
                        (key : valParts) ->
                          Just (key, T.intercalate column valParts)
              contents = T.intercalate threeDashes rest
           in (tags, contents)
        [contents] -> ([], contents)
        _ -> error $ "Failed to parse attributes in" <> T.unpack cs

parseDayFromUrl :: String -> Maybe Day
parseDayFromUrl =
  parseTimeM False defaultTimeLocale "%F" . take (T.length "2016-01-24")

data EntryExtra
  = PostEntry PostExtra
  | QuoteEntry QuoteExtra
  deriving (Typeable, Lift)

type Entry = MdFile EntryExtra

entryTags :: Entry -> [Text]
entryTags e =
  case mdExtra e of
    PostEntry p -> postTags p
    QuoteEntry q -> quoteTags q

entryDate :: Entry -> Day
entryDate e =
  case mdExtra e of
    PostEntry p -> postDate p
    QuoteEntry q -> quoteDate q

data PageExtra = PageExtra {pageDescription :: !Text}
  deriving (Typeable, Lift)

type Page = MdFile PageExtra

pagesValFunc :: Text -> Text -> Page
pagesValFunc = mkValFunc $ \urlString _ attributes ->
  let maybeAtt k =
        case lookup k attributes of
          Nothing ->
            error $
              unlines
                [ "The page with url",
                  urlString,
                  "Does not have an attribute with key",
                  T.unpack k
                ]
          Just a -> a
      description = maybeAtt "description"
   in PageExtra {pageDescription = description}
