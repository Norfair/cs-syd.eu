module Site.Style.TH where

import Language.Haskell.TH
import System.Environment
import System.Exit
import Yesod.EmbeddedStatic
import Yesod.EmbeddedStatic.Types

styleGenerator :: Generator
styleGenerator = do
  md <- runIO $ lookupEnv "STYLE_FILE"
  case md of
    Nothing -> runIO $ die "No STYLE_FILE configured."
    Just f -> do
      runIO $ putStrLn $ unwords ["Including style in : ", f]
      embedFileAt "index.css" f
