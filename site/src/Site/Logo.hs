{-# LANGUAGE TemplateHaskell #-}

module Site.Logo
  ( module Site.Logo,
    module Yesod.EmbeddedStatic,
  )
where

import Site.Logo.TH
import Yesod.EmbeddedStatic

mkEmbeddedLogo
