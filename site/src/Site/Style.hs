{-# LANGUAGE TemplateHaskell #-}

module Site.Style
  ( module Site.Style,
    module Yesod.EmbeddedStatic,
  )
where

import Site.Constants
import Site.Style.TH
import Yesod.EmbeddedStatic

mkEmbeddedStatic
  development
  "staticStyle"
  [ styleGenerator
  ]
