{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Home
  ( getHomeR,
  )
where

import qualified Data.Map.Strict as M
import SchemaDotOrg
import SchemaDotOrg.JSONLD
import Site.Foundation
import Site.LD

getHomeR :: Handler Html
getHomeR = do
  mVerificationTag <- siteVerification <$> getYesod
  es <- loadIO $ sortEntries . M.toList <$> getEntries
  defaultLayout $ do
    setTitle $ toHtml siteTitle
    setDescriptionIdemp siteDescription
    forM_ mVerificationTag $ \verificationTag ->
      toWidgetHead [hamlet|<meta name="google-site-verification" content="#{verificationTag}"/>|]
    renderUrl <- getUrlRender
    toWidgetHead $ toJSONLDData $ renderClass classWebSite $ csSydWebsite renderUrl
    $(widgetFile "home")
