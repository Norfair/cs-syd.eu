{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Quotes
  ( getQuotesR,
  )
where

import qualified Data.Map.Strict as M
import Site.Foundation

getQuotesR :: Handler Html
getQuotesR = do
  quotes <- loadIO $ sortQuotes . M.toList <$> getQuotes
  defaultLayout $ do
    withTitle "Quotes"
    toWidgetHead [hamlet| <meta name="description" content="Quote Posts"/> |]
    $(widgetFile "quotes")
