{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Site.Handler.Rss
  ( getRssR,
    getAtomR,
  )
where

import qualified Data.Map.Strict as M
import Data.Time
import Site.Foundation
import Text.Blaze.Html as HTML
import Yesod.AtomFeed
import Yesod.RssFeed

getRssR :: Handler RepRss
getRssR = do
  feed <- loadIO $ getFeed RssR
  rssFeed feed

getAtomR :: Handler RepAtom
getAtomR = do
  feed <- loadIO $ getFeed AtomR
  atomFeed feed

getFeed :: Route Site -> Load (Feed (Route Site))
getFeed selfLink = do
  entries <- getEntries
  pure $
    Feed
      { feedTitle = "CS SYD",
        feedLinkSelf = selfLink,
        feedLinkHome = HomeR,
        feedAuthor = "Tom Sydney Kerckhove <syd@cs-syd.eu>",
        feedDescription = "CS SYD Blog",
        feedLanguage = "en-gb",
        feedUpdated = maximum $ M.map (dayTime . entryLastUpdated) entries,
        feedLogo = Just (LogoR positive_logo_png, "CS SYD Logo"),
        feedEntries = flip map (M.toList entries) $ \(url, md@MdFile {..}) ->
          FeedEntry
            { feedEntryLink = case mdExtra of
                PostEntry _ -> PostsR url
                QuoteEntry _ -> QuoteR url,
              feedEntryUpdated = dayTime $ entryLastUpdated md,
              feedEntryTitle = mdTitle,
              feedEntryContent = HTML.preEscapedToHtml mdRendered,
              feedEntryEnclosure = Nothing,
              feedEntryCategories = []
            }
      }

entryLastUpdated :: Entry -> Day
entryLastUpdated e = fromMaybe (entryDate e) (mdLastUpdated e)

dayTime :: Day -> UTCTime
dayTime d = localTimeToUTC utc $ LocalTime d midnight
