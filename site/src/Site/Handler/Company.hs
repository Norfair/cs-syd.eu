{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Company (getCompanyR) where

import SchemaDotOrg
import SchemaDotOrg.JSONLD.Render
import Site.Foundation
import Site.LD

getCompanyR :: Handler Html
getCompanyR =
  defaultLayout $ do
    withTitle "Company"
    renderUrl <- getUrlRender
    toWidgetHead [hamlet|<meta name="description" content="Fact sheet of the CS SYD organisation."/>|]
    toWidgetHead $ toJSONLDData $ renderClass classOrganization $ csSydOrganization renderUrl
    $(widgetFile "company")
