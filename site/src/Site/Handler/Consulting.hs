{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Consulting where

import Site.Foundation

getConsultingR :: Handler Html
getConsultingR = do
  defaultLayout $ do
    withTitle "Technical Consulting"
    addScript $ AssetsStaticR bulma_carousel_js
    toWidgetHead [hamlet|<meta name="description" content="An overview of my technical consulting services"/>|]
    $(widgetFile "consulting")
