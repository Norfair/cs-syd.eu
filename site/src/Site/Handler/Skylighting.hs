{-# LANGUAGE OverloadedStrings #-}

module Site.Handler.Skylighting (getSkylightingCssR) where

import Site.Foundation
import qualified Skylighting

getSkylightingCssR :: Handler TypedContent
getSkylightingCssR = do
  neverExpires
  respond "text/css" $ Skylighting.styleToCss Skylighting.pygments
