{-# LANGUAGE OverloadedStrings #-}

module Site.Handler.Assets
  ( getAssetsR,
  )
where

import qualified Data.Text as T
import Site.Foundation

-- TODO get rid of these and use type-safe redirects in post files
getAssetsR :: [Text] -> Handler Html
getAssetsR t = do
  if development
    then sendFile "" $ T.unpack $ "assets/" <> T.intercalate "/" t
    else do
      neverExpires
      redirect $ T.intercalate "/" $ "/assets-static/res" : t
