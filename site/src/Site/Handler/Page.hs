{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Page
  ( getCvsR,
    getNotesR,
    getTalksR,
    getThesisR,
  )
where

import Site.Foundation

getCvsR :: Handler Html
getCvsR = redirect AboutR

getNotesR :: Handler Html
getNotesR = pageHandler "notes"

getTalksR :: Handler Html
getTalksR = pageHandler "talks"

getThesisR :: Handler Html
getThesisR = pageHandler "thesis"

pageHandler :: Text -> Handler Html
pageHandler fn = do
  page <- getPage fn
  defaultLayout $ do
    withTitle $ mdTitle page
    toWidgetHead
      [hamlet|
        <meta name="description" content="#{pageDescription $ mdExtra page}"/>
      |]
    $(widgetFile "page")
