{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Admin
  ( getPanelR,
  )
where

import qualified Data.Map.Strict as M
import Site.Foundation

getPanelR :: Handler Html
getPanelR = do
  renderer <- getUrlRender
  posts <- loadIO $ sortPosts . M.toList <$> getPosts
  defaultLayout $ do
    withTitle "Admin"
    $(widgetFile "admin")
