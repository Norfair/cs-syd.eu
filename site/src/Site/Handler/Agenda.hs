{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Agenda (getContactR, getAgendaR) where

import Site.Foundation

getContactR :: Handler Html
getContactR = getAgendaContactPage

getAgendaR :: Handler Html
getAgendaR = getAgendaContactPage

getAgendaContactPage :: Handler Html
getAgendaContactPage = do
  defaultLayout $ do
    withTitle "Contact"
    toWidgetHead [hamlet|<meta name="description" content="Instructions for contacting Syd."/>|]
    $(widgetFile "contact")
