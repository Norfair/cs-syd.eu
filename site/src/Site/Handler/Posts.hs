{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Posts
  ( getPostsR,
  )
where

import qualified Data.Map.Strict as M
import qualified Data.Text as T
import SchemaDotOrg
import SchemaDotOrg.JSONLD.Render
import Site.Foundation
import Site.LD

getPostsR :: Text -> Handler Html
getPostsR url = do
  posts <- loadIO getPosts
  case M.lookup url posts of
    Nothing -> do
      quotes <- loadIO getQuotes
      case M.lookup url quotes of
        Just _ -> redirect $ QuoteR url
        Nothing -> do
          let url' = T.replace "_" "-" url -- In case we ever mistype a url with a _ instead of - somewhere.
          if url' == url
            then notFound
            else redirect $ PostsR url'
    Just md -> do
      entries <- loadIO getEntries
      let (mPrevE, mNextE) = findNextAndPrevious url fst $ sortEntries $ M.toList entries
      neverExpires
      let title = mdTitle md
      let description = postDescription $ mdExtra md
      defaultLayout $ do
        withTitle title
        toWidgetHead
          [hamlet|
            <meta name="description" content="#{description}"/>

            <meta property="og:type" content="website">
            <meta property="og:url" content=@{PostsR url}>
            <meta property="og:title" content=#{title}>
            <meta property="og:description" content=#{description}>
            
            <meta name="twitter:site" content="@kerckhove_ts">
            <meta property="twitter:domain" content=@{HomeR}>
            <meta property="twitter:url" content=@{PostsR url}>
            <meta name="twitter:title" content=#{title}>
            <meta property="twitter:description" content=#{description}>
          |]
        urlRender <- getUrlRender
        toWidgetHead $ toJSONLDData $ renderClass classArticle $ postLDArticle urlRender url md
        addStylesheet SkylightingCssR
        -- Maths
        addScriptRemote
          "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
        $(widgetFile "post")

postLDArticle :: (Route Site -> Text) -> Text -> Post -> [RenderOf '[Article, CreativeWork, Thing]]
postLDArticle renderUrl mdUrl post =
  concat
    [ [ renderTextProperty propertyArticleArticleBody $ mdBody post,
        renderProperty propertyArticleWordCount $ toInteger $ mdWordCount post,
        renderPropertyClass propertyCreativeWorkAuthor classPerson $ tomSydneyKerckhovePerson renderUrl,
        renderPropertyClass propertyCreativeWorkPublisher classOrganization $ csSydOrganization renderUrl,
        renderPropertyClass propertyCreativeWorkIsPartOf classWebSite $ csSydWebsite renderUrl,
        renderTextProperty propertyThingName $ mdTitle post,
        renderTextProperty propertyThingUrl $ renderUrl $ PostsR mdUrl,
        renderPropertyList propertyThingImage ([] :: [Text]),
        renderProperty propertyCreativeWorkDateModified $ postDate $ mdExtra post
      ],
      [ renderProperty propertyCreativeWorkDatePublished modified | modified <- maybeToList (mdLastUpdated post)
      ]
    ]
