{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Drafts
  ( getDraftsR,
    getDraftR,
  )
where

import qualified Data.Map as M
import Site.Foundation

getDraftsR :: Handler Html
getDraftsR = do
  renderDrafts <- siteDrafts <$> getYesod
  unless renderDrafts notFound
  drafts <- loadIO $ sortDrafts . M.toList <$> getDrafts
  defaultLayout $ do
    withTitle "Drafts"
    toWidgetHead [hamlet| <meta name="description" content="Draft Posts"/> |]
    addStylesheet SkylightingCssR
    $(widgetFile "drafts")

getDraftR :: Text -> Handler Html
getDraftR url = do
  renderDrafts <- siteDrafts <$> getYesod
  unless renderDrafts notFound
  drafts <- loadIO getDrafts
  case M.lookup url drafts of
    Nothing -> notFound
    Just md ->
      defaultLayout $ do
        withTitle $ mdTitle md
        -- Maths
        addScriptRemote
          "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS-MML_HTMLorMML"
        toWidgetHead [hamlet|<meta name="description" content="#{draftDescription $ mdExtra md}"/>|]
        addStylesheet SkylightingCssR
        $(widgetFile "draft")
