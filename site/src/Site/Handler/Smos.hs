{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Smos where

import Site.Foundation

getSmosR :: Handler Html
getSmosR =
  defaultLayout $(widgetFile "smos")
