{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Blog
  ( getBlogR,
  )
where

import qualified Data.Map as M
import Data.Time
import Site.Foundation

getBlogR :: Handler Html
getBlogR = do
  es <- loadIO $ sortEntries . M.toList <$> getEntries
  entryYears <- loadIO getEntryYears
  defaultLayout $ do
    withTitle "Blog"
    toWidgetHead [hamlet| <meta name="description" content="CS SYD Blog"/> |]
    $(widgetFile "blog")

getEntryYears :: Load [(Integer, [(Text, Entry)])]
getEntryYears =
  fmap (map (\es -> (year . snd $ head es, es)) . groupBy ((==) `on` (year . snd))) (sortEntries . M.toList <$> getEntries)
  where
    year e =
      let (y, _, _) = toGregorian (entryDate e)
       in y
