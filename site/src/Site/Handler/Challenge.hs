{-# LANGUAGE OverloadedStrings #-}

module Site.Handler.Challenge
  ( getChallengeR,
  )
where

import qualified Data.Text as T
import Site.Foundation
import System.Random

getChallengeR :: Handler Html
getChallengeR = do
  p <- liftIO genPrime
  addHeader "CHALLENGE" $
    "Mention this value in your application email: " <> T.pack (show p)
  pure "Solve this challenge!"

genPrime :: IO Integer
genPrime = do
  i <- abs <$> randomRIO (1, 10)
  pure $ cycle primesToChooseFrom !! i

primesToChooseFrom :: [Integer]
primesToChooseFrom =
  filter isPrime $ map (\v -> 2 ^ v - 1) [(10 :: Integer) .. 20]

isPrime :: Integer -> Bool
isPrime n = go 2
  where
    go d
      | d * d > n = True
      | n `rem` d == 0 = False
      | otherwise = go (d + 1)
