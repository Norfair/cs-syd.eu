module Site.Handler.Archive
  ( getArchiveR,
  )
where

import Site.Foundation

getArchiveR :: Handler Html
getArchiveR = redirect BlogR
