{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Tag
  ( getTagR,
  )
where

import qualified Data.Map.Strict as M
import qualified Data.Text as T
import Site.Foundation

getTagR :: Text -> Handler Html
getTagR tag = do
  entries <- loadIO getEntries
  let taggedEntries = sortEntries $ M.toList $ M.filter (\p -> tag `elem` entryTags p) entries
  defaultLayout $ do
    withTitle $ T.concat ["Posts tagged \"", tag, "\""]
    toWidgetHead [hamlet|<meta name="description" content="An overview of the blog posts tagged \"${tag}\""/>|]
    $(widgetFile "tag")
