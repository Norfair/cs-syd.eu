{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Support
  ( getSupportR,
    getThankyouR,
  )
where

import Site.Foundation

getSupportR :: Handler Html
getSupportR =
  defaultLayout $ do
    withTitle "Support"
    addScriptRemote "https://buttons.github.io/buttons.js"
    toWidgetHead [hamlet| <meta name="description" content="Support CS SYD"/> |]
    $(widgetFile "support")

getThankyouR :: Handler Html
getThankyouR =
  defaultLayout $ do
    withTitle "Thank You!"
    $(widgetFile "thankyou")
