{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Site.Handler.Sitemap
  ( getRobotsR,
    getSitemapR,
  )
where

import qualified Data.Map.Strict as M
import qualified Data.Text as T
import Data.Time
import Language.Haskell.TH.Load
import Site.Foundation
import Yesod.Sitemap

getRobotsR :: Handler Text
getRobotsR = (<> rest) <$> robots SitemapR
  where
    rest :: Text
    rest =
      T.unlines
        [ "Allow: /",
          "Disallow: /admin",
          "",
          "User-agent: SemrushBot",
          "Disallow: /",
          "",
          "User-agent: PetalBot",
          "Disallow: /",
          "",
          "User-agent: AhrefsBot",
          "Disallow: /",
          "",
          "User-agent: SeznamBot",
          "Disallow: /",
          "",
          "User-agent: MJ12bot",
          "Disallow: /",
          "",
          "User-agent: dotbot",
          "Disallow: /",
          "",
          "User-agent: DataForSeoBot",
          "Disallow: /"
        ]

getSitemapR :: Handler TypedContent
getSitemapR = do
  list <- loadIO getList
  sitemapList list

getList :: Load [SitemapUrl (Route Site)]
getList = do
  mainUrls <- getMainUrls
  entryUrls <- getEntryUrls
  tagsUrls <- getTagsUrls
  pure $
    homeUrl
      : aboutUrl
      : supportUrl
      : archiveUrl
      : contactUrl
      : concat [bigUrls, mainUrls, tagsUrls, entryUrls, feedUrls]

homeUrl :: SitemapUrl (Route Site)
homeUrl =
  SitemapUrl
    { sitemapLoc = HomeR,
      sitemapLastMod = Nothing,
      sitemapChangeFreq = Just Weekly,
      sitemapPriority = Just 1.0
    }

aboutUrl :: SitemapUrl (Route Site)
aboutUrl =
  SitemapUrl
    { sitemapLoc = AboutR,
      sitemapLastMod = Nothing,
      sitemapChangeFreq = Just Weekly,
      sitemapPriority = Just 0.8
    }

supportUrl :: SitemapUrl (Route Site)
supportUrl =
  SitemapUrl
    { sitemapLoc = SupportR,
      sitemapLastMod = Nothing,
      sitemapChangeFreq = Just Monthly,
      sitemapPriority = Just 0.7
    }

contactUrl :: SitemapUrl (Route Site)
contactUrl =
  SitemapUrl
    { sitemapLoc = ContactR,
      sitemapLastMod = Nothing,
      sitemapChangeFreq = Just Yearly,
      sitemapPriority = Just 0.7
    }

bigUrls :: [SitemapUrl (Route Site)]
bigUrls =
  map
    ( \r ->
        SitemapUrl
          { sitemapLoc = r,
            sitemapLastMod = Nothing,
            sitemapChangeFreq = Just Monthly,
            sitemapPriority = Just 0.85
          }
    )
    [SelfManagementR, SpeakingR]

getMainUrls :: Load [SitemapUrl (Route Site)]
getMainUrls = do
  pages <- getPages
  pure $
    map
      ( \(url, v) -> do
          case M.lookup url pages of
            Nothing -> error $ "main url not found: " <> T.unpack url
            Just md ->
              SitemapUrl
                { sitemapLoc = v,
                  sitemapLastMod = dayTime <$> mdLastUpdated md,
                  sitemapChangeFreq = Just Weekly,
                  sitemapPriority = Just 0.8
                }
      )
      [ ("notes", NotesR),
        ("talks", TalksR),
        ("thesis", ThesisR)
      ]

archiveUrl :: SitemapUrl (Route Site)
archiveUrl =
  SitemapUrl
    { sitemapLoc = ArchiveR,
      sitemapLastMod = Nothing,
      sitemapChangeFreq = Just Monthly,
      sitemapPriority = Just 0.5
    }

getTagsUrls :: Load [SitemapUrl (Route Site)]
getTagsUrls = do
  tags <- getTags
  pure $
    SitemapUrl
      { sitemapLoc = TagsR,
        sitemapLastMod = Nothing,
        sitemapChangeFreq = Just Weekly,
        sitemapPriority = Just 0.5
      }
      : map
        ( \t ->
            SitemapUrl
              { sitemapLoc = TagR t,
                sitemapLastMod = Nothing,
                sitemapChangeFreq = Just Monthly,
                sitemapPriority = Just 0.4
              }
        )
        tags

getEntryUrls :: Load [SitemapUrl (Route Site)]
getEntryUrls = do
  entries <- getEntries
  pure $
    flip map (M.toList entries) $ \(url, MdFile {..}) ->
      SitemapUrl
        { sitemapLoc = case mdExtra of
            PostEntry _ -> PostsR url
            QuoteEntry _ -> QuoteR url,
          sitemapLastMod = dayTime <$> mdLastUpdated,
          sitemapChangeFreq = Just Never,
          sitemapPriority = Just 0.1
        }

dayTime :: Day -> UTCTime
dayTime d = localTimeToUTC utc $ LocalTime d midnight

feedUrls :: [SitemapUrl (Route Site)]
feedUrls =
  map
    ( \u ->
        SitemapUrl
          { sitemapLoc = u,
            sitemapLastMod = Nothing,
            sitemapChangeFreq = Just Monthly,
            sitemapPriority = Just 0.3
          }
    )
    [RssR, SitemapR]
