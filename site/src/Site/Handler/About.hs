{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.About where

import SchemaDotOrg
import SchemaDotOrg.JSONLD.Render
import Site.Foundation
import Site.LD

getAboutR :: Handler Html
getAboutR = do
  es <- loadIO getEntries
  defaultLayout $ do
    withTitle "About"
    addScript $ AssetsStaticR bulma_carousel_js
    toWidgetHead [hamlet|<meta name="description" content="About Tom Sydney Kerckhove"/>|]
    renderUrl <- getUrlRender
    toWidgetHead $ toJSONLDData $ renderClass classPerson $ tomSydneyKerckhovePerson renderUrl
    $(widgetFile "about")
