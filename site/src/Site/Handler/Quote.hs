{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Site.Handler.Quote
  ( getQuoteR,
  )
where

import qualified Data.Map.Strict as M
import SchemaDotOrg
import SchemaDotOrg.JSONLD.Render
import Site.Foundation
import Site.LD

getQuoteR :: Text -> Handler Html
getQuoteR url = do
  quotes <- loadIO getQuotes
  case M.lookup url quotes of
    Nothing -> notFound
    Just md -> do
      entries <- loadIO getEntries
      let (mPrevE, mNextE) = findNextAndPrevious url fst $ sortEntries $ M.toList entries
      neverExpires
      let title = mdTitle md
      let description = quoteDescription $ mdExtra md
      defaultLayout $ do
        withTitle title
        toWidgetHead
          [hamlet|
            <meta name="description" content="#{description}"/>

            <meta property="og:type" content="website">
            <meta property="og:url" content=@{QuoteR url}>
            <meta property="og:title" content=#{title}>
            <meta property="og:description" content=#{description}>
            
            <meta name="twitter:site" content="@kerckhove_ts">
            <meta property="twitter:domain" content=@{HomeR}>
            <meta property="twitter:url" content=@{QuoteR url}>
            <meta name="twitter:title" content=#{title}>
            <meta property="twitter:description" content=#{description}>
          |]
        urlRender <- getUrlRender
        toWidgetHead $ toJSONLDData $ renderClass classArticle $ quoteLDArticle urlRender url md

        $(widgetFile "quote")

quoteLDArticle :: (Route Site -> Text) -> Text -> Quote -> [RenderOf '[Article, CreativeWork, Thing]]
quoteLDArticle renderUrl mdUrl quote =
  concat
    [ [ renderTextProperty propertyArticleArticleBody $ mdBody quote,
        renderProperty propertyArticleWordCount $ toInteger $ mdWordCount quote,
        renderPropertyClass propertyCreativeWorkAuthor classPerson $ tomSydneyKerckhovePerson renderUrl,
        renderPropertyClass propertyCreativeWorkPublisher classOrganization $ csSydOrganization renderUrl,
        renderPropertyClass propertyCreativeWorkIsPartOf classWebSite $ csSydWebsite renderUrl,
        renderTextProperty propertyThingName $ mdTitle quote,
        renderTextProperty propertyThingUrl $ renderUrl $ PostsR mdUrl,
        renderPropertyList propertyThingImage ([] :: [Text]),
        renderProperty propertyCreativeWorkDateModified $ quoteDate $ mdExtra quote
      ],
      [ renderProperty propertyCreativeWorkDatePublished modified | modified <- maybeToList (mdLastUpdated quote)
      ]
    ]
