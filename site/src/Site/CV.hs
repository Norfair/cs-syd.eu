{-# LANGUAGE TemplateHaskell #-}

module Site.CV
  ( module Site.CV,
    module Yesod.EmbeddedStatic,
  )
where

import Site.CV.TH
import Yesod.EmbeddedStatic

mkEmbeddedCv
