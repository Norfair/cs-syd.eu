{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Site.Serve where

import Control.Monad.Logger
import qualified Data.Text as T
import qualified Necrork
import Network.HTTP.Types as HTTP
import Network.Wai as Wai
import qualified Network.Wai.Handler.Warp as Warp
import Network.Wai.Middleware.RequestLogger as Wai
import Site.Application ()
import Site.Foundation
import Site.OptParse

serve :: Settings -> IO ()
serve settings@Settings {..} = runStderrLoggingT $ do
  let site =
        Site
          { siteLogo = staticLogo,
            siteStyle = staticStyle,
            siteAssets = staticAssets,
            siteCv = staticCv,
            siteApproot = settingApproot,
            siteTracking = settingTracking,
            siteVerification = settingVerification,
            siteDrafts = settingDrafts
          }

  let runServer :: LoggingT IO ()
      runServer = liftIO $ do
        putStrLn $ unlines ["Serving with these settings", ppShow settings]

        siteApp <- toWaiAppPlain site
        Warp.run settingPort $ customMiddleware siteApp
  Necrork.withMNotifier settingNecrorkNotifierSettings runServer

customMiddleware :: Wai.Middleware
customMiddleware =
  retryWithoutHtmlMiddleware
    . ( if development
          then Wai.logStdoutDev
          else Wai.logStdout
      )
    . defaultMiddlewaresNoLogging

retryWithoutHtmlMiddleware :: Wai.Middleware
retryWithoutHtmlMiddleware doRequest request sendResp =
  doRequest request $ \response ->
    if responseStatus response /= notFound404
      then sendResp response
      else case dropHtmlExtension $ pathInfo request of
        Nothing -> sendResp response
        Just pathInfo' ->
          let request' = request {pathInfo = pathInfo'}
           in doRequest request' sendResp

-- 'Nothing' means "Don't retry at all"
dropHtmlExtension :: [Text] -> Maybe [Text]
dropHtmlExtension ts =
  case reverse ts of
    [] -> Nothing
    (part : rest) ->
      case T.stripSuffix ".html" part of
        Nothing -> Nothing
        Just stripped -> Just $ reverse $ stripped : rest
