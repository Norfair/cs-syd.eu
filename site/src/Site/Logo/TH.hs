module Site.Logo.TH where

import Language.Haskell.TH
import Site.Constants
import System.Environment
import System.Exit
import Yesod.EmbeddedStatic

mkEmbeddedLogo :: Q [Dec]
mkEmbeddedLogo = do
  md <- runIO $ lookupEnv "LOGO_DIR"
  let doEmbed d =
        [ embedDirAt "positive" (d <> "/positive"),
          embedDirAt "negative" (d <> "/negative"),
          embedFileAt "favicon.ico" (d <> "/favicon/favicon.ico")
        ]
  fs <- case md of
    Nothing ->
      runIO $ die "No LOGO_DIR configured."
    Just d -> do
      runIO $ putStrLn $ unwords ["Including logo in dir: ", d]
      pure $ doEmbed d
  mkEmbeddedStatic development "staticLogo" fs
