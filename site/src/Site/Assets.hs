{-# LANGUAGE TemplateHaskell #-}

module Site.Assets
  ( module Site.Assets,
    module Yesod.EmbeddedStatic,
  )
where

import Site.Assets.TH
import Yesod.EmbeddedStatic

mkAssets
