{-# OPTIONS_GHC -ddump-splices #-}

module Site.CV.TH where

import Language.Haskell.TH
import Site.Constants
import System.Environment
import System.Exit
import Yesod.EmbeddedStatic

mkEmbeddedCv :: Q [Dec]
mkEmbeddedCv = do
  let baseFiles =
        [ "fail.pdf",
          "ic.pdf",
          "tl.pdf",
          "cv.pdf",
          "haskell.pdf"
        ]
  md <- runIO $ lookupEnv "CV_DIR"
  fs <-
    case md of
      Nothing -> runIO $ die "No CV_DIR configured."
      Just d -> do
        runIO $ putStrLn $ unwords ["Including CV in dir: ", d]
        pure $ map (\p -> embedFileAt p $ d ++ "/" ++ p) baseFiles
  mkEmbeddedStatic development "staticCv" fs
