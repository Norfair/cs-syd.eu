module Site.Assets.TH (mkAssets) where

import Data.FileEmbed (makeRelativeToProject)
import Language.Haskell.TH
import Site.Constants
import Yesod.EmbeddedStatic
import Yesod.EmbeddedStatic.Remote
import Yesod.EmbeddedStatic.Types as Static

mkAssets :: Q [Dec]
mkAssets = do
  staticDir <- makeRelativeToProject "static/"
  let remoteStatic :: FilePath -> String -> Static.Generator
      remoteStatic fp = embedRemoteFileAt fp (staticDir ++ fp)
  mkEmbeddedStatic
    development
    "staticAssets"
    [ remoteStatic "instantpage.js" "https://instant.page/5.1.0",
      remoteStatic "bulma-carousel.js" "https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js",
      embedDir "assets"
    ]
