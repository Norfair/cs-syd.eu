{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}

module Site.LD where

import SchemaDotOrg
import SchemaDotOrg.JSONLD.Render
import Site.Foundation

-- Should be 51-55 chars
siteTitle :: Text
siteTitle = "CS SYD: Technical Consulting, Self-management and Blog"

-- Should be 150-160 chars
siteDescription :: Text
siteDescription = "CS SYD: Technical consulting in Leadership, Haskell, Nix, Rust; Self-management training, Sustainable Products, Speaking engagements and over 200 blog posts"

csSydWebsite :: (Route Site -> Text) -> [RenderOf '[WebSite, CreativeWork, Thing]]
csSydWebsite renderUrl =
  csSydWebsiteShared renderUrl
    ++ let tomSydneyKerckhove = tomSydneyKerckhovePersonShared renderUrl
        in [ renderPropertyClass propertyCreativeWorkCopyrightHolder classPerson tomSydneyKerckhove,
             renderPropertyClass propertyCreativeWorkCreator classPerson tomSydneyKerckhove,
             renderPropertyClass propertyCreativeWorkProducer classOrganization $ csSydOrganizationShared renderUrl
           ]

csSydWebsiteShared :: (Route Site -> Text) -> [RenderOf '[WebSite, CreativeWork, Thing]]
csSydWebsiteShared renderUrl =
  [ renderTextProperty propertyCreativeWorkCopyrightNotice "Copyright Tom Sydney Kerckhove (c) 2012-2024",
    renderTextProperty propertyCreativeWorkHeadline siteTitle,
    renderTextProperty propertyThingDescription siteDescription,
    renderTextProperty propertyThingName "CS SYD",
    renderTextProperty propertyThingUrl $ renderUrl HomeR,
    renderSimpleProperty propertyCreativeWorkCopyrightYear 2012,
    renderTextProperty propertyCreativeWorkInLanguage "English",
    renderPropertyList
      propertyCreativeWorkKeywords
      [ "Leadership",
        "Haskell",
        "Nix",
        "Rust",
        "Self-management",
        "Speaking",
        "Blog" :: Text
      ]
  ]

csSydOrganization :: (Route Site -> Text) -> [RenderOf '[Organization, Thing]]
csSydOrganization renderUrl =
  csSydOrganizationShared renderUrl
    ++ [ renderPropertyClass propertyOrganizationFounder classPerson $ tomSydneyKerckhovePersonShared renderUrl
       ]

csSydOrganizationShared :: (Route Site -> Text) -> [RenderOf '[Organization, Thing]]
csSydOrganizationShared renderUrl =
  [ renderTextProperty propertyThingName "CS SYD",
    renderTextProperty propertyThingUrl $ renderUrl CompanyR,
    renderTextProperty propertyOrganizationLogo $ renderUrl $ LogoR positive_logo_svg
  ]

tomSydneyKerckhovePerson :: (Route Site -> Text) -> [RenderOf '[Person, Thing]]
tomSydneyKerckhovePerson renderUrl =
  tomSydneyKerckhovePersonShared renderUrl
    ++ [ renderPropertyClass propertyPersonAffiliation classOrganization $ csSydOrganizationShared renderUrl
       ]

tomSydneyKerckhovePersonShared :: (Route Site -> Text) -> [RenderOf '[Person, Thing]]
tomSydneyKerckhovePersonShared renderUrl =
  [ renderTextProperty propertyThingName "Tom Sydney Kerckhove",
    renderTextProperty propertyThingUrl $ renderUrl AboutR
  ]
