{-# LANGUAGE CPP #-}
{-# LANGUAGE TypeFamilies #-}

module Site.Widget where

import Data.Default
import Language.Haskell.TH
import Site.Constants
import Yesod.Default.Util

widgetFile :: String -> Q Exp
widgetFile =
  if development
    then widgetFileReload widgetFileSettings
    else widgetFileNoReload widgetFileSettings

widgetFileSettings :: WidgetFileSettings
widgetFileSettings = def
