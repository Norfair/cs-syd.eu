{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module Site.OptParse
  ( getSettings,
    Settings (..),
  )
where

import Import
import qualified Necrork
import OptEnvConf
import Paths_site (version)

getSettings :: IO Settings
getSettings = runSettingsParser version "Run the cs-syd.eu site"

data Settings = Settings
  { settingPort :: !Int,
    settingApproot :: !(Maybe Text),
    settingTracking :: !(Maybe Text),
    settingVerification :: !(Maybe Text),
    settingNecrorkNotifierSettings :: !(Maybe Necrork.NotifierSettings),
    settingDrafts :: !Bool
  }
  deriving (Show)

instance HasParser Settings where
  settingsParser = subEnv_ "site" $ withLocalYamlConfig $ do
    settingPort <-
      setting
        [ help "Port to serve web requests on",
          reader auto,
          value 8080,
          name "port",
          metavar "PORT"
        ]
    settingApproot <-
      optional $
        setting
          [ help "approot",
            reader str,
            name "approot",
            metavar "APPROOT",
            example "https://cs-syd.eu"
          ]
    settingTracking <-
      optional $
        setting
          [ help "Google Analytics tracking code",
            reader str,
            name "analytics-tracking-id",
            metavar "TRACKING_ID"
          ]
    settingVerification <-
      optional $
        setting
          [ help "Google Search Console verification code",
            reader str,
            name "search-console-verification",
            metavar "VERIFICATION_CODE"
          ]
    settingNecrorkNotifierSettings <- optional $ subSettings "necrork"
    settingDrafts <-
      enableDisableSwitch
        [ help "Whether to serve drafts",
          name "drafts",
          value False
        ]
    pure Settings {..}
