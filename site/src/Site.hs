module Site where

import Site.OptParse
import Site.Serve

main :: IO ()
main = getSettings >>= serve
