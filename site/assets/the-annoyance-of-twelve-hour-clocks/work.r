library(chron)

png(filename="./12h_graph.png",width=800, height=450, units="px")

amcol = "red"
pmcol = "green"

r1 <- c("00:00:00", "00:59:59", "12:00:00", "12:59:59", amcol)
r2 <- c("01:00:00", "11:59:59", "01:00:00", "11:59:59", amcol)
r3 <- c("12:00:00", "12:59:59", "12:00:00", "12:59:59", pmcol)
r4 <- c("13:00:00", "23:59:59", "01:00:00", "11:59:59", pmcol)

m <- matrix(c(r1, r2, r3, r4), ncol=5, byrow=TRUE)
print(m)

tt <- vector(mode="numeric", length=0)
weirdhs <- vector(mode="numeric", length=0)
plot(NULL
    , NULL
    , ylab = "Time (H:M:S)"
    , xlab = "Hour of the day"
    , type = 'n'
    , yaxt = "n"
    , col="red"
    , xlim=c(0
    , 1)
    , ylim=c(0
    , 0.6)
    , xaxs="i"
    , yaxs="i"
    , xaxt="n"
    , yaxt="n")


for (i in 1:length(m[,1])) {
  skip <- times("00:01:00")

  x1 = times(m[i, 1])
  x2 = times(m[i, 2])
  y1 = times(m[i, 3])
  y2 = times(m[i, 4])
  xs <- seq(x1, x2, by = skip)
  ys <- seq(y1, y2, by = skip)

  thiscol <- m[i, 5]
  lines(xs, ys, col=thiscol)
  pxs <- c(x1, xs, x2)
  pys <- c(times("00:00:00"), ys, times("00:00:00"))
  polygon(pxs, pys, col=thiscol)

  tt <- append(tt, xs)
  weirdhs <- append(weirdhs, ys)
}

tts <- seq(times("00:00:00"), times("23:59:59"), times("01:00:00"))
axis(1, tts, labels = sub(":..:00", "", times(tts)))
wss <- seq(times("00:00:00"), times("13:00:00"), times("01:00:00"))
axis(2, wss, labels = sub(":..:00", "", times(wss)))

abline(v = 0.5, col="black", lwd=2)
abline(h = 0.5, col="black")

legend(0.6,0.4 # places a legend at the appropriate place
       , c("AM","PM") # puts text in the legend
       , lty=c(1, 1) # gives the legend appropriate symbols (lines)
       , lwd=c(2.5, 2.5)
       , col=c(amcol, pmcol)) # gives the legend lines the correct color and width

png(filename="./24h_graph.png",width=800, height=450, units="px")
plot(tt, tt
    , ylab = "Time (H:M:S)"
    , xlab = "Hour of the day"
    , type="n"
    , xaxs="i"
    , yaxs="i"
    , xaxt="n"
    , yaxt="n"
    )
polygon(
      c(times("00:00:00"), times("23:59:59"), times("23:59:59"))
    , c(times("00:00:00"), times("23:59:59"), times("00:00:00"))
    , col="blue"
    )
axis(1, tts, labels = sub(":..:00", "", times(tts)))
axis(2, tts, labels = sub(":..:00", "", times(tts)))
legend(0.2,0.6 # places a legend at the appropriate place
       , c("day") # puts text in the legend
       , lty=c(1) # gives the legend appropriate symbols (lines)
       , lwd=c(2.5)
       , col=c("blue")) # gives the legend lines the correct color and width
abline(v = 0.5, col="black", lwd=2)
abline(h = 0.5, col="black")
