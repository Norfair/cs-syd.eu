size="250"
density="200"

function tikz(){
    file="$1"
    pdflatex $file.tex
    pdftops -eps $file.pdf
    convert -density $density -resize $size $file.eps $file.png
}

#tikz normal
tikz study
rm -f *.fls *.log *.pdf *.eps *.aux
