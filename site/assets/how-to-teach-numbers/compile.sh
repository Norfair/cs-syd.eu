size="500"
density="200"

function tikz(){
    file="$1"
    latexmk -pdf $file.tex
    pdftops -eps $file.pdf
    convert -density $density -resize $size $file.eps $file.png
}

tikz zero
tikz numbers
tikz addition
tikz multiplication
tikz division
tikz division2
tikz one
tikz powers
tikz powers2
tikz powers3
tikz roots1
tikz roots2
    
rm -f *.aux *.fdb_latexmk *.fls *.log *.eps *.pdf
