{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.IO as T
import Text.Dot
import Text.Dot.FSA

main :: IO ()
main =
  renderToStdOut $ fsaGraph [a, b, c] a [b] [(a, b, p), (b, b, q), (b, c, p)]
  where
    a = "a"
    b = "b"
    c = "c"
    p = "p"
    q = "q"
