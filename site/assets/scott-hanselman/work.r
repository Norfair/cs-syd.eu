hours  <- c(189.0, 94.5, 63.0, 47.25, 37.8, 31.5, 27.0, 23.625, 21.0, 18.9, 17.18, 15.75, 14.53, 13.5, 12.6, 11.81, 11.12, 10.5, 9.95, 9.45)
projects <- c("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20") #c("3 Gears", "4 Gears", "5   Gears")
col=c("green","green","green","green","green","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","yellow","red","red","red","red","red")


png(filename="./illustration.png",width=800, height=450, units="px")
barplot(hours, names.arg=projects, main="Throughput", xlab="Number of projects", ylab="Maximum average amount of hours per week", col=col)
dev.off()
