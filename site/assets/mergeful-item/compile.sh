size="800"
density="300"

function tikz(){
    file="$1"
    latexmk -pdf $file.tex
    pdftops -eps $file.pdf
    convert -density $density -resize $size $file.eps $file.png
}

tikz workflow
    
rm -f *.aux *.fdb_latexmk *.fls *.log *.eps *.pdf
