{-# LANGUAGE TypeApplications #-}

module Site.OptParseSpec (spec) where

import OptEnvConf.Test
import Site.OptParse
import Test.Syd

spec :: Spec
spec = do
  settingsLintSpec @Settings
  goldenSettingsReferenceDocumentationSpec @Settings "test_resources/documentation.txt" "site"
  goldenSettingsNixOptionsSpec @Settings "options.nix"
