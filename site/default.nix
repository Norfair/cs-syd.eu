{ mkDerivation, aeson, autoexporter, base, blaze-html, bytestring
, case-insensitive, cmark, containers, data-default, file-embed
, filepath, fsnotify, http-types, lib, monad-logger, necrork
, opt-env-conf, opt-env-conf-test, path, path-io, pretty-show
, random, safe, schema-dot-org, schema-dot-org-jsonld, shakespeare
, skylighting, sydtest, sydtest-discover, template-haskell
, template-haskell-reload, text, th-lift-instances, time, wai
, wai-extra, warp, yesod, yesod-auth, yesod-autoreload
, yesod-newsfeed, yesod-sitemap, yesod-static, yesod-static-remote
}:
mkDerivation {
  pname = "site";
  version = "0.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base blaze-html bytestring case-insensitive cmark containers
    data-default file-embed filepath fsnotify http-types monad-logger
    necrork opt-env-conf path path-io pretty-show random safe
    schema-dot-org schema-dot-org-jsonld shakespeare skylighting
    template-haskell template-haskell-reload text th-lift-instances
    time wai wai-extra warp yesod yesod-auth yesod-autoreload
    yesod-newsfeed yesod-sitemap yesod-static yesod-static-remote
  ];
  libraryToolDepends = [ autoexporter ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [ base opt-env-conf-test sydtest ];
  testToolDepends = [ sydtest-discover ];
  license = "unknown";
  mainProgram = "site";
}
