{ sources ? import ./nix/sources.nix
, pkgs ? import ./nix/pkgs.nix { inherit sources; }
, pre-commit ? import ./nix/pre-commit.nix { inherit sources; }
}:
pkgs.haskell.lib.buildStackProject {
  name = "cs-syd-shell";
  buildInputs = with pkgs; [
    (import sources.niv { }).niv
    haskellPackages.autoexporter
    haskellPackages.linkcheck
    haskellPackages.seocheck
    sass
    zlib
  ] ++ pre-commit.tools;
  shellHook = ''
    ${pre-commit.run.shellHook}
    export TERM=xterm
    export CV_DIR=${pkgs.cvRelease}

    function devel () {
      ./scripts/devel.sh
    }
  '';
}
