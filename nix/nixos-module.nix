{ cs-syd-site
, opt-env-conf
}:
{ envname }:
{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.cs-syd."${envname}";

  mergeListRecursively = pkgs.callPackage ./merge-lists-recursively.nix { };
in
{
  options.services.cs-syd."${envname}" = {
    enable = mkEnableOption "CsSyd Website Service";
    default = mkOption {
      type = types.bool;
      default = false;
      example = true;
      description = "Whether this should be the default nginx server";
    };
    hosts = mkOption {
      type = types.listOf types.str;
      example = "cs-syd.eu";
      default = [ ];
      description = "The host to serve web requests on";
    };
    openFirewall = mkOption {
      type = types.bool;
      default = false;
      description = "Whether to open the specified ports in the firewall";
    };
    config = mkOption {
      default = { };
      type = types.submodule {
        options = import ../site/options.nix { inherit lib; };
      };
    };
    extraConfig = mkOption {
      description = "The contents of the config file, as an attribute set. This will be translated to Yaml and put in the right place along with the rest of the options defined in this submodule.";
      default = { };
    };
  };
  config =
    let
      cs-syd-service =
        let
          nullOrOption =
            name: opt: optionalAttrs (!builtins.isNull opt) { "${name}" = opt; };
          nullOrOptionHead =
            name: opt: optionalAttrs (!builtins.isNull opt && opt != [ ]) { "${name}" = builtins.head opt; };
          config = with cfg;
            mergeListRecursively [
              cfg.config
              cfg.extraConfig
            ];
          config-file = (pkgs.formats.yaml { }).generate "siteconfig.yaml" config;
          workingDir = "/www/cs-syd.eu/${envname}";
        in
        opt-env-conf.addSettingsCheckToService {
          description = "cs-syd.eu site ${envname} Service";
          wantedBy = [ "multi-user.target" ];
          environment = {
            "SITE_CONFIG_FILE" = "${config-file}";
          };
          script = ''
            mkdir -p "${workingDir}"
            cd "${workingDir}"
            ${cs-syd-site}/bin/site
          '';
          serviceConfig = {
            Restart = "always";
            RestartSec = 1;
            Nice = 15;
          };
        };
    in
    mkIf cfg.enable {
      systemd.services = { "cs-syd.eu-${envname}" = cs-syd-service; };
      networking.firewall.allowedTCPPorts = optional (cfg.enable && cfg.openFirewall) cfg.config.port;
      services.nginx.virtualHosts =
        let
          redirectHost = host:
            nameValuePair "www.${host}" {
              enableACME = true;
              forceSSL = true;
              globalRedirect = host;
            };
        in
        optionalAttrs (cfg.enable && cfg.hosts != [ ]) ({
          "${head (cfg.hosts)}" = {
            enableACME = true;
            forceSSL = true;
            locations."/".proxyPass =
              "http://localhost:${builtins.toString (cfg.config.port)}";
            default = cfg.default;
            serverAliases = tail cfg.hosts;
          };
        } // builtins.listToAttrs (builtins.map redirectHost cfg.hosts));
    };
}
