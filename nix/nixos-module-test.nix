{ nixosTest
, cs-syd-nixos-module-factory
}:
let
  cs-syd-production = cs-syd-nixos-module-factory {
    envname = "production";
  };
  port = 8001;
in
nixosTest (
  { lib, pkgs, ... }: {
    name = "cs-syd-module-test";
    nodes = {
      server = {
        imports = [
          cs-syd-production
        ];
        services.cs-syd.production = {
          enable = true;
          openFirewall = true;
          config = {
            inherit port;
            drafts = true;
          };
        };
      };
      client = { };
    };
    testScript = ''
      server.start()
      client.start()

      server.wait_for_unit("multi-user.target")
      client.wait_for_unit("multi-user.target")

      server.wait_for_unit("cs-syd.eu-production.service")

      server.wait_for_open_port(${builtins.toString port})
      client.succeed("curl server:${builtins.toString port}")
    '';
  }
)
