final: prev:
with final.lib;
with final.haskell.lib;
{
  cs-syd-logo = builtins.fetchGit {
    url = "https://gitlab.com/Norfair/cs-syd-logo.git/";
    rev = "644c423d4b1c6fb2f5d44b6b4c77259d6b82064b";
  };
  cs-syd-site = final.stdenv.mkDerivation {
    name = "cs-syd-site";
    buildInputs = [
      final.haskellPackages.linkcheck
      final.haskellPackages.seocheck
      final.killall
    ];
    buildCommand = ''
      mkdir -p $out/bin
      ln -s ${justStaticExecutables final.haskellPackages.site}/bin/* $out/bin/

      $out/bin/site --port 8080 &
      sleep 0.1 # To give the site time to migrate the database
      linkcheck http://localhost:8080 --log-level Warn
      seocheck http://localhost:8080 --log-level LevelWarn
      killall site
    '';
  };
  cs-syd-stylesheet = final.stdenv.mkDerivation {
    name = "site-stylesheet.css";
    src = ../site/style/mybulma.scss;
    buildInputs = [ final.sass ];
    buildCommand =
      let
        bulma = builtins.fetchGit {
          url = "https://github.com/jgthms/bulma";
          rev = "c02757cd3043a4b30231c72dd01cd735c3b3672c";
        };
        bulma-carousel = final.fetchzip {
          url = "https://registry.npmjs.org/bulma-carousel/-/bulma-carousel-4.0.24.tgz";
          sha256 = "sha256-kLI2ij3lxuI1rFspizlCVYxdqfIC+abpWxNOg2ShtDY=";
        };
        bulma-pricingtable = final.fetchzip {
          url = "https://registry.npmjs.org/bulma-pricingtable/-/bulma-pricingtable-0.2.0.tgz";
          sha256 = "sha256-UX327JshHOPzN0Jxx3unUcBK1ejCdgFF24hOgrNDP9c=";
        };
        bulma-timeline = final.fetchzip {
          url = "https://registry.npmjs.org/bulma-timeline/-/bulma-timeline-3.0.5.tgz";
          sha256 = "sha256-zHopt4JG07hzuJH4i4LghTS/tQ7ixS+GScRrhHykjkA=";
        };

      in
      ''
        # Dependencies: bulma and bulma-pricingtable
        ln -s ${bulma} bulma
        ln -s ${bulma-pricingtable} bulma-pricingtable
        ln -s ${bulma-carousel} bulma-carousel
        ln -s ${bulma-timeline} bulma-timeline

        # The file we want to compile
        # We need to copy this so that the relative path within it resolves to
        # here instead of wherever we would link it from.
        cp $src mybulma.scss
        scss \
          --sourcemap=none \
          mybulma.scss:index.css --style compressed
        cp index.css $out
      '';
  };


  haskellPackages = prev.haskellPackages.override (old:
    {
      overrides = composeExtensions (old.overrides or (_: _: { })) (

        self: super:
          let
            templateHaskellReloadRepo = builtins.fetchGit {
              url = "https://github.com/NorfairKing/template-haskell-reload";
              rev = "6f8627b45ae63f64ad0a47d1130630aa2e24470d";
            };
            yesodAutoReloadRepo = builtins.fetchGit {
              url = "https://github.com/NorfairKing/yesod-autoreload";
              rev = "7135e864c0d4a48efeae473ee2761f5168946e58";
            };
          in
          with final.haskellPackages;
          {
            site = failOnAllWarnings (overrideCabal (self.opt-env-conf.installManpagesAndCompletions [ "site" ] (self.callPackage ../site { })) (old: {
              doBenchmark = true;
              enableLibraryProfiling = false;
              enableExecutableProfiling = false;

              buildFlags = (old.buildFlags or [ ]) ++ [
                "--ghc-options=-O2"
                "--ghc-options=-Wincomplete-uni-patterns"
                "--ghc-options=-Wincomplete-record-updates"
                "--ghc-options=-Wpartial-fields"
                "--ghc-options=-Widentities"
                "--ghc-options=-Wredundant-constraints"
                "--ghc-options=-Wcpp-undef"
                "--ghc-options=-Wunused-packages"
              ];

              preConfigure =
                let
                  instantclick_js = builtins.fetchurl {
                    url = "https://instant.page/5.1.0";
                    sha256 = "sha256:03ryk64a2dxrs65fwpjy2n03nvxd68mdi414pmwd7b7k3lvk8p7s";
                  };
                  bulma_carousel_js = builtins.fetchurl {
                    url = "https://cdn.jsdelivr.net/npm/bulma-carousel@4.0.3/dist/js/bulma-carousel.min.js";
                    sha256 = "sha256:0cm7wj49qmbi9kp5hs3wc6vcr1h0d5h864pa5bc401nm5kppp958";
                  };
                in
                ''
                  ${old.preConfigure or ""}
                  export CV_DIR=${final.cvRelease}
                  export LOGO_DIR=${final.cs-syd-logo}
                  export STYLE_FILE=${final.cs-syd-stylesheet}

                  mkdir --parents static/
                  ln -s ${instantclick_js} static/instantpage.js
                  ln -s ${bulma_carousel_js} static/bulma-carousel.js
                '';
            }));
          }
      );
    }
  );
}
