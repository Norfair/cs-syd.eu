{
  description = "cs-syd";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";
    weeder-nix.url = "github:NorfairKing/weeder-nix";
    weeder-nix.flake = false;
    validity.url = "github:NorfairKing/validity";
    validity.flake = false;
    autodocodec.url = "github:NorfairKing/autodocodec";
    autodocodec.flake = false;
    safe-coloured-text.url = "github:NorfairKing/safe-coloured-text";
    safe-coloured-text.flake = false;
    fast-myers-diff.url = "github:NorfairKing/fast-myers-diff";
    fast-myers-diff.flake = false;
    sydtest.url = "github:NorfairKing/sydtest";
    sydtest.flake = false;
    token-limiter-concurrent.url = "github:NorfairKing/token-limiter-concurrent";
    token-limiter-concurrent.flake = false;
    opt-env-conf.url = "github:NorfairKing/opt-env-conf/development";
    opt-env-conf.flake = false;
    template-haskell-reload.url = "github:NorfairKing/template-haskell-reload";
    template-haskell-reload.flake = false;
    yesod-autoreload.url = "github:NorfairKing/yesod-autoreload";
    yesod-autoreload.flake = false;
    yesod-static-remote.url = "github:NorfairKing/yesod-static-remote";
    yesod-static-remote.flake = false;
    schema-dot-org.url = "github:NorfairKing/schema-dot-org";
    schema-dot-org.flake = false;
    cv-typst.url = "gitlab:Norfair/cv-typst";
    looper.url = "github:NorfairKing/looper";
    looper.flake = false;
    necrork.url = "github:NorfairKing/necrork";
    necrork.flake = false;
    linkcheck.url = "github:NorfairKing/linkcheck";
    linkcheck.flake = false;
    seocheck.url = "github:NorfairKing/seocheck";
    seocheck.flake = false;
  };

  outputs =
    { self
    , nixpkgs
    , pre-commit-hooks
    , weeder-nix
    , validity
    , safe-coloured-text
    , fast-myers-diff
    , sydtest
    , token-limiter-concurrent
    , opt-env-conf
    , autodocodec
    , template-haskell-reload
    , cv-typst
    , yesod-autoreload
    , yesod-static-remote
    , schema-dot-org
    , looper
    , necrork
    , linkcheck
    , seocheck
    }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
        overlays = [
          self.overlays.${system}
          (import (validity + "/nix/overlay.nix"))
          (import (autodocodec + "/nix/overlay.nix"))
          (import (safe-coloured-text + "/nix/overlay.nix"))
          (import (fast-myers-diff + "/nix/overlay.nix"))
          (import (sydtest + "/nix/overlay.nix"))
          (import (token-limiter-concurrent + "/nix/overlay.nix"))
          (import (opt-env-conf + "/nix/overlay.nix"))
          (import (template-haskell-reload + "/nix/overlay.nix"))
          (import (yesod-autoreload + "/nix/overlay.nix"))
          (import (yesod-static-remote + "/nix/overlay.nix"))
          (import (schema-dot-org + "/nix/overlay.nix"))
          (import (looper + "/nix/overlay.nix"))
          (import (necrork + "/nix/overlay.nix"))
          (import (linkcheck + "/nix/overlay.nix"))
          (import (seocheck + "/nix/overlay.nix"))
          (import (weeder-nix + "/nix/overlay.nix"))
          (_:_: { cvRelease = cv-typst.packages.${system}.default; })
        ];
      };
    in
    {
      overlays.${system} = import ./nix/overlay.nix;
      packages.${system}.default = pkgs.cs-syd-site;
      checks.${system} = {
        release = self.packages.${system}.default;
        shell = self.devShells.${system}.default;
        nixos-module-test = import ./nix/nixos-module-test.nix {
          inherit (pkgs) nixosTest;
          cs-syd-nixos-module-factory = self.nixosModuleFactories.${system}.default;
        };
        weeder-check = pkgs.weeder-nix.makeWeederCheck {
          weederToml = ./weeder.toml;
          packages = [ "site" ];
        };
        pre-commit = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            hlint.enable = true;
            hpack.enable = true;
            ormolu.enable = true;
            nixpkgs-fmt.enable = true;
            nixpkgs-fmt.excludes = [ ".*/default.nix" ];
            cabal2nix.enable = true;
          };
        };
      };
      devShells.${system}.default = pkgs.haskellPackages.shellFor {
        name = "cs-syd-shell";
        packages = p: [ p.site ];
        withHoogle = true;
        doBenchmark = true;
        buildInputs = with pkgs; [
          zlib
          cabal-install
        ] ++ self.checks.${system}.pre-commit.enabledPackages;
        shellHook = self.checks.${system}.pre-commit.shellHook + ''
          export STYLE_FILE=${pkgs.cs-syd-stylesheet}
          export LOGO_DIR=${pkgs.cs-syd-logo}
          export CV_DIR=${cv-typst.packages.${system}.default}
        '';
      };
      nixosModules.${system}.default = self.nixosModuleFactories.${system}.default { envname = "production"; };
      nixosModuleFactories.${system}.default = import ./nix/nixos-module.nix {
        inherit (pkgs) cs-syd-site;
        inherit (pkgs.haskellPackages) opt-env-conf;
      };
    };
}
